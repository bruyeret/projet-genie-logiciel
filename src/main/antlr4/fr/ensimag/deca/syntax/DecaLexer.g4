lexer grammar DecaLexer;

options {
   language=Java;
   // Tell ANTLR to make the generated lexer class extend the
   // the named class, which is where any supporting code and
   // variables will be placed.
   superClass = AbstractDecaLexer;
}

@members {
}

// Deca lexer rules.

fragment EOL: '\n';

//**********************
// Keywords
//**********************
ASM       : 'asm';
CLASS     : 'class';
EXTENDS   : 'extends';
ELSE      : 'else';
FALSE     : 'false';
IF        : 'if';
INSTANCEOF: 'instanceof';
LEN       : 'len';
NEW       : 'new';
NULL      : 'null';
READINT   : 'readInt';
READFLOAT : 'readFloat';
PRINT     : 'print';
PRINTLN   : 'println';
PRINTLNX  : 'printlnx';
PRINTX    : 'printx';
PROTECTED : 'protected';
RETURN    : 'return';
THIS      : 'this';
TRUE      : 'true';
WHILE     : 'while';

//**********************
// Identifiers
//**********************
fragment LETTER: 'a' .. 'z' | 'A' .. 'Z';
fragment DIGIT : '0' .. '9';
IDENT : (LETTER | '$' | '_') (LETTER | DIGIT | '$' | '_')*;

//**********************
// Special symbols
//**********************
LT     : '<';
GT     : '>';
EQUALS : '=';
PLUS   : '+';
MINUS  : '-';
TIMES  : '*';
SLASH  : '/';
PERCENT: '%';
DOT    : '.';
COMMA  : ',';
OPARENT: '(';
CPARENT: ')';
OBRACE : '{';
CBRACE : '}';
OSQBRA : '[';
CSQBRA : ']';
EXCLAM : '!';
SEMI   : ';';
EQEQ   : '==';
NEQ    : '!=';
GEQ    : '>=';
LEQ    : '<=';
AND    : '&&';
OR     : '||';

//**********************
// Integer literals
//**********************
fragment POSITIVE_INT : '1' .. '9';
INT : '0' | (POSITIVE_INT DIGIT*);

//**********************
// Float literals
//**********************
fragment NUM     : DIGIT+;
fragment SIGN    : '+' | '-' | /* epsilon */ ;
fragment EXP     : ('E' | 'e') SIGN NUM;
fragment DEC     : NUM '.' NUM;
fragment FLOATDEC: (DEC | (DEC EXP)) ('F' | 'f' | /* epsilon */ );
fragment DIGITHEX: '0' .. '9' | 'a' .. 'f' | 'A' .. 'F';
fragment NUMHEX  : DIGITHEX+;
fragment FLOATHEX: ('0x' | '0X') NUMHEX '.' NUMHEX ('P' | 'p') SIGN NUM ('F' | 'f' | /* epsilon */ );
FLOAT : FLOATDEC | FLOATHEX;

//**********************
// Strings
//**********************
fragment STRING_CAR: ~('"' | '\\' | '\n');
STRING            : '"' (STRING_CAR | '\\"' | '\\\\')* '"';
MULTI_LINE_STRING : '"' (STRING_CAR | EOL | '\\"' | '\\\\')* '"';

//**********************
// Comments
//**********************
fragment MULTI_LINE_COMMENT: '/*' .*? '*/';
fragment MONO_LINE_COMMENT : '//' .*? (EOL | EOF);
fragment COMMENT           : MULTI_LINE_COMMENT | MONO_LINE_COMMENT;

//**********************
// Separators
//**********************
SEP : (' ' | '\t' | '\n' | '\r' | COMMENT) { skip(); };

//**********************
// File inclusion
//**********************
fragment FILENAME: (LETTER | DIGIT | '.' | '-' | '_')+;
INCLUDE : '#include' ' '* '"' FILENAME '"' { doInclude(getText()); };
