package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;

/**
 * Type defined by a table.
 *
 * @author gl46
 * @date 22/01/2021
 */
public class TabType extends Type {
    
    protected TabDefinition definition;
    
    public TabDefinition getDefinition() {
        return this.definition;
    }

    public TabType(Symbol name) {
        super(name);
    }
    
    @Override
    public boolean isTab() {
        return true;
    }
    
    /**
     * Standard creation of a type table.
     */
    public TabType(Symbol tabName, Type variableType, Location location) {
        super(tabName);
        this.definition = new TabDefinition(this, variableType, location);
    }

    @Override
    public boolean sameType(Type otherType) {
        return otherType.isTab() && this.getDefinition().getVarType().sameType(((TabType) otherType).getDefinition().getVarType());
    }
}