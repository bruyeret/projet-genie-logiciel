package fr.ensimag.deca.context;

import java.util.ArrayList;
import java.util.List;

/**
 * Signature of a method (i.e. list of arguments)
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Signature {
    List<Type> args = new ArrayList<Type>();

    public void add(Type t) {
        args.add(t);
    }
    
    public Type paramNumber(int n) {
        return args.get(n);
    }
    
    public int size() {
        return args.size();
    }
    
    public void decapitate(){
        if (this.size() != 0){
            args.remove(0);
        }
    }
    
    public void displaySig(){
        System.out.println("Signature : ");
        for (Type elt : args){
            System.out.println(elt);
        }
    }
    
    @Override
    public boolean equals(Object other){
        if (other instanceof Signature){
            boolean same = true;
            int idx = 0;
            for (idx = 0 ; idx < this.size() ; idx++){
                if (idx < ((Signature) other).size()){
                    if (!this.paramNumber(idx).sameType(((Signature) other).paramNumber(idx))){
                        same = false;
                    }
                } else {
                    same = false;
                }
            }
            if (((Signature) other).size() != idx){
                same = false;
            }
            return same;
        }
        return false;
    }

}
