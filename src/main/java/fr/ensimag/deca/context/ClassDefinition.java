package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.RegisterOffset;

import org.apache.commons.lang.Validate;

/**
 * Definition of a class.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ClassDefinition extends TypeDefinition {
    
     private int counterMethod;
     
    public int getCounterMethod() {
        return this.counterMethod;
    }
    
    public int setCounterMethod(int c) {
        return this.counterMethod = c;
    }
    
    
    /**
     * Address of the first cell of the object in the methods table 
     */
    private RegisterOffset methodTableAddress = null;
    
    public RegisterOffset getMethodTableAddress() {
        Validate.isTrue(methodTableAddress != null,
                "setMethodTableAddress() should have been called before");
        return methodTableAddress;
    }
    
    public void setMethodTableAddress(RegisterOffset methodTableAddress) {
        Validate.notNull(methodTableAddress);
        this.methodTableAddress = methodTableAddress;
    }

    public void setNumberOfFields(int numberOfFields) {
        this.numberOfFields = numberOfFields;
    }

    public int getNumberOfFields() {
        return numberOfFields;
    }

    public void incNumberOfFields() {
        this.numberOfFields++;
    }

    public int getNumberOfMethods() {
        return numberOfMethods;
    }

    public void setNumberOfMethods(int n) {
        Validate.isTrue(n >= 0);
        numberOfMethods = n;
    }
    
    public int incNumberOfMethods() {
        numberOfMethods++;
        return numberOfMethods;
    }

    private int numberOfFields = 0;
    private int numberOfMethods = 0;
    
    @Override
    public boolean isClass() {
        return true;
    }
    
    @Override
    public ClassType getType() {
        // Cast succeeds by construction because the type has been correctly set
        // in the constructor.
        return (ClassType) super.getType();
    };

    public ClassDefinition getSuperClass() {
        return superClass;
    }

    private final EnvironmentExp members;
    private final ClassDefinition superClass; 
    private Label[] labelTable;
    private Label initLabel;

    public EnvironmentExp getMembers() {
        return members;
    }
    
    /**
     * Returns the method label table used for code generation
     * it is generated if it was not yet generated
     * @return
     */
    public Label[] getLabelTable() {
        if (labelTable == null) {
            labelTable = members.getLabelTable(numberOfMethods);
        }
        return labelTable;
    }

    public Label getInitLabel() {
        Validate.isTrue(initLabel != null,
                "setInitLabel() should have been called before");
        return initLabel;
    }
    
    public ClassDefinition(ClassType type, Location location, ClassDefinition superClass) {
        super(type, location);
        EnvironmentExp parent;
        if (superClass != null) {
            parent = superClass.getMembers();
        } else {
            parent = null;
        }
        members = new EnvironmentExp(parent);
        this.superClass = superClass;
    }

    /**
     * Generates the initialization label given the name of the class
     * @param className
     */
    public void createLabel(String className) {
        initLabel= new Label("init." + className);
    }
    
}
