package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;

/**
 * Type defined by a class.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ClassType extends Type {
    
    protected ClassDefinition definition;
    
    public ClassDefinition getDefinition() {
        return this.definition;
    }
            
    @Override
    public ClassType asClassType(String errorMessage, Location l) {
        return this;
    }

    @Override
    public boolean isClass() {
        return true;
    }

    @Override
    public boolean isClassOrNull() {
        return true;
    }

    /**
     * Standard creation of a type class.
     */
    public ClassType(Symbol className, Location location, ClassDefinition superClass) {
        super(className);
        this.definition = new ClassDefinition(this, location, superClass);
    }

    /**
     * Creates a type representing a class className.
     * (To be used by subclasses only)
     */
    protected ClassType(Symbol className) {
        super(className);
    }
    

    @Override
    public boolean sameType(Type otherType) {
        return otherType.isClass() && (this.getDefinition() == ((ClassType) otherType).getDefinition());
    }

    /*
    * Returns whether this is a subclass of other
    */
    public boolean isSubClassOf(ClassType other) {
        ClassDefinition defThis = this.getDefinition();
        ClassDefinition defOther = other.getDefinition();
        while (defThis != null && defThis.getType() != defOther.getType()) {
            defThis = defThis.getSuperClass();
        }
        if (defThis == null) {
            return false;
        }
        return defThis.getType() == defOther.getType();
    }

}
