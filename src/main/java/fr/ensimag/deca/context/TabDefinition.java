package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;

/**
 * Definition of a class.
 *
 * @author gl46
 * @date 21/01/2021
 */
public class TabDefinition extends TypeDefinition {
    
    private Type typeVar;

    public TabDefinition(TabType type, Type typeVariable, Location location) {
        super(type , location);
        this.typeVar = typeVariable;
    }
    
    public Type getVarType(){
        return typeVar;
    }
    
    @Override
    public boolean isTab() {
        return true;
    }

    @Override
    public String getNature() {
        return "table";
    }

    @Override
    public boolean isExpression() {
        return true;
    }
}