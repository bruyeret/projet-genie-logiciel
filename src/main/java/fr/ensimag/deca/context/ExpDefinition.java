package fr.ensimag.deca.context;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.DAddr;

/**
 * Definition associated to identifier in expressions.
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class ExpDefinition extends Definition {

    public void setOperand(DAddr operand) {
        Validate.notNull(operand);
        this.operand = operand;
    }

    public DAddr getOperand() {
        if (operand == null) {
            throw new DecacInternalError("setOperand was not called on expression of type " + getType() + " defined at " + this.getLocation() + " and of nature " + getNature());
        }
        return operand;
    }
    private DAddr operand;

    public ExpDefinition(Type type, Location location) {
        super(type, location);
    }

}
