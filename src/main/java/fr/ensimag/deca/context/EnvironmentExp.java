package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.Label;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Dictionary associating identifier's ExpDefinition to their names.
 * 
 * This is actually a linked list of dictionaries: each EnvironmentExp has a
 * pointer to a parentEnvironment, corresponding to superblock (eg superclass).
 * 
 * The dictionary at the head of this list thus corresponds to the "current" 
 * block (eg class).
 * 
 * Searching a definition (through method get) is done in the "current" 
 * dictionary and in the parentEnvironment if it fails. 
 * 
 * Insertion (through method declare) is always done in the "current" dictionary.
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class EnvironmentExp {
    Map<Symbol, ExpDefinition> dictionary;

    EnvironmentExp parentEnvironment;
    
    public EnvironmentExp(EnvironmentExp parentEnvironment) {
        this.parentEnvironment = parentEnvironment;
        this.dictionary = new HashMap<Symbol, ExpDefinition>();
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     */
    public ExpDefinition get(Symbol key) {
        if (this.dictionary.get(key) != null) {
            return this.dictionary.get(key);
        }
        else if (this.parentEnvironment != null && this.parentEnvironment.get(key) != null) {
            return this.parentEnvironment.get(key);
        }
        return null;
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     * 
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary 
     * - or, hides the previous declaration otherwise.
     * 
     * @param name
     *            Name of the symbol to define
     * @param def
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(Symbol name, ExpDefinition def) throws DoubleDefException {
        if (this.dictionary.containsKey(name)) {
            throw new DoubleDefException();
        }
        this.dictionary.put(name, def);       
    }
    
    public void deleteSymbol(Symbol name) throws DoubleDefException {
        if (this.dictionary.containsKey(name)) {
            this.dictionary.remove(name);
        } else {
            throw new DoubleDefException();
        }   
        
    }
    
    /**
     * Disjoint Union between the current EnvExp and the given one.
     * The given one will contain the disjoint union at the end
     * For example, if env1.disjointUnion(env2) is called,
     * then env2 will contain the union between env1 and env2
     * @param env 
     */
    public void disjointUnion(EnvironmentExp env) throws DoubleDefException{
        // iteration on the elements of the current environment
        for (Symbol elt : this.dictionary.keySet()){
            ExpDefinition myDef = this.get(elt);
            try{
                env.declare(elt, myDef);
            } catch (DoubleDefException e){
//                throw new DecacInternalError("Internal error : the two environment are not disjoint, the union is impossible");
                throw new DoubleDefException();
            }
        }
    }
    
    /**
     * Empilement of two EnvExp.
     * The returned env newOne will contain the stacking of env1 on env2
     * @param env 
     */
    public EnvironmentExp stacking(EnvironmentExp env){
        EnvironmentExp newOne = new EnvironmentExp(env);
        env.stackingInPlace(newOne);
        for (Symbol elt : this.dictionary.keySet()){
            ExpDefinition myDef = this.get(elt);
            try{
               newOne.declare(elt, myDef);
            } catch (DoubleDefException e){
                newOne.alter(elt, myDef); 
            }
        }
        return newOne;
    }
    
    /**
     * Empilement of two EnvExp.
     * The given one will contain the disjoint union at the end
     * For example, if env1.stackingInPlace(env2) is called,
     * then env2 will contain the stacking of env1 on env2
     * @param env 
     */
    public void stackingInPlace(EnvironmentExp env){
        for (Symbol elt : this.dictionary.keySet()){
            ExpDefinition myDef = this.get(elt);
            try{
               env.declare(elt, myDef);
            } catch (DoubleDefException e){
                env.alter(elt, myDef); 
            }
        }
    }
    
    /**
     * Replace the element which has key nom and give it the ExpDefinition def
     */
    public void alter(Symbol nom, ExpDefinition def) {
        if (this.dictionary.containsKey(nom)) {
            this.dictionary.replace(nom, def);  
        } else {
            this.dictionary.put(nom, def);
        }
    }

    /**
     * Returns an array of labels for generating the method table
     * @param numberOfMethods
     * @return a Label array contain the method labels
     * for the n first methods of a class whose environment is this
     */
    public Label[] getLabelTable(int numberOfMethods) {
        int methodCounter = 0;
        // Initialized to a null array
        Label[] labelTable = new Label[numberOfMethods];
        EnvironmentExp currentEnv = this;
        // Exploring the current class environment and its superclass(es)
        while (currentEnv != null) {
            for (ExpDefinition def : currentEnv.values()) {
                if (def.isMethod()) {
                    MethodDefinition methodDef = (MethodDefinition) def;
                    // If the method is not already (re)defined
                    if (methodDef.getIndex() < numberOfMethods && labelTable[methodDef.getIndex()] == null) {
                        labelTable[methodDef.getIndex()] = methodDef.getCodeLabel();
                        methodCounter++;
                        if (methodCounter == numberOfMethods) {
                            return labelTable;
                        }
                    }
                }
            }
            currentEnv = currentEnv.parentEnvironment;
        }
        return labelTable;
    }
    
    public Collection<ExpDefinition> values() {
        return dictionary.values();
    }
    
    /**
     * Print the EnvironmentExp on which the method is called.
     * For example : 
     * EnvExp : BEGIN
     *	 g	 - 	 method defined at [9, 1], type=void
     *   x	 - 	 field defined at [2, 5], type=int
     *	 f	 - 	 method defined at [4, 1], type=void
     *	 equals	 - 	 method (builtin), type=boolean
     *	 END
     */
    public void helpMePrint(){
        System.out.println(this.helpMePrintString());
    }
    
    /**
     * Print in a returned String the EnvironmentExp on which the method is called
     * It prints the symbols and definition of the currents class, and the ones from
     * the parent classes, if not already print
     */
    public String helpMePrintString(){
        String message = "EnvExp : BEGIN\n";
        Set<Symbol> alreadyPrint = new HashSet<Symbol>();
        for (Symbol elt : this.dictionary.keySet()){
            alreadyPrint.add(elt);
            message = message + "\t " + elt.getName();
            message = message + "\t - ";
            message = message + "\t " + this.get(elt) + "\n";
        }
        if (parentEnvironment != null){
            parentEnvironment.helpMePrintStringRec(alreadyPrint, message);
        }
        message = message + "\t END\n";
        return message;
    }
    
    /**
     * Recursive function to print the EnvironmmentExp in a String
     */
    public void helpMePrintStringRec(Set<Symbol> alreadyPrint, String message){
        for (Symbol elt : this.dictionary.keySet()){
            if (!alreadyPrint.contains(elt)){
                alreadyPrint.add(elt);
                message = message + "\t " + elt.getName();
                message = message + "\t - ";
                message = message + "\t " + this.get(elt) + "\n";
            }
        }
        if (parentEnvironment != null){
            parentEnvironment.helpMePrintStringRec(alreadyPrint, message);
        }
    }
}
