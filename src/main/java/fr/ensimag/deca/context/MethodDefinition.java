package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.Label;
import org.apache.commons.lang.Validate;

/**
 * Definition of a method
 *
 * @author gl46
 * @date 01/01/2021
 */
public class MethodDefinition extends ExpDefinition {

    @Override
    public boolean isMethod() {
        return true;
    }

    public String getName() {
        Validate.isTrue(name != null,
                "createLabels() should have been called before");
        return name;
    }
    
    public Label getCodeLabel() {
        Validate.isTrue(codeLabel != null,
                "createLabels() should have been called before");
        return codeLabel;
    }
    
    public Label getEndLabel() {
        Validate.isTrue(endLabel != null,
                "createLabels() should have been called before");
        return endLabel;
    }

    public void createLabels(String className, String methodName) {
        name = className.toString() + '.' + methodName;
        codeLabel = new Label("code." + name);
        endLabel = new Label("end." + name);
    }

    public int getIndex() {
        return index;
    }
    
    public void setIndex(int idx) {
        this.index = idx;
    }

    private int index;

    @Override
    public MethodDefinition asMethodDefinition(String errorMessage, Location l)
            throws ContextualError {
        return this;
    }

    private final Signature signature;
    private String name;
    private Label codeLabel;
    private Label endLabel;
    
    /**
     * 
     * @param type Return type of the method
     * @param location Location of the declaration of the method
     * @param signature List of arguments of the method
     * @param index Index of the method in the class. Starts from 0.
     */
    public MethodDefinition(Type type, Location location, Signature signature, int index) {
        super(type, location);
        this.signature = signature;
        this.index = index;
    }

    public Signature getSignature() {
        return signature;
    }

    @Override
    public String getNature() {
        return "method";
    }

    @Override
    public boolean isExpression() {
        return false;
    }

}
