package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;

/**
 * Deca Type (internal representation of the compiler)
 *
 * @author gl46
 * @date 01/01/2021
 */

public abstract class Type {


    /**
     * True if this and otherType represent the same type (in the case of
     * classes, this means they represent the same class).
     * @param otherType another type
     * @return both types represent the same type or the same class
     */
    public abstract boolean sameType(Type otherType);

    private final Symbol name;

    public Type(Symbol name) {
        this.name = name;
    }

    public Symbol getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName().toString();
    }

    public boolean isClass() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isBoolean() {
        return false;
    }

    public boolean isVoid() {
        return false;
    }

    public boolean isString() {
        return false;
    }

    public boolean isNull() {
        return false;
    }

    public boolean isClassOrNull() {
        return false;
    }
    
    public boolean isTab() {
        return false;
    }

    /**
     * Returns the same object, as type ClassType, if possible.Throws
 ContextualError(errorMessage, l) otherwise.Can be seen as a cast, but throws an explicit contextual error when the
 cast fails.
     * @param errorMessage
     * @param l
     * @return 
     * @throws fr.ensimag.deca.context.ContextualError
     */
    public ClassType asClassType(String errorMessage, Location l)
            throws ContextualError {
        throw new ContextualError(errorMessage, l);
    }

    /**
     * Returns wether the other is a subtype of this according to
     * "Relation de sous-typage", page 77 of poly-projet-GL.pdf
     * T1.isSubtypeVanilla(T2) corresponds to subtype(env, T1, T2)
     * @param other the supposed mother type of this
     * @return this is a subtype of other
     */
    private boolean isSubtypeVanilla(Type other) {
        // Pour tout type T, T est un sous-type de T
        if (this.sameType(other)) {
            return true;
        }
        // Pour toute classe A, null est un sous-type de type_class(A).
        if (this.isNull() && other.isClass()) {
            return true;
        }
        // Vérification de relation d'héritage
        if (this.isClass() && other.isClass() && ((ClassType) this).isSubClassOf((ClassType) other)) {
            return true;
        }
        // Les if sont redondants mais on conserve la clarté du code
        return false;
    }
    
    /**
     * Subtype but with tabs too
     * @param other
     * @return 
     */
    public boolean isSubtype(Type other) {
        if (other == null){
            return false;
        }
        if (this.isTab() && other.isTab()) {
            Type thisType = ((TabType) this).getDefinition().getVarType();
            Type otherType = ((TabType) other).getDefinition().getVarType();
            return thisType.isSubtypeVanilla(otherType);
        } else {
            return this.isSubtypeVanilla(other);
        }
    }
    
    
    /**
     * check whether an object of type "this" can get a value of type "other"
     * example : a = b; is correct if type(a).assignCompatibleVanilla(type(b))
     * cf.page 77 "Compatibilité pour l’affectation"
     * T1.assignCompatibleVanilla(T2) corresponds to assign_compatible(env, T1, T2);
     * @param other type we are trying to associate to this
     * @return "this" can get a value of type "other"
     */
    private boolean assignCompatibleVanilla(Type other){
        if ((this.isFloat() && other.isInt()) || this.sameType(other)) {
            return true;
        }
        if (other.isSubtype(this)) {
            return true;
        }
        // Les if sont redondants mais on conserve la clarté du code
        return false;
    }
    
    /**
     * assignCompatible with tabs too
     * @param other
     * @return 
     */
    public boolean assignCompatible(Type other) {
        if (this.isTab() && other.isTab()) {
            Type thisType = ((TabType) this).getDefinition().getVarType();
            Type otherType = ((TabType) other).getDefinition().getVarType();
            return thisType.assignCompatibleVanilla(otherType);
        } else {
            return this.assignCompatibleVanilla(other);
        }
    }
    
    
    /**
     * verify if the cast is possible (3.40)
     * équivaut à poly page 77 : cast_compatible(env, this, other)
     */
    public boolean castCompatible(Type other) throws ContextualError{
        return (!this.isVoid() && (this.assignCompatible(other) || other.assignCompatible(this)));
    }
}


