package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.deca.DecacCompiler;
import java.util.HashMap;
import java.util.Map;

/**
 * Dictionary associating identifier's TypeDefinition to their names.
 * @author gl46
 * @date 01/01/2021
 */
public class EnvironmentType {
    
    Map<String, TypeDefinition> dictionary;
    
    private VoidType typeVoid;
    private BooleanType typeBoolean;
    private IntType typeInt;
    private FloatType typeFloat;
    private StringType typeString;
    private NullType typeNull;
    private ClassType typeObject;
    
    // Private Constructor to force the creation of a predefined environment
    public EnvironmentType() {
        this.dictionary = new HashMap<String, TypeDefinition>();
    }

    // Predefined environment getter
    public void getPredefined(DecacCompiler compiler) {
        // Creation of the symbols
        Symbol V = compiler.getSymbolTable().create("void");
        Symbol B = compiler.getSymbolTable().create("boolean");
        Symbol Bt = compiler.getSymbolTable().create("boolean[]");
        Symbol F = compiler.getSymbolTable().create("float");
        Symbol Ft = compiler.getSymbolTable().create("float[]");
        Symbol I = compiler.getSymbolTable().create("int");
        Symbol It = compiler.getSymbolTable().create("int[]");
        Symbol S = compiler.getSymbolTable().create("string");
        Symbol N = compiler.getSymbolTable().create("null");
        Symbol O = compiler.getSymbolTable().create("Object");
        Symbol Ot = compiler.getSymbolTable().create("Object[]");
        // Creation of types
        VoidType typeV = new VoidType(V);
        BooleanType typeB = new BooleanType(B);
        TabType typeBt = new TabType(Bt, typeB, Location.BUILTIN);
        FloatType typeF = new FloatType(F);
        TabType typeFt = new TabType(Ft, typeF, Location.BUILTIN);
        IntType typeI = new IntType(I);
        TabType typeIt = new TabType(It, typeI, Location.BUILTIN);
        StringType typeS = new StringType(S);
        NullType typeN = new NullType(N);
        ClassType typeO = new ClassType(O, Location.BUILTIN, null);
        TabType typeOt = new TabType(Ot, typeO, Location.BUILTIN);
        // Affectation 
        this.typeVoid = typeV;
        this.typeInt = typeI;
        this.typeFloat = typeF;
        this.typeString = typeS;
        this.typeNull = typeN;
        this.typeBoolean = typeB;
        this.typeObject = typeO;
        // Creation of the definitions
        TypeDefinition defV = new TypeDefinition(typeV, Location.BUILTIN);
        TypeDefinition defB = new TypeDefinition(typeB, Location.BUILTIN);
        TypeDefinition defF = new TypeDefinition(typeF, Location.BUILTIN);
        TypeDefinition defI = new TypeDefinition(typeI, Location.BUILTIN);
        ClassDefinition defO = typeO.getDefinition();
        TabDefinition defBt = typeBt.getDefinition();
        TabDefinition defIt = typeIt.getDefinition();
        TabDefinition defFt = typeFt.getDefinition();
        TabDefinition defOt = typeOt.getDefinition();
        // Initialisation de la définition de Object
        Signature sig = new Signature();
        sig.add(typeObject);
        MethodDefinition methDef = new MethodDefinition(typeBoolean, Location.BUILTIN, sig, 0);
        Symbol eq = compiler.getSymbolTable().create("equals");
        try {
            defO.getMembers().declare(eq, methDef);
            defO.incNumberOfMethods();
        } catch (EnvironmentExp.DoubleDefException ex) {}
        // Declaration of the symbols and typeDefinitions
        try {
            this.declare("void", defV);
            this.declare("boolean", defB);
            this.declare("float", defF);
            this.declare("int", defI);
            this.declare("Object", defO);
            this.declare("int[]", defIt);
            this.declare("float[]", defFt);
            this.declare("boolean[]", defBt);
            this.declare("Object[]", defOt);
        } catch (DoubleDefException e) {}
    }
    
    
     // getter attribut typeClass
    public ClassType getTypeObject() {
        return this.typeObject;
    }
    
    // getter attribut typeVoid
    public VoidType getTypeVoid() {
        return this.typeVoid;
    }
    
    // getter attribut typeInt
    public IntType getTypeInt() {
        return this.typeInt;
    }
    
    // getter attribut typeBoolean
    public BooleanType getTypeBoolean() {
        return this.typeBoolean;
    }
    
    // getter attribut typeFloat
    public FloatType getTypeFloat() {
        return this.typeFloat;
    }
    
    // getter attribut typeString
    public StringType getTypeString() {
        return this.typeString;
    }
    
    // getter attribut typeNull
    public NullType getTypeNull() {
        return this.typeNull;
    }
    
    // getter attribut type tab of the given one, and null if not existing
    public TabType getTypeTab(Type type){
        TypeDefinition myDef = this.getDef(type.getName().getName() + "[]");
        if (myDef == null){
            return null;
        }
        return (TabType) ((TabDefinition) myDef).getType();
    }
    
    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     */
    public TypeDefinition getDef(String nom){
        return this.dictionary.get(nom);
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     * 
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary 
     * - or, hides the previous declaration otherwise.
     * 
     * @param name
     *            Name of the symbol to define
     * @param def
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(String nom, TypeDefinition def) throws DoubleDefException {
        if (this.dictionary.containsKey(nom)) {
            throw new DoubleDefException();
       }
        this.dictionary.put(nom, def);       
    }
    
    public void alter(String nom, TypeDefinition def) {
        this.dictionary.replace(nom, def);    
    }
    
    /**
     * Print in the returned String the EnvironmentType of the program.
     */
    public String helpMePrint(){
        String message = "";
        message = message + "EnvType : BEGIN \n ";
        for (String elt : this.dictionary.keySet()){
            message = message + "\t " + elt ;
            message = message + "\t - ";
            message = message + "\t " + this.getDef(elt) + "\n";
        }
        message = message + "\tEND\n";
        return message;
    }

}
