package fr.ensimag.deca;

import java.io.File;
import org.apache.log4j.Logger;

/**
 * Main class for the command-line Deca compiler.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class DecacMain {
    private static Logger LOG = Logger.getLogger(DecacMain.class);
    
    public static void main(String[] args) {
        // example log4j message.
        LOG.info("Decac compiler started");
        boolean error = false;
        final CompilerOptions options = new CompilerOptions();
        try {
            options.parseArgs(args);
        } catch (CLIException e) {
            System.err.println("Error during option parsing:\n"
                    + e.getMessage());
            options.displayUsage();
            System.exit(1);
        }
        if (options.getPrintBanner()) {
        	options.displayBanner();
        }
        else if (options.getPrintHelp()) {
        	options.displayHelp();
        }
        else if (options.getSourceFiles().isEmpty()) {
        	System.out.println("No source file to compile.");
        	options.displayHelp();
            System.exit(1);
        }
        if (options.getParallel()) {
        	for (File source : options.getSourceFiles()) {
        		Thread thread = new Thread() {
        			public void run() {
                        DecacCompiler compiler = new DecacCompiler(options, source);
                        compiler.compile();
        			}
        		};
        		thread.start();
            }
        	// Warning : Error code is not set when compiling in parallel.
        }
        else {
            for (File source : options.getSourceFiles()) {
                DecacCompiler compiler = new DecacCompiler(options, source);
                if (compiler.compile()) {
                    error = true;
                }
            }
        }
        System.exit(error ? 1 : 0);
    }
}
