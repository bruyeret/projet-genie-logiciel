package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.SEQ;
import fr.ensimag.ima.pseudocode.instructions.STORE;

public class MethodTable {
    /**
     * Generates code that fills the Object class method table
     * Note: needs to be called before codeGenMethodTable for other classes
     * @param compiler
     */
    public static void codeGenObject(DecacCompiler compiler) {
        // Creating labels for Object.equals
        ClassDefinition classDef = compiler.getEnvType().getTypeObject().getDefinition();
        classDef.createLabel("Object");
        // Object environment only contains equals
        MethodDefinition equalsDef = null;
        for (ExpDefinition def : classDef.getMembers().values()) {
            equalsDef = (MethodDefinition) def;
            equalsDef.createLabels("Object", "equals");
        }
        
        compiler.addComment("Methods table for class Object");
        // LOAD #null, R0 (Object has no superclass)
        compiler.addInstruction(new LOAD(new NullOperand(), Register.R0));
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(1, Register.GB)));
        compiler.addInstruction(new LOAD(equalsDef.getCodeLabel(), Register.R0));
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(2, Register.GB)));
        // Next method table will start at 3(GB)
        compiler.setGBOffset(3);
        // Setting the method table address to 1(GB)
        ClassDefinition objectDef = compiler.getEnvType().getTypeObject().getDefinition();
        objectDef.setMethodTableAddress(new RegisterOffset(1, Register.GB));
    }
    
    /**
     * Generates code for the Object.equals method
     * -2(LB) contains the address of the object on which the method is called
     * -3(LB) contains the address of the object passed in
     * @see src/main/resources/Object.equals.txt for more details
     * @param compiler
     */
    public static void codeGenEquals(DecacCompiler compiler) {
        compiler.newBlock();
        compiler.addLabel("code.Object.equals");
        // LOAD -2(LB), R0
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));
        // CMP -3(LB), R0
        compiler.addInstruction(new CMP(new RegisterOffset(-3, Register.LB), Register.R0));
        // SEQ R0
        compiler.addInstruction(new SEQ(Register.R0));
        compiler.appendBlock(false, true);
    }
    
    /**
     * Generates code for a the method table of a class
     * Note: codeGenObject needs to be called beforehand
     * @param compiler
     * @param classDef
     */
    public static void codeGenMethodTable(DecacCompiler compiler, ClassDefinition classDef) {
        int baseOffset = compiler.getGBOffset();
        RegisterOffset baseStackCell = new RegisterOffset(baseOffset, Register.GB);
        classDef.setMethodTableAddress(baseStackCell);
        
        // Loading the address of the superclass' method table in to R0
        compiler.addInstruction(new LEA(classDef.getSuperClass().getMethodTableAddress(), Register.R0));
        // Storing it into the first cell of the current object's method table
        compiler.addInstruction(new STORE(Register.R0, baseStackCell));
        
        // Generating the method label table
        Label[] labels = classDef.getLabelTable();
        // Generating code for each method
        for (int i = 0; i < classDef.getNumberOfMethods(); i++) {
            if (labels[i] == null) {
                throw new DecacInternalError("No method found with number " + i);
            }
            // Loading the address of the method into R0
            compiler.addInstruction(new LOAD(labels[i], Register.R0));
            // Storing it into the first cell of the current object's method table
            compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(baseOffset + i + 1, Register.GB)));
        }
        
        // Updating the baseOffset
        compiler.setGBOffset(baseOffset + classDef.getNumberOfMethods() + 1);
    }
}
