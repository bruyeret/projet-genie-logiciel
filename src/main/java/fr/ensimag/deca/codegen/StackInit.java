package fr.ensimag.deca.codegen;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.SUBSP;
import fr.ensimag.ima.pseudocode.instructions.TSTO;

/**
 * Class to generate code for stack initialization
 * has a default constructor (all attributes with 0 value)
 */
public class StackInit {
    private int temporaryCompute;
    /**
     * When evaluating expressions, this will
     * be the max value of either the number of
     * parameters to push on stack for method calls,
     * or the number of temporary data to push into
     * the stack when evaluating large arithmetical
     * expressions
     */
    private int tmpAndParams;
    private int variables;
    private int registers;
    private int methodTable;
    
    public void setMethodTable(int methodTable) {
        Validate.isTrue(methodTable > 0);
        this.methodTable = methodTable;
    }
    
    /**
     * Set the max number of parameters to put on stack for method calls
     * Notes:
     * - When setting this parameter, the 2 added stack pushes done
     * when calling BSR have to be added (LB and PC are saved).
     * - The object on which the method is called is also a parameter.
     * @param parameters
     */
    public void setMaxParameters(int parameters) {
        Validate.isTrue(parameters >= 0);
        if (parameters > tmpAndParams) {
            tmpAndParams = parameters;
        }
    }
    
    /**
     * Increments the number of temporary values to store
     * on stack for the current expression evaluation.
     * Note: the actual number of temporary cells necessary
     * to generate the overflow check will be the max
     */
    public void incTemporary() {
        temporaryCompute++;
        if (temporaryCompute > tmpAndParams) {
            tmpAndParams = temporaryCompute;
        }
    }
    
    /**
     * Decrements the number of temporary values to store
     * on stack for the current expression evaluation.
     */
    public void decTemporary() {
        temporaryCompute--;
    }
    
    /**
     * Sets the number of variables necessary.
     * @param variables
     */
    public void setVariables(int variables) {
        Validate.isTrue(variables >= 0);
        this.variables = variables;
    }
    
    /**
     * Sets the number of registers to be saved
     * @param registers
     */
    public void setMaxRegisters(int registers) {
        Validate.isTrue(registers >= 0);
        if (registers > this.registers) {
            this.registers = registers;
        }
    }
    
    /**
     * Generates code for stack initialization at the
     * start of the current block of code in the compiler
     * @param compiler
     */
    public void codeGenStackInit(DecacCompiler compiler, boolean isMethod) {
        // No need to save registers outside of methods
        if (!isMethod) {
            registers = 0;
        }
        int size = tmpAndParams + variables + registers + methodTable;
        // Must write backwards with addFirst
        if (size > 0) {
            if (isMethod) {
                codeGenRegisters(compiler);
                if (variables > 0) {
                    compiler.addInstruction(new SUBSP(variables));
                }
            }
            if (variables + methodTable > 0) {
                compiler.addFirst(new ADDSP(variables + methodTable));
            }
            if (!compiler.getCompilerOptions().getNoCheck()) {
                compiler.addFirst(new BOV(CodeError.STACK_OVERFLOW.getLabel()));
                compiler.addFirst(new TSTO(size));
                if (registers > 0) {
                    compiler.addFirst(" - registers: " + registers);
                }
                if (variables > 0) {
                    compiler.addFirst(" - variables: " + variables);
                }
                if (tmpAndParams > 0) {
                    compiler.addFirst(" - temporary data and parameters on stack: " + tmpAndParams);
                }
                if (methodTable > 0) {
                    compiler.addFirst(" - method table: " + methodTable);
                }
            }
            else {
            	compiler.addFirst("Overflow check disabled.");
            }
        }
        else {
            compiler.addFirst("Nothing on stack");
        }
        compiler.addFirst("Stack initialization and overflow check:");
    }
    
    /**
     * Generates code at the start and end of the current block
     * that saves registers and restores them at the end.
     * @param compiler
     */
    private void codeGenRegisters(DecacCompiler compiler) {
        if (registers > 0) {
            compiler.addComment("Restoring registers");
            for (int i = registers + 1; i >= 2; i--) {
                // Saving
                compiler.addFirst(new PUSH(Register.getR(i)));
                // Restoring
                compiler.addInstruction(new POP(Register.getR(i)));
            }
            compiler.addFirst("Saving registers");
        }
    }
}
