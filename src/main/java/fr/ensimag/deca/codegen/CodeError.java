/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BGE;
import fr.ensimag.ima.pseudocode.instructions.BLT;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.ERROR;
import fr.ensimag.ima.pseudocode.instructions.WNL;
import fr.ensimag.ima.pseudocode.instructions.WSTR;
import fr.ensimag.ima.pseudocode.Label;

/**
 * Class to generate code for error messages
 */
public class CodeError {
    public static final CodeError ARITHMETIC_OVERFLOW = new CodeError(
            "arithmetic_overflow_error",
            "Error: Overflow encountered during arithmetic operation");
    public static final CodeError STACK_OVERFLOW = new CodeError(
            "stack_overflow_error",
            "Error: Stack overflow");
    public static final CodeError HEAP_OVERFLOW = new CodeError(
            "heap_overflow_error",
            "Error: Heap overflow, not enough memory available for allocation");
    public static final CodeError NULL_DEREFERENCING = new CodeError(
            "null_dereferencing_error",
            "Error: Null pointer dereferencing");
    public static final CodeError ZERO_DIVISION = new CodeError(
            "zero_division_error",
            "Error: Zero division error");
    public static final CodeError IMPOSSIBLE_CAST = new CodeError(
            "impossible_cast_error",
            "Error: Impossible cast encountered");
    public static final CodeError INDEX_ERROR = new CodeError(
            "index_error",
            "Error: Index out of bounds");
    public static final CodeError INPUT_OUTPUT = new CodeError(
            "io_error",
            "Error: Input/Output error");
    
    
    private final Label label;
    private final String message;
    
    private CodeError(String label, String message) {
        this.label = new Label(label);
        this.message = message;
    }
    
    public void codeGenErrorSection(DecacCompiler compiler) {
        compiler.addLabel(label);
        compiler.addInstruction(new WSTR(message));
        compiler.addInstruction(new WNL());
        compiler.addInstruction(new ERROR());
    }
    
    public void codeGenBOV(DecacCompiler compiler) {
        compiler.addInstruction(new BOV(label));
    }
    
    public void codeGenBEQ(DecacCompiler compiler) {
        compiler.addInstruction(new BEQ(label));
    }
    
    public void codeGenBRA(DecacCompiler compiler) {
        compiler.addInstruction(new BRA(label));
    }
    
    public void codeGenBLT(DecacCompiler compiler) {
        compiler.addInstruction(new BLT(label));
    }
    
    public void codeGenBGE(DecacCompiler compiler) {
        compiler.addInstruction(new BGE(label));
    }

    public Label getLabel() {
        return label;
    }
}
