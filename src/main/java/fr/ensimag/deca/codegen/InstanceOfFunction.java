package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

public class InstanceOfFunction {
    public static final Label label = new Label("code.instanceof");
    
    /**
     * Generates the code of the instanceof function
     * -2(LB) should contain the address of the object that is being tested
     * -3(LB) should contain the address of the method table of the class tested
     * @see src/main/resources/instanceof.txt for more details
     * @param compiler
     */
    public static void codeGen(DecacCompiler compiler) {
        Label startWhile = new Label("while_start_instanceof");
        Label condWhile = new Label("while_cond_instanceof");
        Label endIf = new Label("if_end_instanceof");
        Label returnFalse = new Label("return_false_instanceof");
        Label end = new Label("end.instanceof");
        
        compiler.newBlock();
        // code.instanceof:
        compiler.addLabel(label);
        // LOAD -3(LB), R1
        compiler.addInstruction(new LOAD(new RegisterOffset(-3, Register.LB), Register.R1));
        // LOAD -2(LB), R0
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));
        // BEQ return_false_instanceof
        compiler.addInstruction(new BEQ(returnFalse));
        // LOAD 0(R0), R0
        compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.R0), Register.R0));
        // BRA while_cond_instanceof
        compiler.addInstruction(new BRA(condWhile));
        // while_start_instanceof:
        compiler.addLabel(startWhile);
        // CMP R0, R1
        compiler.addInstruction(new CMP(Register.R0, Register.R1));
        // BNE if_end_instanceof
        compiler.addInstruction(new BNE(endIf));
        // LOAD #1, R0
        compiler.addInstruction(new LOAD(1, Register.R0));
        // BRA end.instanceof
        compiler.addInstruction(new BRA(end));
        // if_end_instanceof:
        compiler.addLabel(endIf);
        // LOAD 0(R0), R0
        compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.R0), Register.R0));
        // while_cond_instance_of:
        compiler.addLabel(condWhile);
        // CMP #null, R0
        compiler.addInstruction(new CMP(new NullOperand(), Register.R0));
        // BNE while_start_instance_of
        compiler.addInstruction(new BNE(startWhile));
        // return_false_instanceof:
        compiler.addLabel(returnFalse);
        // LOAD #0, R0
        compiler.addInstruction(new LOAD(0, Register.R0));
        // end.instanceof
        compiler.addLabel(end);
        compiler.appendBlock(false, true);
    }
    
    public static void codeGenBSR(DecacCompiler compiler) {
        compiler.addInstruction(new BSR(label));
    }
}
