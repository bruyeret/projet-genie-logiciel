/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.WFLOAT;
import fr.ensimag.ima.pseudocode.instructions.WFLOATX;
import fr.ensimag.ima.pseudocode.instructions.WINT;
import fr.ensimag.ima.pseudocode.instructions.WSTR;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;

/**
 * Class to generate code for print calls
 */
public class PrintGen {
    /**
     * Generates the instructions for printing a string
    */
    public static void printString(DecacCompiler compiler, String value) {
        compiler.addInstruction(new WSTR(value));
    }
    
    /**
     * Generates the instructions for printing an integer
    */
    public static void printInt(DecacCompiler compiler, int value) {
        // Loading the value into R1
        compiler.addInstruction(new LOAD(value, Register.getR(1)));
        // Printing the content of R1 as an integer
        compiler.addInstruction(new WINT());
    }
    
    /**
     * Generates the instructions for printing a float
    */
    public static void printFloat(DecacCompiler compiler, float value, boolean printHex) {
        // Loading the value into R1
        compiler.addInstruction(new LOAD(value, Register.R1));
        if (printHex) {
            // Printing the content of R1 as a hexadecimal float
            compiler.addInstruction(new WFLOATX()); 
        }
        else {
            // Printing the content of R1 as a decimal float
            compiler.addInstruction(new WFLOAT());
        }
    }
    
    /**
     * Generates the instructions for printing a value
     * calculated into a register based on its type
     */
    public static void printReg(DecacCompiler compiler, GPRegister reg, Type type, boolean printHex) {
        if (type == null) {
            throw new DecacInternalError("Type of expression to print does not exist");
        }
        if (type.isInt()) {
            // Loading the value into R1
            compiler.addInstruction(new LOAD(reg, Register.R1));
            // Printing the content of R1 as a decimal float
            compiler.addInstruction(new WINT());
        }
        else if (type.isFloat()) {
            // Loading the value into R1
            compiler.addInstruction(new LOAD(reg, Register.R1));
            if (printHex) {
                // Printing the content of R1 as a decimal float
                compiler.addInstruction(new WFLOATX()); 
            }
            else {
                // Printing the content of R1 as a decimal float
                compiler.addInstruction(new WFLOAT());
            }
        }
        else {
            throw new DecacInternalError("It is impossible to print type " + type);
        }
    }
}
