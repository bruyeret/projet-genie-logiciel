package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Syntax error for an expression that should be an lvalue (ie that can be
 * assigned), but is not.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class InvalidRounding extends DecaRecognitionException {

    private static final long serialVersionUID = 4670163376041273741L;
    private final String customMessage;

    public InvalidRounding(DecaParser recognizer, ParserRuleContext ctx, String customMessage) {
        super(recognizer, ctx);
        this.customMessage = customMessage;
    }

    @Override
    public String getMessage() {
        return "rounding error : " + this.customMessage;
    }
}
