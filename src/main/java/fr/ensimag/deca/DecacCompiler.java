package fr.ensimag.deca;

import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.codegen.StackInit;
import fr.ensimag.deca.syntax.DecaLexer;
import fr.ensimag.deca.syntax.DecaParser;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.context.EnvironmentType; // for third attribute : need of EnvironmentType objects
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.AbstractProgram;
import fr.ensimag.deca.tree.LocationException;
import fr.ensimag.ima.pseudocode.AbstractLine;
import fr.ensimag.ima.pseudocode.IMAProgram;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.RTS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * Decac compiler instance.
 *
 * This class is to be instantiated once per source file to be compiled. It
 * contains the meta-data used for compiling (source file name, compilation
 * options) and the necessary utilities for compilation (symbol tables, abstract
 * representation of target file, ...).
 *
 * It contains several objects specialized for different tasks. Delegate methods
 * are used to simplify the code of the caller (e.g. call
 * compiler.addInstruction() instead of compiler.getProgram().addInstruction()).
 *
 * @author gl46
 * @date 01/01/2021
 */
public class DecacCompiler {
    private static final Logger LOG = Logger.getLogger(DecacCompiler.class);
    
    private int labelID = 0;
    private int offset = 1;
    
    /**
     * Returns the current labelID and then increments it
     * (used for code generation)
     * @return
     */
    public int getLabelID() {
    	return labelID++;
    }
    
    /**
     * Returns the next index to use when storing the
     * method table and global variables in the stack
     * @return
     */
    public int getGBOffset() {
        return offset;
    }
    
    public void setGBOffset(int offset) {
        Validate.isTrue(offset >= 0);
        this.offset = offset;
    }
    
    /**
     * Portable newline character.
     */
    private static final String nl = System.getProperty("line.separator", "\n");
    
    // The Environment table for the whole program
    private EnvironmentType envTypes;
    
    // Unique instanciation of SymbolTable
    private SymbolTable symbols;
    
    // getter de SymbolTable
    public SymbolTable getSymbolTable() {
        return this.symbols;
    }

    public DecacCompiler(CompilerOptions compilerOptions, File source) {
        super();
        this.symbols = new SymbolTable();
        this.envTypes = new EnvironmentType();
        this.envTypes.getPredefined(this);
        this.compilerOptions = compilerOptions;
        this.source = source;
    }

    /**
     * Source file associated with this compiler instance.
     */
    public File getSource() {
        return source;
    }

    /**
     * Environment types associated with this compiler instance.
     */
    public EnvironmentType getEnvType(){
        return envTypes;
    }
    
    /**
     * Compilation options (e.g. when to stop compilation, number of registers
     * to use, ...).
     */
    public CompilerOptions getCompilerOptions() {
        return compilerOptions;
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#add(fr.ensimag.ima.pseudocode.AbstractLine)
     */
    public void add(AbstractLine line) {
        getCurrentBlock().add(line);
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addComment(java.lang.String)
     */
    public void addComment(String comment) {
        getCurrentBlock().addComment(comment);
    }
    
    /**
     * Adds a comment before the current block
     * @param comment
     */
    public void addProgramComment(String comment) {
        program.addComment(comment);
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addLabel(fr.ensimag.ima.pseudocode.Label)
     */
    public void addLabel(Label label) {
        getCurrentBlock().addLabel(label);
    }
    
    public void addLabel(String label) {
        addLabel(new Label(label));
    }
    
    /**
     * Adds a label before the current block
     * @param label
     */
    public void addProgramLabel(Label label) {
        program.addLabel(label);
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction)
     */
    public void addInstruction(Instruction instruction) {
        getCurrentBlock().addInstruction(instruction);
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction,
     * java.lang.String)
     */
    public void addInstruction(Instruction instruction, String comment) {
        getCurrentBlock().addInstruction(instruction, comment);
    }
    
    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addFirst(fr.ensimag.ima.pseudocode.Instruction)
     */
    public void addFirst(Instruction instruction) {
        getCurrentBlock().addFirst(instruction);
    }
    
    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addFirst(java.lang.String)
     */
    public void addFirst(String comment) {
        getCurrentBlock().addFirst(comment);
    }
    
    /**
     * @see 
     * fr.ensimag.ima.pseudocode.IMAProgram#display()
     */
    public String displayIMAProgram() {
        return program.display();
    }
    
    private final CompilerOptions compilerOptions;
    private final File source;
    /**
     * The main program. Every instruction generated will eventually end up here.
     */
    private final IMAProgram program = new IMAProgram();
    /**
     * Current block of code, structure used to initialize the
     * stack after writing some code
     */
    private IMAProgram currentBlock;
    private StackInit stackInit;
    /**
     * Method associated with the block
     * (can be null if the block is not for a method)
     */
    private MethodDefinition methodDef;
    
    /**
     * Creates a new block with its StackInit object.
     * The previous block will be overwritten so it should
     * be added to the program with appendBlock() beforehand.
     */
    public void newBlock() {
        currentBlock = new IMAProgram();
        stackInit = new StackInit();
    }
    
    /**
     * Generates the stack initialization code for the current block
     * if requested and then adds the block to the program.
     * currentBlock is then set to null to prevent adding it twice.
     * @param doStackInit
     * @param doRTS adds an RTS instruction at the end of the block 
     */
    public void appendBlock(boolean doStackInit, boolean isMethod) {
        checkCurrentBlock();
        if (doStackInit) {
            stackInit.codeGenStackInit(this, isMethod);
        }
        if (isMethod) {
            addInstruction(new RTS());
        }
        program.append(currentBlock);
        currentBlock = null;
        stackInit = null;
        methodDef = null;
    }
    
    /**
     * Throws an error if the block doesn't exist
     */
    private void checkCurrentBlock() {
        if (currentBlock == null) {
            throw new DecacInternalError("No block has been created, can not add instructions.");
        }
    }
    
    private IMAProgram getCurrentBlock() {
        checkCurrentBlock();
        return currentBlock;
    }

    public void setMaxParameters(int parameters) {
        checkCurrentBlock();
        stackInit.setMaxParameters(parameters);
    }
    
    public void incTemporary() {
        checkCurrentBlock();
        stackInit.incTemporary();
    }
    
    public void decTemporary() {
        checkCurrentBlock();
        stackInit.decTemporary();
    }
    
    public void setVariables(int variables) {
        checkCurrentBlock();
        stackInit.setVariables(variables);
    }
    
    public void setMaxRegisters(int registers) {
        checkCurrentBlock();
        stackInit.setMaxRegisters(registers);
    }

    public void setMethodTable(int methodTable) {
        checkCurrentBlock();
        stackInit.setMethodTable(methodTable);
    }
    
    /**
     * Can be called before calling newBlock
     * @param methodDef
     */
    public void setMethodDef(MethodDefinition methodDef) {
        Validate.notNull(methodDef);
        this.methodDef = methodDef;
    }
    
    public MethodDefinition getMethodDef() {
        if (methodDef == null) {
            throw new DecacInternalError("The current block has no associated method definition.");
        }
        return methodDef;
    }
    
    /**
     * Run the compiler (parse source file, generate code)
     *
     * @return true on error
     */
    public boolean compile() {
        String sourceFile = source.getAbsolutePath();
        String destFile = sourceFile.substring(0, sourceFile.length() - 4) + "ass";

        PrintStream err = System.err;
        PrintStream out = System.out;
        LOG.debug("Compiling file " + sourceFile + " to assembly file " + destFile);
        try {
            return doCompile(sourceFile, destFile, out, err);
        } catch (LocationException e) {
            e.display(err);
            return true;
        } catch (DecacFatalError e) {
            err.println(e.getMessage());
            return true;
        } catch (StackOverflowError e) {
            LOG.debug("stack overflow", e);
            err.println("Stack overflow while compiling file " + sourceFile + ".");
            return true;
        } catch (Exception e) {
            LOG.fatal("Exception raised while compiling file " + sourceFile
                    + ":", e);
            err.println("Internal compiler error while compiling file " + sourceFile + ", sorry.");
            return true;
        } catch (AssertionError e) {
            LOG.fatal("Assertion failed while compiling file " + sourceFile
                    + ":", e);
            err.println("Internal compiler error while compiling file " + sourceFile + ", sorry.");
            return true;
        }
    }

    /**
     * Internal function that does the job of compiling (i.e. calling lexer,
     * verification and code generation).
     *
     * @param sourceName name of the source (deca) file
     * @param destName name of the destination (assembly) file
     * @param out stream to use for standard output (output of decac -p)
     * @param err stream to use to display compilation errors
     *
     * @return true on error
     */
    private boolean doCompile(String sourceName, String destName,
            PrintStream out, PrintStream err)
            throws DecacFatalError, LocationException {
        AbstractProgram prog = doLexingAndParsing(sourceName, err);

        if (prog == null) {
            LOG.info("Parsing failed");
            return true;
        }
        assert(prog.checkAllLocations());

        if (compilerOptions.getSteps() == CompilerOptions.PARSE) {
        	prog.decompile(System.out);
        	return false;
        }

        prog.verifyProgram(this);
        assert(prog.checkAllDecorations());
        if (compilerOptions.getSteps() == CompilerOptions.VERIFY) {
        	return false;
        }

        addProgramComment("Main part");
        prog.codeGenProgram(this);
        
        // Error messages section
        newBlock();
        addComment("Error messages");
        if (!compilerOptions.getNoCheck()) {
            CodeError.ARITHMETIC_OVERFLOW.codeGenErrorSection(this);
            CodeError.STACK_OVERFLOW.codeGenErrorSection(this);
            CodeError.HEAP_OVERFLOW.codeGenErrorSection(this);
            CodeError.NULL_DEREFERENCING.codeGenErrorSection(this);
            CodeError.ZERO_DIVISION.codeGenErrorSection(this);
            CodeError.IMPOSSIBLE_CAST.codeGenErrorSection(this);
            CodeError.INDEX_ERROR.codeGenErrorSection(this);
        }
        CodeError.INPUT_OUTPUT.codeGenErrorSection(this);
        appendBlock(false, false);
        
        // Methods section
        addProgramComment("Methods");
        prog.codeGenMethods(this);
        
        LOG.debug("Generated assembly code:" + nl + program.display());
        LOG.info("Output file assembly file is: " + destName);

        FileOutputStream fstream = null;
        try {
            fstream = new FileOutputStream(destName);
        } catch (FileNotFoundException e) {
            throw new DecacFatalError("Failed to open output file: " + e.getLocalizedMessage());
        }

        LOG.info("Writing assembler file ...");

        program.display(new PrintStream(fstream));
        LOG.info("Compilation of " + sourceName + " successful.");
        return false;
    }

    /**
     * Build and call the lexer and parser to build the primitive abstract
     * syntax tree.
     *
     * @param sourceName Name of the file to parse
     * @param err Stream to send error messages to
     * @return the abstract syntax tree
     * @throws DecacFatalError When an error prevented opening the source file
     * @throws DecacInternalError When an inconsistency was detected in the
     * compiler.
     * @throws LocationException When a compilation error (incorrect program)
     * occurs.
     */
    protected AbstractProgram doLexingAndParsing(String sourceName, PrintStream err)
            throws DecacFatalError, DecacInternalError {
        DecaLexer lex;
        try {
            lex = new DecaLexer(CharStreams.fromFileName(sourceName));
        } catch (IOException ex) {
            throw new DecacFatalError("Failed to open input file: " + ex.getLocalizedMessage());
        }
        lex.setDecacCompiler(this);
        CommonTokenStream tokens = new CommonTokenStream(lex);
        DecaParser parser = new DecaParser(tokens);
        parser.setDecacCompiler(this);
        return parser.parseProgramAndManageErrors(err);
    }

}
