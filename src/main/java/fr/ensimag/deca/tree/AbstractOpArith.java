package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Arithmetic binary operations (+, -, /, ...)
 * 
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractOpArith extends AbstractBinaryExpr {

    public AbstractOpArith(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    /**
     * Partial operation for binary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        Type myType = verifyTypeArithOp(compiler, type1, type2);
        return myType;
    }
    
    /**
    * Partial operation for arithmetic operations [SyntaxeContextuelle]
    */
    public Type verifyTypeArithOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        if (type1.isInt() && type2.isInt()) {
            return compiler.getEnvType().getTypeInt();
        }
        else if (type1.isInt() && type2.isFloat()){
            setLeftOperand(new ConvFloat(getLeftOperand()));
            getLeftOperand().setType(compiler.getEnvType().getTypeFloat());
            return compiler.getEnvType().getTypeFloat();
        }
        else if (type1.isFloat() && type2.isInt()) {
            setRightOperand(new ConvFloat(getRightOperand()));
            getRightOperand().setType(compiler.getEnvType().getTypeFloat());
            return compiler.getEnvType().getTypeFloat();
        }
        else if (type1.isFloat() && type2.isFloat()){
            return compiler.getEnvType().getTypeFloat();
        }
        else {
            throw new ContextualError("Conflicting types : " + getOperatorName() + " is an arithmetic operation, types of the operands should be int or float, "
                    + "but are " + type1 + " and " + type2, this.getLocation());
        }
    }
    
    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.33)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type = verifyTypeBinaryOp(compiler, type1, type2);
            this.setType(type);
            checkDecoration();
            return type;
    }
    
    /**
     * Naive code generation for arithmetic expression
     */
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
    	// Right operand has a DVal
    	if (getRightOperand().getDVal() != null) {
    		getLeftOperand().codeGenExp(compiler, reg);
    		codeGenOpArith(compiler, reg, getRightOperand().getDVal());
    	}
    	// Right operand does not have a defined DVal
    	else {
    		int registersAvailable = reg.getNumber() - compiler.getCompilerOptions().getRmax();
    		// At least 2 registers are free: no need to push on stack
    		if (registersAvailable < 0) {
    		    // One more register will need to be saved on stack
    		    compiler.setMaxRegisters(reg.getNumber());
    			getLeftOperand().codeGenExp(compiler, reg);
    			GPRegister rnext = Register.getR(reg.getNumber() + 1);
    			getRightOperand().codeGenExp(compiler, rnext);
    			codeGenOpArith(compiler, reg, rnext);
    		}
    		// Only 1 register is free
    		else if (registersAvailable == 0) {
    			getLeftOperand().codeGenExp(compiler, reg);
    			// One temporary stack cell is necessary
    			compiler.incTemporary();
    			compiler.addInstruction(new PUSH(reg));
    			getRightOperand().codeGenExp(compiler, reg);
    			compiler.addInstruction(new LOAD(reg, Register.R0));
                compiler.decTemporary();
    			compiler.addInstruction(new POP(reg));
    			codeGenOpArith(compiler, reg, Register.R0);
    		}
    		else {
    			throw new DecacInternalError("Cannot perform the operation: no register available");
    		}
    	}
    	// Overflow check for operations on floats
        if (getType().isFloat() && !compiler.getCompilerOptions().getNoCheck()) {
            CodeError.ARITHMETIC_OVERFLOW.codeGenBOV(compiler);
        }
    }
    
    protected abstract void codeGenOpArith(DecacCompiler compiler, GPRegister op1, DVal op2);
}
