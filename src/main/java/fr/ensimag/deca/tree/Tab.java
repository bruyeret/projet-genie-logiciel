package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.NEW;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import java.util.Iterator;

public class Tab extends AbstractExpr {
    final private ListExpr listExpr;
    
    public Tab(ListExpr listExpr) {
        Validate.notNull(listExpr);
        this.listExpr = listExpr;
    }
    
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        Type inType = null;
        if (listExpr.isEmpty()) {
            throw new ContextualError("Unable to find the type of an empty table",
                    this.getLocation());
        }
        for (AbstractExpr partOfTab : listExpr.getList()){
            Type typeR = partOfTab.verifyExpr(compiler, localEnv, currentClass);
            if (typeR.isTab()) {
                throw new ContextualError("Conflicting types : an element of a table should not be a table",
                    this.getLocation());
            }
            if (typeR.isVoid()) {
                throw new ContextualError("Conflicting types : an element of a table should not be void",
                    this.getLocation());
            }
            if (inType == null) {
                inType = typeR;
            }
            if (!inType.assignCompatible(typeR)) {
                // On ne peut pas faire d'association dans le sens inType = typeR
                if (!typeR.assignCompatible(inType)) {
                    // On ne peut pas non plus faire l'association dans l'autre sens
                    throw new ContextualError("Conflicting types : a table can "
                            + "not contain both types " + inType + " and " + typeR,
                        this.getLocation());
                } else {
                    // On prend le type le plus général
                    inType = typeR;
                }
            }
        }
        inType = compiler.getEnvType().getTypeTab(inType);
        this.setType(inType);
        return inType;
    }
    
    
    public void verifyConvFloatTab(DecacCompiler compiler){
        int counter = 0;
        for (AbstractExpr expr : listExpr.getList()) {
            if (expr.getType().isInt()) {
                ConvFloat cv = new ConvFloat(expr.getExpression());
                cv.setType(compiler.getEnvType().getTypeFloat());
                listExpr.set(counter, cv);
            }
            counter++;
        }
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Allocating memory
        compiler.addInstruction(new NEW(listExpr.size() + 1, reg));
        // Loading the length into R0
        compiler.addInstruction(new LOAD(listExpr.size(), Register.R0));
        // Storing the length into the 1st cell
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(0, reg)));
        
        // Where the address of the aaray is stored
        GPRegister tabReg;
        // Where to compute expressions
        GPRegister exprReg;
        
        int registersAvailable = reg.getNumber() - compiler.getCompilerOptions().getRmax();
        // At least 2 registers are free: no need to push on stack
        if (registersAvailable < 0) {
            // One more register will need to be saved on stack
            compiler.setMaxRegisters(reg.getNumber());
            tabReg = reg;
            exprReg = Register.getR(reg.getNumber() + 1);
        }
        // Only 1 register is free
        else if (registersAvailable == 0) {
            // One temporary stack cell is necessary
            compiler.incTemporary();
            // Saving R2 instead of reg so we don't have to pop
            // the array address every time we compute an expression
            compiler.addInstruction(new PUSH(Register.getR(2)));
            // Copying reg to R2
            compiler.addInstruction(new LOAD(reg, Register.getR(2)));
            tabReg = Register.getR(2);
            exprReg = reg;
        }
        else {
            throw new DecacInternalError("Cannot perform the operation: no register available");
        }
        
        int index = 1;
        for (AbstractExpr expr : listExpr.getList()) {
            // Computing the expression into exprReg
            expr.codeGenExp(compiler, exprReg);
            // Storing the value into the cell
            compiler.addInstruction(new STORE(exprReg, new RegisterOffset(index, tabReg)));
            index++;
        }
        
        if (registersAvailable == 0) {
            // Copying the tab address to reg
            compiler.addInstruction(new LOAD(Register.getR(2), reg));
            // Restoring R2
            compiler.addInstruction(new POP(Register.getR(2)));
            compiler.decTemporary();
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print('[');
        listExpr.decompile(s);
        s.print(']');
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        Iterator<AbstractExpr> iter = listExpr.iterator();
        while (iter.hasNext()) {
            AbstractExpr piece = iter.next();
            piece.prettyPrint(s, prefix, !iter.hasNext(), true);
        }
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        for (AbstractExpr piece : listExpr.getList()) {
            f.apply(piece);
        }
    }
}
