package fr.ensimag.deca.tree;


import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import org.apache.commons.lang.Validate;
import fr.ensimag.ima.pseudocode.DAddr;

import java.io.PrintStream;

/**
 * Declaration of a parameter.
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class DeclParam extends AbstractDeclParam {
    
    final private AbstractIdentifier type;
    final private AbstractIdentifier paramName;
    
    public DeclParam(AbstractIdentifier type, AbstractIdentifier paramName) {
        Validate.notNull(type);
        Validate.notNull(paramName);
        this.type = type;
        this.paramName = paramName;
    }
    
    @Override
    public AbstractIdentifier getParamName() {
        return this.paramName;
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        type.decompile(s);
        s.print(' ');
        paramName.decompile(s);
    }
    
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.9)
     */
    @Override
    public Type verifyParam(DecacCompiler compiler) throws ContextualError {
        Type myType = this.type.verifyType(compiler);
        if (!myType.isVoid()){
            this.paramName.setType(myType);
            checkDecoration();
            return myType;
        } else {
            throw new ContextualError("The type of a parameter can not be void", getLocation());
        }
    }

    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.13)
     */
    public SymbolTable.Symbol verifyParamBody(DecacCompiler compiler) throws ContextualError {
        type.verifyType(compiler);
        Symbol name = paramName.getName();
        return name;
    }
    
    /**
     * Sets the operand of the parameter.
     * (used for code generation)
     * @param operand
     */
    public void setCodeGenOperand(DAddr operand){
        paramName.getParamDefinition().setOperand(operand);
    }
    
    @Override
    protected
    void iterChildren(TreeFunction f) {
        type.iter(f);
        paramName.iter(f);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        paramName.prettyPrint(s, prefix, true);
    }

}