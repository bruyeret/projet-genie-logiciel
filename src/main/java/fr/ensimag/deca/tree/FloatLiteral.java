package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.PrintGen;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.ImmediateFloat;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Single precision, floating-point literal
 *
 * @author gl46
 * @date 01/01/2021
 */
public class FloatLiteral extends AbstractExpr {

    public float getValue() {
        return value;
    }

    private float value;

    public FloatLiteral(float value) {
        Validate.isTrue(!Float.isInfinite(value),
                "literal values cannot be infinite");
        Validate.isTrue(!Float.isNaN(value),
                "literal values cannot be NaN");
        this.value = value;
    }

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.45)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeFloat();
        this.setType(type);
        checkDecoration();
        return type;        
    }

    @Override
    protected void codeGenPrint(DecacCompiler compiler, boolean printHex) {
        PrintGen.printFloat(compiler, value, printHex);
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // do nothing
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        compiler.addInstruction(new LOAD(value, reg));
    }
    
    @Override
    protected DVal getDVal() {
        return new ImmediateFloat(value);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print(java.lang.Float.toHexString(value));
    }

    @Override
    String prettyPrintNode() {
        return "Float (" + getValue() + ")";
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
