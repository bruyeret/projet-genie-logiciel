package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Conversion of an int into a float. Used for implicit conversions.
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class ConvFloat extends AbstractUnaryExpr {
    public ConvFloat(AbstractExpr operand) {
        super(operand);
    }

    /**
     * Partial operation for unary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeUnaryOp(DecacCompiler compiler, Type type2) throws ContextualError{
        // nothing to do ; never reached
        return compiler.getEnvType().getTypeFloat();
    }
    
    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.39)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        // nothing to do ; never reached
        this.setType(compiler.getEnvType().getTypeFloat());
        return compiler.getEnvType().getTypeFloat();
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        getOperand().codeGenExp(compiler, reg);
        compiler.addInstruction(new FLOAT(reg, reg));
    }

    @Override
    protected String getOperatorName() {
        return "/* conv float */";
    }

}
