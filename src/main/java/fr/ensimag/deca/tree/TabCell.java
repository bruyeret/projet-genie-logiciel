package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.TabType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.RegisterOffsetIndex;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

public class TabCell extends AbstractHeapLValue {

    final private AbstractExpr tab;
    final private AbstractExpr index;
    
    public TabCell(AbstractExpr tab, AbstractExpr index) {
        Validate.notNull(tab);
        Validate.notNull(index);
        this.tab = tab;
        this.index = index;
    }
    
    @Override
    public Type verifyExpr(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // type is TypeTab
        Type myType = this.tab.verifyExpr(compiler, localEnv, currentClass);
        
        // returnType of the index
        Type returnType = this.index.verifyExpr(compiler, localEnv, currentClass);
        
        if ((myType.isTab()&& returnType.isInt())) {
            TabType myTabType = (TabType) myType;
            this.setType(myTabType.getDefinition().getVarType());
            return myTabType.getDefinition().getVarType();
        } else {
            throw new ContextualError("Conflicting types : the type " + myType 
                    + " should be a table type and the type " + returnType + " should be int", this.getLocation());
        }
    }

    @Override
    protected RegisterOffset getHeapAddress(DecacCompiler compiler, GPRegister reg) {
        // Computing the address of the array into reg
        tab.codeGenExp(compiler, reg);
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Check if null (no need to use CMP because EQ flag is already set)
            CodeError.NULL_DEREFERENCING.codeGenBEQ(compiler);
        }
        
        GPRegister tabReg;
        GPRegister indexReg;
        
        int registersAvailable = reg.getNumber() - compiler.getCompilerOptions().getRmax();
        // At least 2 registers are free: no need to push on stack
        if (registersAvailable < 0) {
            indexReg = Register.getR(reg.getNumber() + 1);
            tabReg = reg;
            compiler.setMaxRegisters(reg.getNumber());
            // Computing the index into rnext
            index.codeGenExp(compiler, indexReg);
        }
        // Only 1 register is free
        else if (registersAvailable == 0) {
            tabReg = Register.R1;
            indexReg = reg;
            // Pushing the address into the stack
            compiler.incTemporary();
            compiler.addInstruction(new PUSH(reg));
            // Computing the index into reg
            index.codeGenExp(compiler, indexReg);
            // Loading the address into R1
            compiler.decTemporary();
            compiler.addInstruction(new POP(Register.R1));
        }
        else {
            throw new DecacInternalError("Cannot perform the operation: no register available");
        }
        // Loading the length of the table into R0
        compiler.addInstruction(new LOAD(new RegisterOffset(0, tabReg), Register.R0));
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Error if negative index
            CodeError.INDEX_ERROR.codeGenBLT(compiler);
            compiler.addInstruction(new CMP(Register.R0, indexReg));
            // Error if index >= length
            CodeError.INDEX_ERROR.codeGenBGE(compiler);
        }
        // Storing the address of the cell into reg
        compiler.addInstruction(new LEA(new RegisterOffsetIndex(1, tabReg, indexReg), reg));
        // Returning the address
        return new RegisterOffset(0, reg);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        tab.decompile(s);
        s.print('[');
        index.decompile(s);
        s.print(']');
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        tab.prettyPrint(s, prefix, false);
        index.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        tab.iter(f);
        index.iter(f);
    }
}
