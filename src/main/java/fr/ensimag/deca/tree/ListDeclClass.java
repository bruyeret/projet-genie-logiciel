package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;

import org.apache.log4j.Logger;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ListDeclClass extends TreeList<AbstractDeclClass> {
    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);
    
    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclClass c : getList()) {
            c.decompile(s);
            s.println();
        }
    }

    /**
     * Pass 1 of [SyntaxeContextuelle] rule (1.2)
     */
    void verifyListClass(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify listClass pass 1 : start");
        for (AbstractDeclClass declClass : this.getList()){
            declClass.verifyClass(compiler);
        }
        LOG.debug("verify listClass pass 1 : end");
    }

    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.2)
     */
    public void verifyListClassMembers(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify listClass pass 2 : start");
        for (AbstractDeclClass declClass : this.getList()){
                declClass.verifyClassMembers(compiler);
        }
        LOG.debug("verify listClass pass 2 : end");
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.2)
     */
    public void verifyListClassBody(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify listClass pass 3 : start");
        for (AbstractDeclClass declClass : this.getList()){
            LOG.debug("\nEnvironnement de la classe : " + declClass.getCurrentClass().getName().getName() 
                    + "\n" + declClass.getCurrentClass().getClassDefinition().getMembers().helpMePrintString() 
                    + "\nNumber of methods : " + declClass.getCurrentClass().getClassDefinition().getNumberOfMethods() 
                    + "\nNumber of fields : " + declClass.getCurrentClass().getClassDefinition().getNumberOfFields());
            declClass.verifyClassBody(compiler);
        }
        LOG.debug("verify listClass pass 3 : end");
    }

    public void codeGenMethodTable(DecacCompiler compiler) {
        for (AbstractDeclClass declClass: getList()) {
            declClass.codeGenMethodTable(compiler);
        }
    }

    public void codeGenMethods(DecacCompiler compiler) {
        for (AbstractDeclClass declClass: getList()) {
            declClass.codeGenMethods(compiler);
        }
    }
}
