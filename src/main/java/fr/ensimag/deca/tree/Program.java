package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.InstanceOfFunction;
import fr.ensimag.deca.codegen.MethodTable;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.instructions.*;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * Deca complete program (class definition plus main block)
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Program extends AbstractProgram {
    private static final Logger LOG = Logger.getLogger(Program.class);
    
    public Program(ListDeclClass classes, AbstractMain main) {
        Validate.notNull(classes);
        Validate.notNull(main);
        this.classes = classes;
        this.main = main;
    }
    public ListDeclClass getClasses() {
        return classes;
    }
    public AbstractMain getMain() {
        return main;
    }
    private ListDeclClass classes;
    private AbstractMain main;

    /**
     * For the three passes of [SyntaxeContextuelle], rules (1.1), (2.1), (3.1)
     */
    @Override
    public void verifyProgram(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify program: start");
        // Pass 1 of [SyntaxeContextuelle] rule (1.1)
        this.classes.verifyListClass(compiler);
        // Pass 2 of [SyntaxeContextuelle] rule (2.1)
        this.classes.verifyListClassMembers(compiler);
        LOG.debug("\nEnvironmentType : " 
                + "\n" + compiler.getEnvType().helpMePrint());
        // Pass 3 of [SyntaxeContextuelle] rule (3.1)
        this.classes.verifyListClassBody(compiler);
        this.main.verifyMain(compiler);
        
        // LOG.debug("verify program: end");
    }

    @Override
    public void codeGenProgram(DecacCompiler compiler) {
        compiler.newBlock();
        
        compiler.addComment("Method tables");
        MethodTable.codeGenObject(compiler);
        classes.codeGenMethodTable(compiler);
        compiler.setMethodTable(compiler.getGBOffset() - 1);
        
        compiler.addComment("Main program");
        main.codeGenMain(compiler);
        
        compiler.addInstruction(new HALT());
        // No need to save registers in the main program
        compiler.appendBlock(true, false);
    }
    
    @Override
    public void codeGenMethods(DecacCompiler compiler) {
        InstanceOfFunction.codeGen(compiler);
        MethodTable.codeGenEquals(compiler);
        classes.codeGenMethods(compiler);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        getClasses().decompile(s);
        getMain().decompile(s);
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        classes.iter(f);
        main.iter(f);
    }
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        classes.prettyPrint(s, prefix, false);
        main.prettyPrint(s, prefix, true);
    }
}
