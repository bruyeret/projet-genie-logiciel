package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Print statement (print, println, ...).
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractPrint extends AbstractInst {

    private boolean printHex;
    private ListExpr arguments = new ListExpr();
    
    abstract String getSuffix();

    public AbstractPrint(boolean printHex, ListExpr arguments) {
        Validate.notNull(arguments);
        this.arguments = arguments;
        this.printHex = printHex;
    }

    public ListExpr getArguments() {
        return arguments;
    }

    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.21)
     * implicit : rules (3.26) (3.27) (3.30) (3.31)
     * calling rule (3.32)
     */
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        for (AbstractExpr expr : this.arguments.getList()){
            Type type = expr.verifyExpr(compiler, localEnv, currentClass);
            if (type != compiler.getEnvType().getTypeInt() && type != compiler.getEnvType().getTypeFloat() 
                    && type != compiler.getEnvType().getTypeString()){
                throw new ContextualError("Conflicting types : expression of type " + type + " is not printable", this.getLocation());
            }
        }
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        for (AbstractExpr a : getArguments().getList()) {
            a.codeGenPrint(compiler, printHex);
        }
    }

    @Override
    public void decompile(IndentPrintStream s) {
    	s.print("print");
    	s.print(getSuffix());
    	if (printHex) {
    	    s.print('x');
    	}
    	s.print('(');
    	arguments.decompile(s);
    	s.print(')');
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        arguments.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        arguments.prettyPrint(s, prefix, true);
    }
}
