package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.InlinePortion;

public class MethodAsmBody extends AbstractMethodBody {
    final private StringLiteral code; 
    
    public MethodAsmBody(StringLiteral code) {
        Validate.notNull(code);
        this.code = code;
    }
    
    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.15)
     */
    protected void verifyMethodBody(DecacCompiler compiler, EnvironmentExp localEnv,
            EnvironmentExp paramEnv, ClassDefinition currentClass, Type returnType)
            throws ContextualError {
            code.verifyExpr(compiler, localEnv, currentClass);
   }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.println();
        s.indent();
        s.println("asm(");
        s.indent();
        code.decompile(s);
        s.unindent();
        s.println(");");
        s.unindent();
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler) {
        compiler.addProgramLabel(compiler.getMethodDef().getCodeLabel());
        compiler.newBlock();
        
        // Method code (no checks are performed by the compiler)
        compiler.add(new InlinePortion(code.getValue()));
        
        compiler.appendBlock(false, false);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        code.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        code.iter(f);
    }

}
