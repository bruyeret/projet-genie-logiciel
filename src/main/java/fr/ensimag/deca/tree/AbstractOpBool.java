package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractOpBool extends AbstractBinaryExpr {

    public AbstractOpBool(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }
    
    /**
     * Partial operation for binary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        String op = this.getOperatorName();
        if (type1.isBoolean() && type2.isBoolean()){
            return compiler.getEnvType().getTypeBoolean();
        } else {
            throw new ContextualError("Conflicting types : " + op + " is a boolean operation, types of the operands should be both boolean, "
                    + "but are " + type1 + " and " + type2 , this.getLocation());
        }
    }
    
    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.33)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        Type type = verifyTypeBinaryOp(compiler, type1, type2);
        this.setType(type);
        checkDecoration();
        return type;
    }


}
