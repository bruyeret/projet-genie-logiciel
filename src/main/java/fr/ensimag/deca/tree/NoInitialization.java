package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;

import java.io.PrintStream;

/**
 * Absence of initialization (e.g. "int x;" as opposed to "int x =
 * 42;").
 *
 * @author gl46
 * @date 01/01/2021
 */
public class NoInitialization extends AbstractInitialization {
    @Override
    public boolean isEmpty() {
        return true;
    }
    
    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.9)
    */
    protected void verifyInitialization(DecacCompiler compiler, Type t,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // Nothing to do
    }

    @Override
    protected void codeGenInitialization(DecacCompiler compiler, DAddr addr) {
        // nothing
    }
    
    @Override
    protected void codeGenInitField(DecacCompiler compiler, int fieldIndex) {
        // nothing
    }
    
    /**
     * Node contains no real information, nothing to check.
     */
    @Override
    protected void checkLocation() {
        // nothing
    }

    @Override
    public void decompile(IndentPrintStream s) {
        // nothing
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
