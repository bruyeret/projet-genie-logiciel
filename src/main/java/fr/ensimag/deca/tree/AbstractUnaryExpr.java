package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Unary expression.
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractUnaryExpr extends AbstractExpr {

    public AbstractExpr getOperand() {
        return operand;
    }
    
    private AbstractExpr operand;
    
    public AbstractUnaryExpr(AbstractExpr operand) {
        Validate.notNull(operand);
        this.operand = operand;
    }

    protected abstract String getOperatorName();
    
    /**
     * Partial operation for unary operations [SyntaxeContextuelle]
     */
    public abstract Type verifyTypeUnaryOp(DecacCompiler compiler, Type type2) throws ContextualError;
  
    @Override
    public void decompile(IndentPrintStream s) {
        s.print(getOperatorName());
        s.print('(');
        operand.decompile(s);
        s.print(')');
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        operand.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        operand.prettyPrint(s, prefix, true);
    }

}
