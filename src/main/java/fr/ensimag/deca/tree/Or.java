package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Or extends AbstractOpBool {

    public Or(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "||";
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        // <Code(C1 || C2 , b, E)>
        // ≡ <Code(!(!C1 && !C2), b, E)>
        // ≡ <Code(!C1 && !C2, !b, E)>
        
        // <Code(C1 || C2 , vrai, E)> ≡ <Code(!C1 && !C2, faux, E)>
        if (branchTruthValue) {
            // <Code(!C1 , faux, E)> ≡ <Code(C1, vrai, E)>
            getLeftOperand().codeGenExpBool(compiler, reg, true, branchLabel);
            // <Code(!C2 , faux, E)> ≡ <Code(C2, vrai, E)>
            getRightOperand().codeGenExpBool(compiler, reg,  true, branchLabel);
        }
        // <Code(C1 || C2 , faux, E)> ≡ <Code(!C1 && !C2, vrai, E)>
        else {
            Label endLabel = new Label("or_end_" + compiler.getLabelID());
            // <Code(!C1 , faux, E_Fin.n)> ≡ <Code(C1 , vrai, E_Fin.n)>
            getLeftOperand().codeGenExpBool(compiler, reg,  true, endLabel);
            // <Code(!C2 , vrai, E)> ≡ <Code(C2 , faux, E)>
            getRightOperand().codeGenExpBool(compiler, reg,  false, branchLabel);
            // E_Fin.n :
            compiler.addLabel(endLabel);
        }
    }
}
