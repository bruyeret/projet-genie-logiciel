package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.EnvironmentExp.DoubleDefException;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;

public class ListDeclMethod extends TreeList<AbstractDeclMethod> {
    
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.6)
     */
    public EnvironmentExp verifyListMethodMembers(DecacCompiler compiler, ClassDefinition superClass, ClassDefinition currentClass)
            throws ContextualError {
        EnvironmentExp localEnv = new EnvironmentExp(superClass.getMembers());
        for (AbstractDeclMethod declMethod : this.getList()){
            AbstractIdentifier method = declMethod.verifyMethodMembers(compiler, superClass, currentClass);
            Symbol symbol = method.getName();
            ExpDefinition def = declMethod.getMethodName().getMethodDefinition();
            try {
                localEnv.declare(symbol, def);
            } catch(DoubleDefException ex) {
                throw new ContextualError("Method " + symbol.getName() + " has already been declared", declMethod.getLocation());
            }
        }
        return localEnv; 
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.10)
     */
    public void verifyListMethodBody(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            for (AbstractDeclMethod method : this.getList()){
                method.verifyMethodBodyParam(compiler, localEnv, currentClass);
            }
    }
    
    protected void codeGenMethods(DecacCompiler compiler) {
        for (AbstractDeclMethod declMethod : getList()) {
            declMethod.codeGenMethod(compiler);
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclMethod declMethod: getList()) {
            declMethod.decompile(s);
            s.println();
        }
    }
}