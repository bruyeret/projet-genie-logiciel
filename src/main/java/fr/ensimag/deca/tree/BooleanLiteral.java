package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class BooleanLiteral extends AbstractExpr {

    private boolean value;

    public BooleanLiteral(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.47)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeBoolean();
        this.setType(type);
        checkDecoration();
        return type;  
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // do nothing
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        compiler.addInstruction(new LOAD(value ? 1 : 0, reg));
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        // <Code(true, vrai, E)> ≡ BRA E
        // <Code(true, faux, E)> ≡ ε
        // <Code(false, b, E)> ≡ <Code(!true, b, E)>
        if (value && branchTruthValue || !value && !branchTruthValue) {
            compiler.addInstruction(new BRA(branchLabel));
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print(Boolean.toString(value));
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    String prettyPrintNode() {
        return "BooleanLiteral (" + value + ")";
    }

}
