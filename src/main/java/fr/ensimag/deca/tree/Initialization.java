package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.TabType;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * @author gl46
 * @date 01/01/2021
 */
public class Initialization extends AbstractInitialization {
    @Override
    public boolean isEmpty() {
        return false;
    }
    
    public AbstractExpr getExpression() {
        return expression;
    }

    private AbstractExpr expression;

    public void setExpression(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    public Initialization(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.8)
    */ 
    protected void verifyInitialization(DecacCompiler compiler, Type t, 
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // Verification of the right memeber of the initialisation
        expression.verifyRValue(compiler, localEnv, currentClass, t);
        // if ConvFloat needed and if the orands are not tables
        if (t.isFloat() && expression.getType().isInt()){
            setExpression(new ConvFloat(getExpression()));
            expression.setType(compiler.getEnvType().getTypeFloat());
        }
        // if they are tables 
        if (t.isTab()){
            TabType myType = (TabType) t;
            Type declTabType = myType.getDefinition().getVarType();
            // if ConvFloat needed
            if(declTabType.isFloat()){
                ((Tab) expression).verifyConvFloatTab(compiler);
                expression.setType(compiler.getEnvType().getTypeTab(compiler.getEnvType().getTypeFloat()));
            }
        }
    }

    @Override
    protected void codeGenInitialization(DecacCompiler compiler, DAddr addr) {
        // Computes the value into R2
        compiler.setMaxRegisters(1);
        getExpression().codeGenExp(compiler, Register.getR(2));
        // Store it into addr
        compiler.addInstruction(new STORE(Register.getR(2), addr));
    }
    
    @Override
    protected void codeGenInitField(DecacCompiler compiler, int fieldIndex) {
        // Computes the value into R2 (at least 1 register used)
        compiler.setMaxRegisters(1);
        getExpression().codeGenExp(compiler, Register.getR(2));
        // Loading the object's address (stored at -2(LB) when calling the init function) into R1
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R1));
        // Storing the result into the field
        compiler.addInstruction(new STORE(Register.getR(2),
                new RegisterOffset(fieldIndex, Register.R1)));
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
    	s.print(" = ");
    	expression.decompile(s);
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        expression.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expression.prettyPrint(s, prefix, true);
    }
}
