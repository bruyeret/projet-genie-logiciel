package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGE;
import fr.ensimag.ima.pseudocode.instructions.BLT;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Lower extends AbstractOpIneq {

    public Lower(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected void codeGenOpCmp(DecacCompiler compiler, boolean branchTruthValue, Label branchLabel) {
        if (branchTruthValue) {
            compiler.addInstruction(new BLT(branchLabel));
        }
        else {
            compiler.addInstruction(new BGE(branchLabel));
        }
    }

    @Override
    protected String getOperatorName() {
        return "<";
    }

}
