package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractOpCmp extends AbstractBinaryExpr {

    public AbstractOpCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }
    
    /**
     * Partial operation for binary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        if (type1.isInt() && type2.isInt() || type1.isFloat() && type2.isFloat()) {}
        else if (type1.isInt() && type2.isFloat()) {
            setLeftOperand(new ConvFloat(getLeftOperand()));
            getLeftOperand().setType(compiler.getEnvType().getTypeFloat());
        }
        else if (type1.isFloat() && type2.isInt()) {
            setRightOperand(new ConvFloat(getRightOperand()));
            getRightOperand().setType(compiler.getEnvType().getTypeFloat());
        }
        else {
            throw new ContextualError("Conflicting types : types for the " + getOperatorName() + " operation should be int or float, but are " 
                        + type1 + " and " + type2, this.getLocation());
        }
        return compiler.getEnvType().getTypeBoolean();
    }

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.33)
    */ 
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type = verifyTypeBinaryOp(compiler, type1, type2);
            this.setType(type);
            checkDecoration();
            return type;
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        GPRegister rightOp, leftOp;
        
        int registersAvailable = reg.getNumber() - compiler.getCompilerOptions().getRmax();
        // At least 2 registers are free: no need to push on stack
        if (registersAvailable < 0) {
            // One more register will need to be saved on stack
            compiler.setMaxRegisters(reg.getNumber());
            GPRegister rnext = Register.getR(reg.getNumber() + 1);
            // Computing left operand into reg
            getLeftOperand().codeGenExp(compiler, reg);
            // Computing right operand into rnext
            getRightOperand().codeGenExp(compiler, Register.getR(3));
            leftOp = reg;
            rightOp = rnext;
        }
        // Only 1 register is free
        else if (registersAvailable == 0) {
            getLeftOperand().codeGenExp(compiler, reg);
            // One temporary stack cell is necessary
            compiler.incTemporary();
            compiler.addInstruction(new PUSH(reg));
            getRightOperand().codeGenExp(compiler, reg);
            compiler.addInstruction(new LOAD(reg, Register.R0));
            compiler.decTemporary();
            compiler.addInstruction(new POP(reg));
            leftOp = Register.R0;
            rightOp = reg;
        }
        else {
            throw new DecacInternalError("Cannot perform the operation: no register available");
        }

        compiler.addInstruction(new CMP(rightOp, leftOp));
        codeGenOpCmp(compiler, branchTruthValue, branchLabel);
    }
    
    /**
     * Code generation for branch instructions
     * @param compiler
     * @param reg
     */
    protected abstract void codeGenOpCmp(DecacCompiler compiler, boolean branchTruthValue, Label branchLabel);
}
