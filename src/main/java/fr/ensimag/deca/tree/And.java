package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class And extends AbstractOpBool {

    public And(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "&&";
    }
 
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        // <Code(C1 && C2 , vrai, E)> ≡
        if (branchTruthValue) {
            Label endLabel = new Label("end_and_" + compiler.getLabelID());
            // <Code(C1 , faux, E_Fin.n)>
            getLeftOperand().codeGenExpBool(compiler, reg, false, endLabel);
            // <Code(C2 , vrai, E)>
            getRightOperand().codeGenExpBool(compiler, reg,  true, branchLabel);
            // E_Fin.n :
            compiler.addLabel(endLabel);
        }
        // <Code(C1 && C2 , faux, E)> ≡
        else {
            // <Code(C1 , faux, E)>
            getLeftOperand().codeGenExpBool(compiler, reg,  false, branchLabel);
            // <Code(C2 , faux, E)>
            getRightOperand().codeGenExpBool(compiler, reg,  false, branchLabel);
        }
    }
}
