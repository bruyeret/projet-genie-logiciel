package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

public abstract class AbstractDeclField extends Tree {
        
    public abstract Visibility getVisibility();
    
    public abstract AbstractIdentifier getFieldName();
            
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.5)
     */
    protected abstract AbstractIdentifier verifyFieldMembers(DecacCompiler compiler, ClassDefinition superClass,
            ClassDefinition currentClass)
            throws ContextualError;
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.7)
     */
    public abstract void verifyFieldBody(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError;

    /**
     * Generates code that initializes the field to R0 if necessary
     * Assumes that R1 contains the address of the object
     * @param compiler
     */
    protected abstract void codeGenZeroInit(DecacCompiler compiler);
    
    /**
     * Generates code that initializes the field if specified
     * @param compiler
     */
    protected abstract void codeGenInit(DecacCompiler compiler);
}
