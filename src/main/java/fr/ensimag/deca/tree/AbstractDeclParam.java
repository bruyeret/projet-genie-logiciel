package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.DAddr;

public abstract class AbstractDeclParam extends Tree {
    
    public abstract AbstractIdentifier getParamName();
            
    public abstract Type verifyParam(DecacCompiler compiler) throws ContextualError;
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.13)
     */
    public abstract SymbolTable.Symbol verifyParamBody(DecacCompiler compiler) throws ContextualError;
    
    /**
     * Sets the operand of the parameter.
     * (used for code generation)
     * @param operand
     */
    public abstract void setCodeGenOperand(DAddr operand);
}
