package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Assignment, i.e. lvalue = expr.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Assign extends AbstractBinaryExpr {

    @Override
    public AbstractLValue getLeftOperand() {
        // The cast succeeds by construction, as the leftOperand has been set
        // as an AbstractLValue by the constructor.
        return (AbstractLValue)super.getLeftOperand();
    }

    public Assign(AbstractLValue leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    /**
     * Partial operation for binary assignation [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler,Type type1, Type type2) throws ContextualError {
        // Same types, nothing to do
        if (type1.sameType(type2)) {
            return type1;
        }
        // Conversion from int to float
        if (type1.isFloat() && type2.isInt()) {
            this.setRightOperand(new ConvFloat(getRightOperand()));
            this.getRightOperand().setType(compiler.getEnvType().getTypeFloat());
            return compiler.getEnvType().getTypeFloat();
        }
        // Conversion from int to float in float tabs
        if (type1.isTab() && type2.isTab()) {
            ((Tab) this.getRightOperand()).verifyConvFloatTab(compiler);
            this.getRightOperand().setType(compiler.getEnvType().getTypeTab(compiler.getEnvType().getTypeFloat()));
            return compiler.getEnvType().getTypeTab(compiler.getEnvType().getTypeFloat());
        }
        // Type.assignCompatible should have raised an error if assign is incompatible
        return type1;
    }
    
    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.32)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        this.getRightOperand().verifyRValue(compiler, localEnv, currentClass, type);
        Type typeR = this.getRightOperand().getType();
        this.verifyTypeBinaryOp(compiler, type, typeR);
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
    	getLeftOperand().codeGenAssign(compiler, reg, getRightOperand(), false);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        compiler.setMaxRegisters(1);
        getLeftOperand().codeGenAssign(compiler, Register.getR(2), getRightOperand(), true);
    }
    
    @Override
    protected String getOperatorName() {
        return "=";
    }

}
