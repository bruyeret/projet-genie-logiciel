package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.STORE;

/**
 * Abstract class used to define lvalues
 * that are addresses of words stored into
 * the heap.
 */
public abstract class AbstractHeapLValue extends AbstractLValue {
    /**
     * Computes a value into reg and returns
     * the address of the lvalue with a RegisterOffset
     * which references reg.
     * @param compiler
     * @param reg
     * @return
     */
    protected abstract RegisterOffset getHeapAddress(DecacCompiler compiler, GPRegister reg);
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        compiler.addInstruction(new LOAD(getHeapAddress(compiler, reg), reg));
    }
    
    @Override
    protected void codeGenAssign(DecacCompiler compiler, GPRegister reg, AbstractExpr rvalue, boolean isInst) {
        // Computing the address of the lvalue.
        // It is a RegisterOffset which references reg,
        // so reg should not be overwritten afterwards
        RegisterOffset lvalueAddress = getHeapAddress(compiler, reg);
        GPRegister rvalueValue;
        
        int registersAvailable = reg.getNumber() - compiler.getCompilerOptions().getRmax();
        // At least 2 registers are free: no need to push on stack
        if (registersAvailable < 0) {
            // One more register will need to be saved on stack
            compiler.setMaxRegisters(reg.getNumber());
            rvalueValue = Register.getR(reg.getNumber() + 1);
            // Computing the rvalue into rnext
            rvalue.codeGenExp(compiler, rvalueValue);
        }
        // Only 1 register is free
        else if (registersAvailable == 0) {
            // One temporary stack cell is necessary
            compiler.incTemporary();
            // Saving the lvalue address
            compiler.addInstruction(new PUSH(reg));
            // Computing the rvalue into reg
            rvalue.codeGenExp(compiler, reg);
            // Copying reg into R0
            compiler.addInstruction(new LOAD(reg, Register.R0));
            rvalueValue = Register.R0;
            // Restoring the lvalue address into reg
            compiler.addInstruction(new POP(reg));
            compiler.decTemporary();
        }
        else {
            throw new DecacInternalError("Cannot perform the operation: no register available");
        }
        compiler.addInstruction(new STORE(rvalueValue, lvalueAddress));
        if (!isInst) {
            compiler.addInstruction(new LOAD(rvalueValue, reg));
        }
    }
}
