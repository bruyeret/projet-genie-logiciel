package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.instructions.ERROR;
import fr.ensimag.ima.pseudocode.instructions.WNL;
import fr.ensimag.ima.pseudocode.instructions.WSTR;

public class MethodBody extends AbstractMethodBody {
    final private ListDeclVar listDeclVar;
    final private ListInst listInst;
    
    public MethodBody(ListDeclVar listDeclVar, ListInst listInst) {
        Validate.notNull(listDeclVar);
        Validate.notNull(listInst);
        this.listDeclVar = listDeclVar;
        this.listInst = listInst;
    }
    
    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.14)
     */
    protected void verifyMethodBody(DecacCompiler compiler, EnvironmentExp localEnv,
            EnvironmentExp paramEnv, ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        EnvironmentExp varEnv = this.listDeclVar.verifyListDeclVariable(compiler, localEnv, paramEnv, currentClass);
        EnvironmentExp bodyEnv = varEnv.stacking(localEnv);
        // DEBUG BEGIN
//        System.out.println("Variables du corps de la méthode :");
//        varEnv.helpMePrint();
//        System.out.println("Environment relatif au corps de la méthode");
//        bodyEnv.helpMePrint();
        // DEBUG END
        this.listInst.verifyListInst(compiler, bodyEnv, currentClass, returnType);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.println(" {");
        s.indent();
        listDeclVar.decompile(s);
        s.println();
        listInst.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler) {
        compiler.addProgramLabel(compiler.getMethodDef().getCodeLabel());
        compiler.newBlock();
        
        // Method code
        compiler.addComment("Local variables initialization:");
        listDeclVar.codeGenListDeclVar(compiler, false);
        compiler.addComment("Method instructions:");
        listInst.codeGenListInst(compiler);
        
        if (!compiler.getMethodDef().getType().isVoid()) {
            compiler.addInstruction(new WSTR("Error: Exiting method " + compiler.getMethodDef().getName() + " without return."));
            compiler.addInstruction(new WNL());
            compiler.addInstruction(new ERROR());
            compiler.addLabel(compiler.getMethodDef().getEndLabel());
        }
        compiler.appendBlock(true, true);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        listDeclVar.prettyPrint(s, prefix, false);
        listInst.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        listDeclVar.iter(f);
        listInst.iter(f);
    }
}
