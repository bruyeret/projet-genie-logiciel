package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

public abstract class AbstractDeclMethod extends Tree {
    
    public abstract AbstractIdentifier getMethodName(); 
    
    public abstract AbstractMethodBody getMethodBody();
            
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.5)
     */
    protected abstract AbstractIdentifier verifyMethodMembers(DecacCompiler compiler, ClassDefinition superClass, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.11)
     */
    protected abstract void verifyMethodBodyParam(DecacCompiler compiler,
            EnvironmentExp paramEnv, ClassDefinition currentClass)
            throws ContextualError;

    protected abstract void codeGenMethod(DecacCompiler compiler);

    /**
     * Sets the operands for the parameters of the method
     */
    protected abstract void setCodeGenOperands();
}
