package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.instructions.RFLOAT;

import java.io.PrintStream;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ReadFloat extends AbstractReadExpr {

    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.36)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeFloat();
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    public void codeGenRead(DecacCompiler compiler) {
    	// Read a float and store it into R1
    	compiler.addInstruction(new RFLOAT());
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("readFloat()");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
