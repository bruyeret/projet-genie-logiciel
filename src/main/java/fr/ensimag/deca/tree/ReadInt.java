package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.instructions.RINT;

import java.io.PrintStream;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ReadInt extends AbstractReadExpr {

    @Override
    /*
    *   Pass 3 of [SyntaxeContextuelle] rule (3.35)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type = compiler.getEnvType().getTypeInt();
            this.setType(type);
            checkDecoration();
            return type;
            }

    @Override
    public void codeGenRead(DecacCompiler compiler) {
    	// Read an integer and store it into R1
    	compiler.addInstruction(new RINT());
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("readInt()");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
