package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;

/**
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class NoOperation extends AbstractInst {

    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.23)
    */
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        // Nothing to do
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // nothing to do
    }

    @Override
    public void decompile(IndentPrintStream s) {
        // nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
