package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.REM;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Modulo extends AbstractOpArith {

    public Modulo(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }
    
    /**
     * Partial operation for binary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        if (type1.isInt() && type2.isInt()){
            return compiler.getEnvType().getTypeInt();
        } else {
            throw new ContextualError("Incompatible types for the " + getOperatorName() + " operation : types of the operands are "
                    + type1 + " and " + type2 + " but should be both int", this.getLocation());
        }
    }

    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.51)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
            Type type = verifyTypeBinaryOp(compiler, type1, type2);
            this.setType(type);
            checkDecoration();
            return type;
    }


    @Override
    protected String getOperatorName() {
        return "%";
    }

    @Override
    protected void codeGenOpArith(DecacCompiler compiler, GPRegister op1, DVal op2) {
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Loading op2 into R1 for 0 comparison
            compiler.addInstruction(new LOAD(op2, Register.R1));
            compiler.addInstruction(new BEQ("zero_division_error"));
        }
    	compiler.addInstruction(new REM(op2, op1));
    }
}
