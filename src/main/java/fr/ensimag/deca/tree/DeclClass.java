package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.MethodTable;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.EnvironmentType;
import fr.ensimag.deca.context.TabDefinition;
import fr.ensimag.deca.context.TabType;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

import java.io.PrintStream;

/**
 * Declaration of a class (<code>class name extends superClass {members}<code>).
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class DeclClass extends AbstractDeclClass {

    final private AbstractIdentifier currentClass;
    final private AbstractIdentifier superClass;
    final private ListDeclField fields;
    final private ListDeclMethod methods;
       
    public DeclClass(AbstractIdentifier currentClass,
            AbstractIdentifier superClass,
            ListDeclField fields,
            ListDeclMethod methods) {
        this.currentClass = currentClass;
        this.superClass = superClass;
        this.fields = fields;
        this.methods = methods;
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("class ");
        currentClass.decompile(s);
        s.print(" extends ");
        superClass.decompile(s);
        s.println(" {");
        s.indent();
        fields.decompile(s);
        methods.decompile(s);
        s.unindent();
        s.println("}");
    }
    
    /**
     * Pass 1 of [SyntaxeContextuelle] rule (1.3) 
     */
    @Override
    protected void verifyClass(DecacCompiler compiler) throws ContextualError {
        // condition env_types(super) = (class(_),_) : super est une classe
        Definition myDef = compiler.getEnvType().getDef(this.superClass.getName().getName());
        if (myDef == null || !myDef.isClass()){
            throw new ContextualError("The super class " + this.superClass.getName().getName() + " was not declared", this.getLocation());
        }
        this.superClass.setDefinition((ClassDefinition) myDef);
        // declaration de currentClass
        Symbol C = compiler.getSymbolTable().create(this.currentClass.getName().getName());
        ClassType typeC = new ClassType(C, this.getLocation(), this.superClass.getClassDefinition());
        ClassDefinition defC = new ClassDefinition(typeC, currentClass.getLocation(), this.superClass.getClassDefinition());
        this.currentClass.setDefinition(defC);
        this.currentClass.setType(typeC);
        // declaration du type tableau associé à currentClass
        Symbol Ct = compiler.getSymbolTable().create(this.currentClass.getName().getName() + "[]");
        TabType typeCt = new TabType(Ct, typeC, this.getLocation());
        TabDefinition defCt = new TabDefinition(typeCt, typeC, this.getLocation());
        // ajout dans envType
        try {
            compiler.getEnvType().declare(this.currentClass.getName().getName(), defC);
            compiler.getEnvType().declare(Ct.getName(), defCt);
        } catch (EnvironmentType.DoubleDefException e) {
            throw new ContextualError("Class " + this.currentClass.getName().getName() + " already exist", this.getLocation());
        }
    }

    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.3)
     */
    @Override
    protected void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError {
        // Initial number of methods and fields
        this.currentClass.getClassDefinition().setNumberOfFields(this.superClass.getClassDefinition().getNumberOfFields());
        this.currentClass.getClassDefinition().setNumberOfMethods(this.superClass.getClassDefinition().getNumberOfMethods());
        // Set index of a method because depends on the super classes
        if (this.superClass == null) {
            // au cas où currentClass soit Object
            this.currentClass.getClassDefinition().setCounterMethod(1);
       } else {
            this.currentClass.getClassDefinition().setCounterMethod(this.superClass.getClassDefinition().getCounterMethod()+1);
        }
        // verify
        EnvironmentExp envExpF = this.fields.verifyListFieldMembers(compiler, 
                this.superClass.getClassDefinition(), this.currentClass.getClassDefinition());
        EnvironmentExp envExpM = this.methods.verifyListMethodMembers(compiler,
                this.superClass.getClassDefinition(), this.currentClass.getClassDefinition()); 
        EnvironmentExp envExpSuper = superClass.getClassDefinition().getMembers();
        // affectation
        try {
            envExpF.disjointUnion(envExpM); // envExpM contains the union of envexpF and itself
        } catch (EnvironmentExp.DoubleDefException e){
            throw new ContextualError("Two symbols have the same name, they shouln't", this.getLocation());
        }
        EnvironmentExp newEnv = envExpM.stacking(envExpSuper);  // newEnv contains the stacking of envExpM on envExpSuper
        // stacking of the envExp newEnv in the current class definition
        newEnv.stackingInPlace(this.currentClass.getClassDefinition().getMembers());
        // stacking in envType of the class type
        compiler.getEnvType().alter(this.currentClass.getName().getName(), this.currentClass.getClassDefinition());
    }
    
    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.5)
     */
    protected void verifyClassBody(DecacCompiler compiler) throws ContextualError {
        if (compiler.getEnvType().getDef(this.currentClass.getName().getName()).isClass()) {
            EnvironmentExp localEnv = this.currentClass.getClassDefinition().getMembers();
            if (localEnv == null){
                throw new ContextualError("The class " + currentClass.getName().getName() + " has no"
                        + " EnvironmentExp", this.getLocation());
            }
            this.fields.verifyListFieldBody(compiler, localEnv, currentClass.getClassDefinition());
            this.methods.verifyListMethodBody(compiler, localEnv, currentClass.getClassDefinition());
        } else {
            throw new ContextualError("Class type is expected", this.getLocation());
        }
    }


    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        currentClass.prettyPrint(s, prefix, false);
        superClass.prettyPrint(s, prefix, false);
        fields.prettyPrint(s, prefix, false);
        methods.prettyPrint(s, prefix, true);

    }
    
    @Override
    public void codeGenMethodTable(DecacCompiler compiler) {
        // Creating the method labels
        currentClass.getClassDefinition().createLabel(currentClass.getName().toString());
        
        for (AbstractDeclMethod declMethod : methods.getList()) {
            declMethod.getMethodName().getMethodDefinition().createLabels(
                    currentClass.getName().toString(),
                    declMethod.getMethodName().getName().toString());
            declMethod.setCodeGenOperands();
        }
        compiler.addComment("Method table for class " + currentClass.getName());
        MethodTable.codeGenMethodTable(compiler, currentClass.getClassDefinition());
    }

    @Override
    public void codeGenMethods(DecacCompiler compiler) {
        ClassDefinition classDef = currentClass.getClassDefinition();
        
        // Only generating code for initialization if the class has fields
        if (classDef.getNumberOfFields() > 0) {
            compiler.addProgramLabel(classDef.getInitLabel());
            compiler.newBlock();
            // Doing default initialization first to generate less instructions
            // Loading 0 into R0 for default initialization
            compiler.addInstruction(new LOAD(0, Register.R0));
            // Loading the object's address (stored at -2(LB) when calling the init function) into R1
            compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R1));
            for (AbstractDeclField declField : fields.getList()) {
                // Initializing all fields to 0 (if they have no initialization)
                declField.codeGenZeroInit(compiler);
            }
            // Explicit initializations
            for (AbstractDeclField declField : fields.getList()) {
                // Initializing all fields that have an expicit initialization
                declField.codeGenInit(compiler);
            }
            
            if (superClass.getClassDefinition().getNumberOfFields() > 0) {
                // Calling super class init
                compiler.setMaxParameters(3);
                compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R1));
                compiler.addInstruction(new PUSH(Register.R1));
                compiler.addInstruction(new BSR(superClass.getClassDefinition().getInitLabel()));
                compiler.addInstruction(new POP(Register.R1));
            }
            
            compiler.appendBlock(true, true);
        }
            
        // Generating code for all other methods
        methods.codeGenMethods(compiler);
    }
    
    @Override
    public AbstractIdentifier getCurrentClass(){
        return currentClass;
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        currentClass.iter(f);
        superClass.iter(f);
        fields.iter(f);
        methods.iter(f);
    }

}
