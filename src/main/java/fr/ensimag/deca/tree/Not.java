package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Not extends AbstractUnaryExpr {

    public Not(AbstractExpr operand) {
        super(operand);
    }
    
    /**
     * Partial operation for unary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeUnaryOp(DecacCompiler compiler, Type type2) throws ContextualError{
        if (type2.isBoolean()){
            return type2;
        } else {
            throw new ContextualError("Conflicting types : type " + type2 + " is incompatible for a negation operation,"
                    + " it should be a boolean type", this.getLocation());
        }
    }
    
    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.63)
    */ 
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type1 = this.getOperand().verifyExpr(compiler, localEnv, currentClass);
        type1 = verifyTypeUnaryOp(compiler, type1);
        this.setType(type1);
        checkDecoration();
        return type1;
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        // <Code(!C, b, E)> ≡ <Code(C, !b, E)>
        getOperand().codeGenExpBool(compiler, reg, !branchTruthValue, branchLabel);
    }

    @Override
    protected String getOperatorName() {
        return "!";
    }
}
