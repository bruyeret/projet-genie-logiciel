package fr.ensimag.deca.tree;

import fr.ensimag.deca.codegen.PrintGen;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

/**
 * Integer literal
 *
 * @author gl46
 * @date 01/01/2021
 */
public class IntLiteral extends AbstractExpr {
    public int getValue() {
        return value;
    }

    private int value;

    public IntLiteral(int value) {
        this.value = value;
    }

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.44)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeInt();
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    protected void codeGenPrint(DecacCompiler compiler, boolean printHex) {
        PrintGen.printInt(compiler, value);
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // do nothing
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        compiler.addInstruction(new LOAD(value, reg));
    }
    
    @Override
    protected DVal getDVal() {
        return new ImmediateInteger(value);
    }
    
    @Override
    String prettyPrintNode() {
        return "Int (" + getValue() + ")";
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(Integer.toString(value));
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
