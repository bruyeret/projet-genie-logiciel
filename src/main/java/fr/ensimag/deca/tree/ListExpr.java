package fr.ensimag.deca.tree;

import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * List of expressions (eg list of parameters).
 *
 * @author gl46
 * @date 01/01/2021
 */
public class ListExpr extends TreeList<AbstractExpr> {


    @Override
    public void decompile(IndentPrintStream s) {
        boolean comma = false;
    	for (AbstractExpr expr: getList()) {
    	    if (comma) {
    	        s.print(", ");
    	    }
    	    comma = true;
    		expr.decompile(s);
    	}
    }
}
