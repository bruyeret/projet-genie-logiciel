package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Return extends AbstractInst {
    private AbstractExpr expr;

    public Return(AbstractExpr expr) {
        Validate.notNull(expr);
        this.expr = expr;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // Computing the expression into R2
        compiler.setMaxRegisters(1);
        expr.codeGenExp(compiler, Register.getR(2));
        // Copying R2 to R0
        compiler.addInstruction(new LOAD(Register.getR(2), Register.R0));
        compiler.addInstruction(new BRA(compiler.getMethodDef().getEndLabel()));
    }

    /**
     * Pass 3 of [SyntaxeContextuelle] rule 3.24
     */
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        if (!returnType.isVoid()) {
            AbstractExpr expr = this.expr.verifyRValue(compiler, localEnv, currentClass, returnType);
            Type realType = expr.getType();
            if ((returnType.isFloat() && realType.isInt())){
                this.expr = new ConvFloat(this.expr);
                this.expr.setType(compiler.getEnvType().getTypeFloat());
            }
        } else {
        throw new ContextualError("Conflicting types : return type should not be void", this.getLocation());           
        }   
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("return ");
        expr.decompile(s);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        expr.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expr.prettyPrint(s, prefix, true);
    }

}
