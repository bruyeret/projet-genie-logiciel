package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.NEW;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.STORE;

public class New extends AbstractExpr {
    final private AbstractIdentifier className;

    public New(AbstractIdentifier className) {
        Validate.notNull(className);
        this.className = className;
    }
    
    @Override
    /*
    *   Pass 3 of [SyntaxeContextuelle] rule (3.42)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type = this.className.verifyType(compiler);          
            this.setType(type); 
            checkDecoration();
            if (!type.isClass()) {
                throw new ContextualError("Conflicting type : " + type + " is not a class type", this.getLocation());
            }
            return type;
        }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("new ");
        className.decompile(s);
        s.print("()");
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        ClassDefinition classDef = className.getClassDefinition();
        // Allocating memory
        compiler.addInstruction(new NEW(classDef.getNumberOfFields() + 1, reg));
        // Overflow check
        if (!compiler.getCompilerOptions().getNoCheck()) {
            CodeError.HEAP_OVERFLOW.codeGenBOV(compiler);
        }
        // Loading the address of the method table
        compiler.addInstruction(new LEA(classDef.getMethodTableAddress(), Register.R0));
        // Copying the method table address to the first cell of the allocated memory
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(0, reg)));
        // Initializing fields only if the class has fields
        if (classDef.getNumberOfFields() > 0) {
            // Pushing the address of the instance on stack (param at -2(LB))
            compiler.addInstruction(new PUSH(reg));
            // Executing the initialization special method
            compiler.addInstruction(new BSR(classDef.getInitLabel()));
            // Restoring the stack
            compiler.addInstruction(new POP(reg));
        }
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        className.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        className.iter(f);
    }
}
