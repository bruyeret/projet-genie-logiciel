package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.InstanceOfFunction;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import fr.ensimag.ima.pseudocode.instructions.SUBSP;

public class InstanceOf extends AbstractExpr {
    final private AbstractExpr object;
    final private AbstractIdentifier className;

    public InstanceOf(AbstractExpr object, AbstractIdentifier className) {
        Validate.notNull(object);
        Validate.notNull(className);
        this.object = object;
        this.className = className;
    }
    
    public Type verifyTypeInstanceofOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError {
        if (!(type1.isClassOrNull() && type2.isClass())) {
            throw new ContextualError("Conflicting types : the operation InstanceOf cannot be applied to the given types " 
                    + type1 + " and " + type2 + ", class types (or null type for the first type) were required", this.getLocation()); 
        }
       return compiler.getEnvType().getTypeBoolean(); 
    }
    
    @Override
     /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.40)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        Type type1 = this.object.verifyExpr(compiler, localEnv, currentClass);
        Type type2 = this.className.verifyType(compiler);
        Type type = verifyTypeInstanceofOp(compiler, type1, type2);
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        object.codeGenExp(compiler, reg);
        // 2 parameters + 2 additional words reserved by BSR
        compiler.setMaxParameters(4);
        // Reserving space for 2 arguments
        compiler.addInstruction(new ADDSP(2));
        // Storing the address of the object into the stack
        compiler.addInstruction(new STORE(reg, new RegisterOffset(0, Register.SP)));
        // Loading the method table address into reg
        compiler.addInstruction(new LEA(className.getClassDefinition().getMethodTableAddress(), reg));
        // Storing the method table address into the stack
        compiler.addInstruction(new STORE(reg, new RegisterOffset(-1, Register.SP)));
        // Function call
        // (null check is done inside the instanceof function)
        InstanceOfFunction.codeGenBSR(compiler);
        // Freeing the space reserved for arguments
        compiler.addInstruction(new SUBSP(2));
        if (reg != Register.R0) {
            // Copying the result into reg
            compiler.addInstruction(new LOAD(Register.R0, reg));
        }
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        codeGenExp(compiler, reg);
        if (branchTruthValue) {
            compiler.addInstruction(new BNE(branchLabel));
        }
        else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(" instanceof ");
        className.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        className.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        className.iter(f);
    }

}
