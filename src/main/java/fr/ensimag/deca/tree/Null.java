package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;

/**
 * Integer literal
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Null extends AbstractExpr {
    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.48)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeNull();
        setType(type);
        checkDecoration();
        return type;
    }
    
    @Override
    String prettyPrintNode() {
        return "Null";
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("null");
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // do nothing
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        compiler.addInstruction(new LOAD(new NullOperand(), reg));
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

}
