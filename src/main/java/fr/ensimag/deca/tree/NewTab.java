package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.TabType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.TypeDefinition;
import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADD;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.NEW;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import fr.ensimag.ima.pseudocode.instructions.SUB;

public class NewTab extends AbstractExpr {
    final private AbstractIdentifier tabType;
    final private AbstractExpr length;

    public NewTab(AbstractIdentifier tabType, AbstractExpr length) {
        Validate.notNull(tabType);
        Validate.notNull(length);
        this.tabType = tabType;
        this.length = length;
    }
    
    @Override
    /*
    *   Pass 3 of [SyntaxeContextuelle] rule (3.42)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type = this.tabType.verifyType(compiler); 
            TabType myType = compiler.getEnvType().getTypeTab(type);
            if (myType == null){
                throw new ContextualError("Conflicting type : " + type + " is not a table type", this.getLocation());
            }
            TypeDefinition tabDef = compiler.getEnvType().getDef(myType.getName().getName());
            if (tabDef != null){
                this.setType(type); 
                Type typeR = this.length.verifyExpr(compiler, localEnv, currentClass);
                checkDecoration();
                if (!myType.isTab()) {
                    throw new ContextualError("Conflicting type : " + type + " is not a table type", this.getLocation());
                }
                if (!typeR.isInt()){
                    throw new ContextualError("Conflicting type : " + typeR + " is not an int type", this.getLocation());
                }
                return myType;
            } else {
                throw new ContextualError("Table type " + myType + " does not exist", this.getLocation());
            }
        }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("new ");
        tabType.decompile(s);
        s.print("()");
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Computing the length into reg
        length.codeGenExp(compiler, reg);
        // Adding 1 to the length for allocation
        compiler.addInstruction(new ADD(1, reg));
        // Allocating memory and storing the address into R0
        compiler.addInstruction(new NEW(reg, Register.R0));
        if (!compiler.getCompilerOptions().getNoCheck()) {
            CodeError.HEAP_OVERFLOW.codeGenBOV(compiler);
        }
        // Removing 1 from the length for storing it
        compiler.addInstruction(new SUB(1, reg));
        // Storing the length into the 1st cell
        compiler.addInstruction(new STORE(reg, new RegisterOffset(0, Register.R0)));
        // Copying back the address into reg
        compiler.addInstruction(new LOAD(Register.R0, reg));
    }
    
    
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        tabType.prettyPrint(s, prefix, false);
        length.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        tabType.iter(f);
        length.iter(f);
    }
}

