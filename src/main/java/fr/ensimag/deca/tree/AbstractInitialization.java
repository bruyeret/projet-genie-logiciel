package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Initialization (of variable, field, ...)
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractInitialization extends Tree {
    public abstract boolean isEmpty();
    
    /**
     * Implements non-terminal "initialization" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains "env_types" attribute
     * @param t corresponds to the "type" attribute
     * @param localEnv corresponds to the "env_exp" attribute
     * @param currentClass 
     *          corresponds to the "class" attribute (null in the main bloc).
     */
    protected abstract void verifyInitialization(DecacCompiler compiler,
            Type t, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Generates code that computes the initialization
     * value and stores it into the DAddr specified
     * @param compiler
     * @param addr
     */
    protected abstract void codeGenInitialization(DecacCompiler compiler, DAddr addr);
    
    /**
     * Generates code that computes the initialization
     * value and stores it into the field
     * The object address is supposed to be at -2(LB)
     * @param compiler
     * @param fieldIndex
     */
    protected abstract void codeGenInitField(DecacCompiler compiler, int fieldIndex);
}
