package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractOpExactCmp extends AbstractOpCmp {

    public AbstractOpExactCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }
    
    /**
     * Partial operation for binary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeBinaryOp(DecacCompiler compiler, Type type1, Type type2) throws ContextualError{
        try {
            return super.verifyTypeBinaryOp(compiler, type1, type2);
        }
        catch (ContextualError e) {
            if (type1.isBoolean() && type2.isBoolean()) {
                return compiler.getEnvType().getTypeBoolean();
            } else if (type1.isClassOrNull() && type2.isClassOrNull()) {
                return compiler.getEnvType().getTypeBoolean(); 
            } else {
                throw new ContextualError("Conflicting types : types for a " + getOperatorName() + " operation should be int or float, or both boolean, but are " 
                            + type1 + " and " + type2, this.getLocation());
            }
        }
    }


}
