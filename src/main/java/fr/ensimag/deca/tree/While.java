package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class While extends AbstractInst {
    private AbstractExpr condition;
    private ListInst body;

    public AbstractExpr getCondition() {
        return condition;
    }

    public ListInst getBody() {
        return body;
    }

    public While(AbstractExpr condition, ListInst body) {
        Validate.notNull(condition);
        Validate.notNull(body);
        this.condition = condition;
        this.body = body;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // <Code(while (C) {I})>
        int labelID = compiler.getLabelID();
        Label startLabel = new Label("start_while_" + labelID);
        Label condLabel = new Label("cond_while_" + labelID);
        
        // BRA E_Cond
        compiler.addInstruction(new BRA(condLabel));
        // E_Debut :
        compiler.addLabel(startLabel);
        // <Code(I)>
        body.codeGenListInst(compiler);
        // E_Cond :
        compiler.addLabel(condLabel);
        // <Code(C, vrai, E_debut)>
        // Computing condition into R2
        compiler.setMaxRegisters(1);
        condition.codeGenExpBool(compiler, Register.getR(2), true, startLabel);
    }

    /**
     * Pass 3 of [SyntaxeContextuelle] rule 3.25
     */
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        this.condition.verifyCondition(compiler, localEnv, currentClass);
        this.body.verifyListInst(compiler, localEnv, currentClass, returnType);
    }

    @Override
    public void decompileInst(IndentPrintStream s) {
        decompile(s);
        // No semicolon
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("while (");
        getCondition().decompile(s);
        s.println(") {");
        s.indent();
        getBody().decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        condition.iter(f);
        body.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        condition.prettyPrint(s, prefix, false);
        body.prettyPrint(s, prefix, true);
    }

}
