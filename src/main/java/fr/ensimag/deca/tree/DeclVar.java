package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.DAddr;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * @author gl46
 * @date 01/01/2021
 */
public class DeclVar extends AbstractDeclVar {

    
    final private AbstractIdentifier type;
    final private AbstractIdentifier varName;
    final private AbstractInitialization initialization;

    public DeclVar(AbstractIdentifier type, AbstractIdentifier varName, AbstractInitialization initialization) {
        Validate.notNull(type);
        Validate.notNull(varName);
        Validate.notNull(initialization);
        this.type = type;
        this.varName = varName;
        this.initialization = initialization;
    }

    @Override
    /**
     * Pass 3 of [Syntaxe Contextuelle] rule (3.17)
     * @param compiler Contains env_types
     * @param localEnv
     *   its "parentEnvironment" corresponds to "env_exp_sup" attribute
     *   in precondition, its "current" dictionary corresponds to 
     *      the "env_exp" attribute
     *   in postcondition, its "current" dictionary corresponds to 
     *      the "{name → (var, type)} ⊕ env_exp"
     * @param currentClass 
     *          corresponds to "class" attribute (null in the main bloc).
     */
    protected void verifyDeclVar(DecacCompiler compiler, EnvironmentExp superEnv,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // type ↓env_types ↑type
        Type myType = this.type.verifyType(compiler);
        this.type.setType(myType);
        // condition type != void
        if (myType.isVoid()) {
            throw new ContextualError("The type of a variable can not be void", getLocation());
        }
        // Identifier ↑name
        this.varName.setType(myType);
        // initialization ↓env_types ↓env_exp/env_exp_sup ↓class ↓type
        EnvironmentExp newEnv = localEnv.stacking(superEnv);
        this.initialization.verifyInitialization(compiler, myType, newEnv, currentClass);
        // Setting the VariableDefinition
        VariableDefinition myDef = new VariableDefinition(myType, getLocation());
        this.varName.setDefinition(myDef);
        
        // Declaring into the environment
        Symbol symbol = varName.getName();
        try { 
            localEnv.declare(symbol, myDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            throw new ContextualError("Symbol " + symbol.getName() + " has already been declared", this.getLocation());
        }
    }
    
	/**
	 * Sets the operand of the variable.
	 * (used for code generation)
	 * varName must extend ExpDefinition
	 * @param operand
	 */
    @Override
    public void setCodeGenOperand(DAddr operand) {
    	ExpDefinition def = varName.getExpDefinition();
    	if (def == null) {
    		throw new DecacInternalError("No definition found for variable when trying to set operand.");
    	}
    	def.setOperand(operand);
    }
    
    /**
     * Generates code for variable initialization
     * varName must extend ExpDefinition
     * @param compiler
     */
    public void codeGenDeclVar(DecacCompiler compiler) {
    	initialization.codeGenInitialization(compiler, varName.getExpDefinition().getOperand());
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
    	type.decompile(s);
    	s.print(" ");
    	varName.decompile(s);
    	initialization.decompile(s);
    	s.println(";");
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        type.iter(f);
        varName.iter(f);
        initialization.iter(f);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        varName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }
}
