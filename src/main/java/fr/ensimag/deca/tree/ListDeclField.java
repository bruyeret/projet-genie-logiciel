package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;

public class ListDeclField extends TreeList<AbstractDeclField> {
    
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.4)
     */
    public EnvironmentExp verifyListFieldMembers(DecacCompiler compiler, ClassDefinition superClass,
            ClassDefinition currentClass) throws ContextualError {
            EnvironmentExp localEnv = new EnvironmentExp(superClass.getMembers());
            for (AbstractDeclField declField : this.getList()){
                AbstractIdentifier idField = declField.verifyFieldMembers(compiler, superClass, currentClass);
                ExpDefinition def = new FieldDefinition(idField.getType(), idField.getLocation(), declField.getVisibility(), currentClass, currentClass.getNumberOfFields());
                declField.getFieldName().setDefinition(def);
                try {  
                    localEnv.declare(idField.getName(), def);
                } catch(EnvironmentExp.DoubleDefException ex) {
                    throw new ContextualError("Symbol " + idField.getName().getName() + " has already been declared", idField.getLocation());
                }
                 
            }
        return localEnv; 
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.6)
     */
    public void verifyListFieldBody(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        for (AbstractDeclField declField : this.getList()){
            declField.verifyFieldBody(compiler, localEnv, currentClass);
        } 
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclField declField: getList()) {
            declField.decompile(s);
        }
    }
}