package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Deca Identifier
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Identifier extends AbstractIdentifier { 
    @Override
    protected void checkDecoration() {
        if (getDefinition() == null) {
            throw new DecacInternalError("Identifier " + this.getName() + " has no attached Definition");
        }
    }

    @Override
    public Definition getDefinition() {
        return definition;
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * ClassDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a class definition.
     */
    @Override
    public ClassDefinition getClassDefinition() {
        try {
            return (ClassDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a class identifier, you can't call getClassDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * MethodDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a method definition.
     */
    @Override
    public MethodDefinition getMethodDefinition() {
        try {
            return (MethodDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a method identifier, you can't call getMethodDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * FieldDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public FieldDefinition getFieldDefinition() {
        try {
            return (FieldDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a field identifier, you can't call getFieldDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * VariableDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public VariableDefinition getVariableDefinition() {
        try {
            return (VariableDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a variable identifier, you can't call getVariableDefinition on it");
        }
    }
    
    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * ParamDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public ParamDefinition getParamDefinition() {
        try {
            return (ParamDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a parameter identifier, you can't call getParamDefinition on it");
        }
    }

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a ExpDefinition.
     * 
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     * 
     * @throws DecacInternalError
     *             if the definition is not a field definition.
     */
    @Override
    public ExpDefinition getExpDefinition() {
        try {
            return (ExpDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a Exp identifier, you can't call getExpDefinition on it");
        }
    }

    @Override
    public void setDefinition(Definition definition) {
        this.definition = definition;
    }

    @Override
    public Symbol getName() {
        return name;
    }

    private Symbol name;

    public Identifier(Symbol name) {
        Validate.notNull(name);
        this.name = name;
    }

    /**
     * Implements non-terminal "Identifier" of [SyntaxeContextuelle] in the 3 passes, rule (0.1)
     * @param compiler contains "env_types" attribute
     */
    @Override
    public Definition verifyIdentifier(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        ExpDefinition def = localEnv.get(this.getName());
        if (def == null) {
            throw new ContextualError("Identifier " + getName() + " was not declared.", getLocation());
        }
        setDefinition(def);
        setType(def.getType());
        checkDecoration();
        return def;
    }

    /**
     * Implements non-terminal "type" of [SyntaxeContextuelle] in the 3 passes, rule (0.2)
     * @param compiler contains "env_types" attribute
     */
    @Override
    public Type verifyType(DecacCompiler compiler) throws ContextualError {
        Definition myDef = compiler.getEnvType().getDef(this.getName().getName());
        if (myDef == null){
            throw new ContextualError("The type " + this.getName().getName() + " does not exist", this.getLocation());
        }
        Type myType = myDef.getType();
        this.setDefinition(myDef);
        checkDecoration();
        return myType;
    }
    
    /**
     * Pass 3 of [SyntaxContextuelle] rule (3.64) (3.67) (3.68) (3.69) 
     */
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) 
            throws ContextualError{
        Definition myDef = this.verifyIdentifier(compiler, localEnv, currentClass);
        if (this.getDefinition().isField() || this.getDefinition().isParam() || this.getDefinition().isVar()){
            if (myDef.getType().sameType(this.getDefinition().getType())){
                return myDef.getType();
            } else {
                throw new ContextualError("Conflicting types : type should be " + myDef.getType() + " but is " 
                        + this.getDefinition().getType(), this.getLocation());
            }
        } else {
            throw new ContextualError("Type " + myDef.getType() + " was encountered, but a field type,"
                    + " parameter type or variable type was expected", this.getLocation());
        }
    }    
    
    private Definition definition;


    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    /**
     * Can throw exception if Identifier is not an expression
     */
    @Override
    protected DVal getDVal() {
        if (definition.isVar() || definition.isParam()) {
            return getExpDefinition().getOperand();
        }
        return null;
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // If the identifier is a global or local variable
        if (definition.isVar() || definition.isParam()) {
            // Load the value into reg
            compiler.addInstruction(new LOAD(getDVal(), reg));
        }
        // If the identifier is a field
        else if (definition.isField()) { 
            // Load 'this' into reg
            compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), reg));
            // Load the field into reg
            compiler.addInstruction(new LOAD(new RegisterOffset(((FieldDefinition) definition).getIndex(), reg), reg));
        }
        else {
            throw new DecacInternalError("Identifier is neither a variable, a field or a param, cannot generate expression.");
        }
    }
    
    @Override
    protected void codeGenAssign(DecacCompiler compiler, GPRegister reg, AbstractExpr rvalue, boolean isInst) {
        rvalue.codeGenExp(compiler, reg);
        // If the identifier is a global or local variable
        if (definition.isVar() || definition.isParam()) {
            // Load the value into reg
            compiler.addInstruction(new STORE(reg, (DAddr) getDVal()));
        }
        // If the identifier is a field
        else if (definition.isField()) { 
            // Load 'this' into R0
            compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R0));
            // Load the field into reg
            compiler.addInstruction(new STORE(reg, new RegisterOffset(((FieldDefinition) definition).getIndex(), Register.R0)));
        }
    }
    
    @Override
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        if (!getType().isBoolean()) {
            throw new DecacInternalError("MethodCall.codeGenExpBool was called with a method whose return type is not boolean.");
        }
        // <Code(idf, b, E)> ≡
        // LOAD @idf, R0
        codeGenExp(compiler, reg);
        // CC flags are set, no need to use CMP
        // b = vrai
        if (branchTruthValue) {
            // BNE E
            compiler.addInstruction(new BNE(branchLabel));
        }
        // b = faux
        else {
            // BEQ E
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print(name.toString());
    }

    @Override
    String prettyPrintNode() {
        return "Identifier (" + getName() + ")";
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Definition d = getDefinition();
        if (d != null) {
            s.print(prefix);
            s.print("definition: ");
            s.print(d);
            s.println();
        }
    }

}
