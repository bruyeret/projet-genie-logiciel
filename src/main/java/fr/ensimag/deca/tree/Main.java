package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp; // for verify : need of EnvironmentExp objects
import fr.ensimag.deca.context.ClassDefinition; // for verify : need of ClassDefinition objects
import fr.ensimag.deca.tools.IndentPrintStream;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

/**
 * @author gl46
 * @date 01/01/2021
 */
public class Main extends AbstractMain {
    private static final Logger LOG = Logger.getLogger(Main.class);
    
    private ListDeclVar declVariables;
    private ListInst insts;
    public Main(ListDeclVar declVariables,
            ListInst insts) {
        Validate.notNull(declVariables);
        Validate.notNull(insts);
        this.declVariables = declVariables;
        this.insts = insts;
    }

    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.4) and (3.18) 
     */
    @Override
    protected void verifyMain(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify Main: start");
        EnvironmentExp superEnv = new EnvironmentExp(null);
        EnvironmentExp localEnv = new EnvironmentExp(superEnv);
        EnvironmentExp varEnv = this.declVariables.verifyListDeclVariable(compiler, superEnv, localEnv, new ClassDefinition(null, null, null));
        // local env aura été modifié par verify, il n'y a donc pas besoin d'empiler
        EnvironmentExp mainEnv = varEnv.stacking(localEnv);
        LOG.debug("\nEnvironment du main : \n" + mainEnv.helpMePrintString());
        this.insts.verifyListInst(compiler, mainEnv, new ClassDefinition(null, null, null), 
                compiler.getEnvType().getTypeVoid());
        LOG.debug("verify Main: end");
    }

    @Override
    protected void codeGenMain(DecacCompiler compiler) {
        compiler.addComment("Global variables initialization:");
        declVariables.codeGenListDeclVar(compiler, true);
        compiler.addComment("Main instructions:");
        insts.codeGenListInst(compiler);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.println("{");
        s.indent();
        declVariables.decompile(s);
        insts.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        declVariables.iter(f);
        insts.iter(f);
    }
 
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        declVariables.prettyPrint(s, prefix, false);
        insts.prettyPrint(s, prefix, true);
    }
}
