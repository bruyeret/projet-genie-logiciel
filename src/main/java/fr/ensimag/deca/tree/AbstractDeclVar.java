package fr.ensimag.deca.tree;

import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Variable declaration
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractDeclVar extends Tree {
	
	/**
	 * Sets the operand of the variable.
	 * (used for code generation)
	 * @param operand
	 */
	public abstract void setCodeGenOperand(DAddr operand);
	
    /**
     * Generates code for variable initialization
     * @param compiler
     */	
	public abstract void codeGenDeclVar(DecacCompiler compiler);
	
    /**
     * Implements non-terminal "decl_var" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains "env_types" attribute
     * @param localEnv 
     *   its "parentEnvironment" corresponds to the "env_exp_sup" attribute
     *   in precondition, its "current" dictionary corresponds to 
     *      the "env_exp" attribute
     *   in postcondition, its "current" dictionary corresponds to 
     *      the synthetized attribute
     * @param currentClass 
     *          corresponds to the "class" attribute (null in the main bloc).
     */    
    protected abstract void verifyDeclVar(DecacCompiler compiler, EnvironmentExp superEnv,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;
}
