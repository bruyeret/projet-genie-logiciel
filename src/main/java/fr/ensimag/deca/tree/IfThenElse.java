package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Full if/else if/else statement.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class IfThenElse extends AbstractInst {
    
    private final AbstractExpr condition; 
    private final ListInst thenBranch;
    private ListInst elseBranch;

    public IfThenElse(AbstractExpr condition, ListInst thenBranch, ListInst elseBranch) {
        Validate.notNull(condition);
        Validate.notNull(thenBranch);
        Validate.notNull(elseBranch);
        this.condition = condition;
        this.thenBranch = thenBranch;
        this.elseBranch = elseBranch;
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.22)
     */
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        this.condition.verifyCondition(compiler, localEnv, currentClass);
        this.thenBranch.verifyListInst(compiler, localEnv, currentClass, returnType);
        this.elseBranch.verifyListInst(compiler, localEnv, currentClass, returnType);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // <Code(if (C) {It} else {Ie})>
        int labelID = compiler.getLabelID();
        Label endLabel = new Label("end_if_" + labelID);
        Label elseLabel = endLabel;
        if (elseBranch.size() > 0) {
            elseLabel = new Label("else_" + labelID);
        }
        
        
        
        // <Code(C1 , faux, E_Sinon)>
        // Computing condition into R2 
        compiler.setMaxRegisters(1);
        condition.codeGenExpBool(compiler, Register.getR(2), false, elseLabel);
        // <Code(It)>
        thenBranch.codeGenListInst(compiler);
        // BRA E_fin
        compiler.addInstruction(new BRA(endLabel));
        
        if (elseBranch.size() > 0) {
            // E_Sinon
            compiler.addLabel(elseLabel);
            // <Code(Ie)>
            elseBranch.codeGenListInst(compiler);
        }
        // E_Fin
        compiler.addLabel(endLabel);
    }

    @Override
    public void decompileInst(IndentPrintStream s) {
        decompile(s);
        // No semicolon
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
    	s.print("if (");
    	condition.decompile(s);
    	s.println(") {");
    	s.indent();
    	thenBranch.decompile(s);
    	s.unindent();
    	s.println("}");
    	if (elseBranch.size() > 0) {
    		s.println("else {");
    		s.indent();
    		elseBranch.decompile(s);
    		s.unindent();
    		s.println("}");
    	}
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        condition.iter(f);
        thenBranch.iter(f);
        elseBranch.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        condition.prettyPrint(s, prefix, false);
        thenBranch.prettyPrint(s, prefix, false);
        elseBranch.prettyPrint(s, prefix, true);
    }
}
