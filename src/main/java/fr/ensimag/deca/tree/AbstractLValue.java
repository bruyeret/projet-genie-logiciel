package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;

/**
 * Left-hand side value of an assignment.
 * 
 * @author gl46
 * @date 01/01/20compiler21
 */
public abstract class AbstractLValue extends AbstractExpr {
    /**
     * Generates instructions that computes the
     * lvalue address, then the rvalue, and finally
     * stores the rvalue into the lvalue address.
     * @param compiler
     * @param reg The result of the rvalue should be in reg at the end of the method
     * @param isInst If true, the result doesn't need to be in reg after the assignment
     */
    protected abstract void codeGenAssign(DecacCompiler compiler, GPRegister reg, AbstractExpr rvalue, boolean isInst);
}
