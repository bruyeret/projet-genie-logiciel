package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.DIV;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.QUO;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Divide extends AbstractOpArith {
    public Divide(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }


    @Override
    protected String getOperatorName() {
        return "/";
    }
    
    @Override
    protected void codeGenOpArith(DecacCompiler compiler, GPRegister op1, DVal op2) {
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Loading op2 into R1 for 0 comparison
            compiler.addInstruction(new LOAD(op2, Register.R1));
            CodeError.ZERO_DIVISION.codeGenBEQ(compiler);
        }
        if (getType().isInt()) {
            compiler.addInstruction(new QUO(op2, op1));
        }
        else if (getType().isFloat()) {
            compiler.addInstruction(new DIV(op2, op1));
        }
        else {
            throw new DecacInternalError("Wrong expression types for division operation.");
        }
    }
}
