package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import java.io.PrintStream;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.STORE;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.tools.IndentPrintStream;

public class DeclField extends AbstractDeclField {
    final private Visibility visibility;
    final private AbstractIdentifier type;
    final private AbstractIdentifier fieldName;
    final private AbstractInitialization initialization;
    
    public DeclField(Visibility visibility, AbstractIdentifier type,
            AbstractIdentifier fieldName, AbstractInitialization initialization) {
        Validate.notNull(visibility);
        Validate.notNull(type);
        Validate.notNull(fieldName);
        Validate.notNull(initialization);
        this.visibility = visibility;
        this.type = type;
        this.fieldName = fieldName;
        this.initialization = initialization;
    }
    
    @Override
    public AbstractIdentifier getFieldName() {
        return this.fieldName;
    }
    
    @Override
    public Visibility getVisibility() {
        return this.visibility;
    }
    
    @Override
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.5)
     */
    protected AbstractIdentifier verifyFieldMembers(DecacCompiler compiler, ClassDefinition superClass,
            ClassDefinition currentClass)
            throws ContextualError {
        Type type = this.type.verifyType(compiler);
        this.fieldName.setType(type);
        // Creation of condition
        boolean conditionSi1 = currentClass.getSuperClass().isClass();
        boolean conditionSi2 = !(superClass.getMembers().get(fieldName.getName()) == null); 
        boolean conditionSi = conditionSi1 && conditionSi2;
        boolean conditionAlors;
        // Check if the field is already existing in the mother class
        if (superClass.getMembers().get(fieldName.getName()) != null){
            conditionAlors = superClass.getMembers().get(fieldName.getName()).isField();
        } else {
            conditionAlors = false;
            // Increment number op field
            currentClass.incNumberOfFields();
        }
        // check if the types are compatibles for a field
        if (!type.isVoid() && conditionSi) {
            if (conditionAlors) {
                return this.fieldName;
            } else {
                throw new ContextualError("A field type in the super class " + superClass.getType() 
                        + " is expected, and not " + type , this.getLocation());
            }     
        } else if (!type.isVoid()) {
            return this.fieldName; 
        } else {
              throw new ContextualError("The type of a field can not be void" , this.getLocation());
        }
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.7)
     */
    @Override
    public void verifyFieldBody(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type myType = this.type.verifyType(compiler);
        this.fieldName.setType(myType);
        this.initialization.verifyInitialization(compiler, myType, localEnv, currentClass);
    }

    @Override
    protected void codeGenZeroInit(DecacCompiler compiler) {
        // fieldName should have a definition since
        // checkDecoration is called during part B
        FieldDefinition fieldDef = fieldName.getFieldDefinition();
        if (initialization.isEmpty()) {
            compiler.addInstruction(new STORE(Register.R0,
                    new RegisterOffset(fieldDef.getIndex(), Register.R1)));
        }
    }
    
    @Override
    protected void codeGenInit(DecacCompiler compiler) {
        // fieldName should have a definition since
        // checkDecoration is called during part B
        initialization.codeGenInitField(compiler, fieldName.getFieldDefinition().getIndex());
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        fieldName.iter(f);
        initialization.iter(f);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        if (visibility == Visibility.PROTECTED) {
            s.print("protected ");
        }
        type.decompile(s);
        s.print(" ");
        fieldName.decompile(s);
        initialization.decompile(s);
        s.println(";");
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        s.print(visibility);
        s.print(" ");
        type.prettyPrint(s, prefix, false);
        fieldName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }
}