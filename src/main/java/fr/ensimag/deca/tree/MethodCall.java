package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import fr.ensimag.ima.pseudocode.instructions.SUBSP;

public class MethodCall extends AbstractExpr {
    final private AbstractExpr object;
    final private AbstractIdentifier methodName;
    final private ListExpr params; 
    
    public MethodCall(AbstractExpr object, AbstractIdentifier methodName, ListExpr params) {
        Validate.notNull(object);
        Validate.notNull(methodName);
        Validate.notNull(params);
        this.object = object;
        this.methodName = methodName;
        this.params = params;
    }

    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.71)
     */
    public Type verifyExpr(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // Verification of the object on which the method is called
        Type typeClass2 = this.object.verifyExpr(compiler, localEnv, currentClass);
        // Verification of the method itself
        if (typeClass2.isClass()){
            // To get the environment related to the method
            Definition myDef = compiler.getEnvType().getDef(typeClass2.getName().getName());
            EnvironmentExp env2 = ((ClassDefinition) myDef).getMembers();
            // method identifier verification
            MethodDefinition method = this.verifyMethodIdent(compiler, env2, currentClass);
            Type type = method.getType();
            // verification of the parameters
            Signature sig = method.getSignature();
            this.verifyRValueStar(compiler, localEnv, currentClass, sig);
            // setting type and definition to the method
            this.methodName.setType(type);
            this.methodName.setDefinition(method);
            this.setType(type);
            return type;
        } else {
            throw new ContextualError("Conflicting type : " + typeClass2 + " is not a class type", this.getLocation());
        }
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.72)
     */
    protected MethodDefinition verifyMethodIdent(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        Definition def = this.methodName.verifyIdentifier(compiler, localEnv, currentClass);
        if (!def.isMethod()) {
            throw new ContextualError("Conflicting definition : " + def + " is not a method definition", this.getLocation());
        }
        this.methodName.setDefinition((MethodDefinition) def);
        return (MethodDefinition) def;
    }  
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.73) and (3.74)
     */
    public void verifyRValueStar(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass, 
            Signature sig) throws ContextualError {
        if (sig.size() == 0){
            if (!params.isEmpty()){
                throw new ContextualError("Parameter for the method " + methodName.getName().getName() +
                        " were found, but none was expected", this.getLocation());
            }
            // nothing to do
        } else {
            // there is one or several arguments
            int idx = 0;
            if (params.isEmpty()){ // no parameters encountered
                throw new ContextualError("Parameter for the method " + methodName.getName().getName() +
                        " were expected, but none was encountered", this.getLocation());
            }
            // parameters found
            for (AbstractExpr singleParam : params.getList()){
                if (sig.size() == 0) {
                    throw new ContextualError("Method " + this.methodName.getName().getName() + " was called "
                            + "with too many arguments", this.getLocation());
                } else {
                    if (idx >= sig.size()){
                        throw new ContextualError("Method " + this.methodName.getName().getName() + " was called "
                                + "with too few arguments", this.getLocation());
                    }
                    // comparaison between the signature of the method and the types of the encountered parameters
                    Type head = sig.paramNumber(idx);
                    singleParam = singleParam.verifyRValue(compiler, localEnv, currentClass, head);
                    // ConvFloat if needed
                    if (head.isFloat() && singleParam.getType().isInt()){
                        singleParam = new ConvFloat(singleParam);
                        singleParam.setType(compiler.getEnvType().getTypeFloat());
                        params.set(idx, singleParam);
                    }
                    idx++;
                }
            }
        }
    }
    
    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        MethodDefinition methodDef = methodName.getMethodDefinition();
        int numberOfParams = methodDef.getSignature().size();
        // number of parameters + the implicit parameter + 2 additional words reserved by BSR
        compiler.setMaxParameters(numberOfParams + 3);
        // Reserving space of all the parameters
        compiler.addInstruction(new ADDSP(numberOfParams + 1));
        // Computing the address of the object into reg
        object.codeGenExp(compiler, reg);
        // Storing the address of the object into 0(SP) (will become -2(LB) after BSR)
        compiler.addInstruction(new STORE(reg, new RegisterOffset(0, Register.SP)));
        // Computing parameters (from left to right) and storing them into the stack
        int i = -1;
        for (AbstractExpr param : params.getList()) {
            param.codeGenExp(compiler, reg);
            compiler.addInstruction(new STORE(reg, new RegisterOffset(i, Register.SP)));
            i--;
        }
        // Checking if object is null
        compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.SP), reg));
        // No need to use CMP because EQ flag is set by LOAD
        if (!compiler.getCompilerOptions().getNoCheck()) {
            CodeError.NULL_DEREFERENCING.codeGenBEQ(compiler);
        }
        // Loading the method table address
        compiler.addInstruction(new LOAD(new RegisterOffset(0, reg), reg));
        // Calling the method
        compiler.addInstruction(new BSR(new RegisterOffset(methodDef.getIndex() + 1, reg)));
        // Copying the return value from R0 to reg
        if (!methodDef.getType().isVoid() && reg != Register.R0) {
            compiler.addInstruction(new LOAD(Register.R0, reg));
        }
        // Freeing the space reserved for the parameters
        compiler.addInstruction(new SUBSP(numberOfParams + 1));
    }
    
    @Override
    public void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        if (!getType().isBoolean()) {
            throw new DecacInternalError("MethodCall.codeGenExpBool was called with a method whose return type is not boolean.");
        }
        // Computing the result of the method call into R0 so we don't have to copy
        codeGenExp(compiler, Register.R0);
        compiler.addInstruction(new CMP(0, Register.R0));
        if (branchTruthValue) {
            compiler.addInstruction(new BNE(branchLabel));
        }
        else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        compiler.setMaxRegisters(1);
        codeGenExp(compiler, Register.getR(2));
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        if (!object.isImplicit()) {
            object.decompile(s);
            s.print(".");
        }
        methodName.decompile(s);
        s.print("(");
        params.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        methodName.prettyPrint(s, prefix, false);
        params.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        methodName.iter(f);
        params.iter(f);
    }

}
