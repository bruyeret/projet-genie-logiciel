package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.instructions.OPP;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * @author gl46
 * @date 01/01/2021
 */
public class UnaryMinus extends AbstractUnaryExpr {

    public UnaryMinus(AbstractExpr operand) {
        super(operand);
    }
    
    /**
     * Partial operation for unary operations [SyntaxeContextuelle]
     */
    @Override
    public Type verifyTypeUnaryOp(DecacCompiler compiler, Type type2) throws ContextualError{
        if (type2.isFloat()){
            return type2;
        } else if (type2.isInt()){
            return type2;
        } else {
            throw new ContextualError("Conflicting types : type " + type2 + " is incompatible for a unary minus, "
                    + "it should be int or float", this.getLocation());
        }
    }
    

    @Override
    /* 
    * Pass 3 of [SyntaxeContextuelle] rule (3.62)
    */ 
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
            Type type1 = this.getOperand().verifyExpr(compiler, localEnv, currentClass);
            type1 = verifyTypeUnaryOp(compiler, type1);
            this.setType(type1);
            checkDecoration();
            return type1;
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        getOperand().codeGenExp(compiler, reg);
        compiler.addInstruction(new OPP(reg, reg));
    }
    
    @Override
    protected String getOperatorName() {
        return "-";
    }

}
