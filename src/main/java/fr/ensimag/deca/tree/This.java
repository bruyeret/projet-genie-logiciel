package fr.ensimag.deca.tree;

import java.io.PrintStream;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

public class This extends AbstractExpr {
    final private boolean implicit;
    
    public This(boolean implicit) {
        this.implicit = implicit;
    }
    
    @Override
    boolean isImplicit() {
        return implicit;
    }
    
    @Override
    /*
    *   Pass 3 of [SyntaxeContextuelle] rule (3.43)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = currentClass.getType();
        if (type == null) {
            throw new ContextualError("Class should not be 0", this.getLocation());
        }
        this.setType(type);
        checkDecoration();
        return type; 
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Loading the address of 'this' (which is at -2(LB) during a method call) into reg
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), reg));
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        if (!implicit) {
            s.print("this");
        }
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

}
