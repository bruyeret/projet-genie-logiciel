package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.RegisterOffset;

public class Selection extends AbstractHeapLValue {
    final private AbstractExpr object;
    final private AbstractIdentifier fieldName;
    
    public Selection(AbstractExpr object, AbstractIdentifier fieldName) {
        Validate.notNull(object);
        Validate.notNull(fieldName);
        this.object = object;
        this.fieldName = fieldName;
    }
    
    /**
     * Pass 3 of [SyntaxContextuelle] rule (3.65) (3.66) 
     */
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) 
            throws ContextualError{
        Type myType = this.object.verifyExpr(compiler, localEnv, currentClass);
        if (myType.isClass()){
            // Extraction of the class environment related to the selection
            Definition myDef = compiler.getEnvType().getDef(myType.getName().getName());
            ClassType myClass2Type = ((ClassDefinition) myDef).getType();
            EnvironmentExp env2 = ((ClassDefinition) myDef).getMembers();
            // Verification of the selected field
            FieldDefinition myFieldDef = this.verifyFieldIdent(compiler, env2, currentClass);
            Visibility myVisibility = myFieldDef.getVisibility();
//            // DEBUG BEGIN
//            System.out.println("Selection of the " + myVisibility + " field " + this.fieldName.getName().getName() 
//                    + " of the class " + myType);
//            env2.helpMePrint();
//            // DEBUG END
            if (myVisibility == Visibility.PUBLIC){
            // nothing to do : the condition is already used
            } else if (myVisibility == Visibility.PROTECTED){
                boolean firstCond = myClass2Type.isSubtype(currentClass.getType());
                boolean secondCond;
                if (firstCond){
                    secondCond = currentClass.getType().isSubtype(myFieldDef.getContainingClass().getType());
                } else {
                    secondCond = false;
                }
                if (!firstCond || !secondCond){
                    throw new ContextualError("Access refused to the field " + this.fieldName.getName().getName() 
                            + " of the class " + myType + " : " + this.fieldName.getName().getName() +" is protected",
                            this.getLocation());
                }
            }
            this.setType(myFieldDef.getType());
            return myFieldDef.getType();
        } else {
            throw new ContextualError(" A class type was expected, and not a " + myType + " type",
                    this.getLocation());
        }
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.70)
     */
    public FieldDefinition verifyFieldIdent(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError{
        Definition def  = this.fieldName.verifyIdentifier(compiler, localEnv, currentClass);
        if (!def.isField()) {
            throw new ContextualError("Conflicting definition : " + this.fieldName.getName().getName() + " has for definition '" + def 
                    + "' but should have a field definition ", this.getLocation());
        }
        FieldDefinition fieldDef = (FieldDefinition) def;
        this.fieldName.setDefinition(fieldDef);
        return fieldDef;
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(".");
        fieldName.decompile(s);
    }
    
    @Override
    protected RegisterOffset getHeapAddress(DecacCompiler compiler, GPRegister reg) {
        // Computing the address of the object into reg
        object.codeGenExp(compiler, reg);
        if (!compiler.getCompilerOptions().getNoCheck()) {
            // Check if null (no need to use CMP because EQ flag is already set)
            CodeError.NULL_DEREFERENCING.codeGenBEQ(compiler);
        }
        // Returns the address of the field as a register offset
        return new RegisterOffset(fieldName.getFieldDefinition().getIndex(), reg);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        fieldName.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        fieldName.iter(f);
    }

}
