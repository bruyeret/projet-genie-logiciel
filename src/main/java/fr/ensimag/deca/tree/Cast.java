package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;
import fr.ensimag.ima.pseudocode.instructions.INT;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import fr.ensimag.ima.pseudocode.instructions.SUBSP;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.deca.codegen.InstanceOfFunction;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.EnvironmentExp;

import org.apache.commons.lang.Validate;
import java.io.PrintStream;

/**
 * Conversion of an int into a float. Used for implicit conversions.
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class Cast extends AbstractExpr {
    
    private AbstractIdentifier castType;
    private AbstractExpr operand;
    
    public AbstractIdentifier getCastType(){
        return castType;
    }
    
    public AbstractExpr getOperand(){
        return operand;
    }
    
    public Cast(AbstractIdentifier castType, AbstractExpr operand) {
    	super();
    	Validate.notNull(castType);
    	Validate.notNull(operand);
        this.castType = castType;
    	this.operand = operand;
    }

    @Override
    /*
    * Pass 3 of [SyntaxeContextuelle] rule (3.39)
    */
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = castType.verifyType(compiler);
        Type type2 = operand.verifyExpr(compiler, localEnv, currentClass);
        if (!type2.castCompatible(type)) {
            throw new ContextualError("Impossible de caster une expression de type " + type2 + " vers le type " + type, this.getLocation());
        }
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Computing the object address into reg
        operand.codeGenExp(compiler, reg);
        if (getType().isInt() && operand.getType().isFloat()) {
            compiler.addInstruction(new INT(operand.getDVal(), reg));
        }
        else if (getType().isFloat() && operand.getType().isInt()) {
            compiler.addInstruction(new FLOAT(operand.getDVal(), reg));
        }
        else if (getType().isClass()) {
            /**
             * Cast for classes
             * @see src/main/resources/cast.txt for more details
             */
            Label endCast = new Label("cast_end_" + compiler.getLabelID());
            // CMP #null, R
            compiler.addInstruction(new CMP(new NullOperand(), reg));
            // BEQ end_cast
            compiler.addInstruction(new BEQ(endCast));
            // Calling instanceof function so 2 parameters needed + 2 words for BSR
            compiler.setMaxParameters(4);
            // ADDSP 2
            compiler.addInstruction(new ADDSP(2));
            // STORE R, 0(SP)
            compiler.addInstruction(new STORE(reg, new RegisterOffset(0, Register.SP)));
            // LOAD @class.methodTable, R0
            compiler.addInstruction(new LEA(castType.getClassDefinition().getMethodTableAddress(), Register.R0));
            // STORE R0, -1(SP)
            compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(-1, Register.SP)));
            // BSR code.instanceof
            InstanceOfFunction.codeGenBSR(compiler);
            // SUBSP 2
            compiler.addInstruction(new SUBSP(2));
            if (!compiler.getCompilerOptions().getNoCheck()) {
                // CMP #0, R0
                compiler.addInstruction(new CMP(0, Register.R0));
                // BRA impossible_cast_error
                CodeError.IMPOSSIBLE_CAST.codeGenBEQ(compiler);
            }
            // end_cast:
            compiler.addLabel(endCast);
        }
        else if (getType().isInt() && operand.getType().isInt() ||
                getType().isFloat() && operand.getType().isFloat() ||
                getType().isBoolean() && operand.getType().isBoolean()) {
            // do nothing
        }
        else {
            throw new DecacInternalError("Wrong types for cast");
        }
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        castType.iter(f);
        operand.iter(f);
    } 
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        castType.prettyPrint(s, prefix, false);
        operand.prettyPrint(s, prefix, true);
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        castType.decompile(s);
        s.print(") (");
        operand.decompile(s);
        s.print(")");
    }

    @Override
    String prettyPrintNode() {
        return ("Cast");
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        if (castType != null) {
            s.print(prefix);
            s.print("type: ");
            s.print(castType);
            s.println();
        }
    }
}
