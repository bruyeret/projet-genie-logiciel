package fr.ensimag.deca.tree;

import java.io.PrintStream;

import org.apache.commons.lang.Validate;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

public class Len extends AbstractExpr {
    final private AbstractExpr expr;
    
    public Len(AbstractExpr expr) {
        Validate.notNull(expr);
        this.expr = expr;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        Type myType = expr.verifyExpr(compiler, localEnv, currentClass);
        if (!myType.isTab()){
            throw new ContextualError("Conflicting types : type is " + myType.getName().getName() 
                    + " but should be a table type", this.getLocation());
        }
        this.setType(compiler.getEnvType().getTypeInt());
        return compiler.getEnvType().getTypeInt();
    }

    @Override
    public void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Computing the address of the array
        expr.codeGenExp(compiler, reg);
        // Loading the length stored at offset 0
        compiler.addInstruction(new LOAD(new RegisterOffset(0, reg), reg));
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        s.print("len(");
        expr.decompile(s);
        s.print(')');
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expr.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        expr.iter(f);
    }

}
