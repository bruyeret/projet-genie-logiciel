package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class NotEquals extends AbstractOpExactCmp {

    public NotEquals(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected void codeGenOpCmp(DecacCompiler compiler, boolean branchTruthValue, Label branchLabel) {
        if (branchTruthValue) {
            compiler.addInstruction(new BNE(branchLabel));
        }
        else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }

    @Override
    protected String getOperatorName() {
        return "!=";
    }

}
