package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.PrintGen;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * String literal
 *
 * @author gl46
 * @date 01/01/2021
 */
public class StringLiteral extends AbstractStringLiteral {

    @Override
    public String getValue() {
        return value;
    }

    private String value;

    public StringLiteral(String value) {
        Validate.notNull(value);
        this.value = value;
    }

    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.46)
    */
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type type = compiler.getEnvType().getTypeString();
        this.setType(type);
        checkDecoration();
        return type;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // nothing to do
    }
    
    @Override
    protected void codeGenPrint(DecacCompiler compiler, boolean printHex) {
        PrintGen.printString(compiler, value);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print('\"');
        s.print(value.replace("\\", "\\\\").replace("\"", "\\\""));
        s.print('\"');
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }
    
    @Override
    String prettyPrintNode() {
        return "StringLiteral (" + value + ")";
    }

}
