package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGT;
import fr.ensimag.ima.pseudocode.instructions.BLE;

/**
 *
 * @author gl46
 * @date 01/01/2021
 */
public class Greater extends AbstractOpIneq {

    public Greater(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected void codeGenOpCmp(DecacCompiler compiler, boolean branchTruthValue, Label branchLabel) {
        if (branchTruthValue) {
            compiler.addInstruction(new BGT(branchLabel));
        }
        else {
            compiler.addInstruction(new BLE(branchLabel));
        }
    }

    @Override
    protected String getOperatorName() {
        return ">";
    }

}
