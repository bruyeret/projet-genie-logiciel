package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;

/**
 * List of declarations (e.g. int x; float y,z).
 * 
 * @author gl46
 * @date 01/01/2021
 */
public class ListDeclVar extends TreeList<AbstractDeclVar> {

    /**
     * Generates code to initialize variables and
     * set their operands for ode generation
     * @param compiler
     * @param isGlobal If the variable is global
     */
    protected void codeGenListDeclVar(DecacCompiler compiler, boolean isGlobal) {
        // Adding to the stack pointer so computations for initialization are done
        // outside of the global variable storage space
        if (size() > 0) {
            // Stack overflow check and stack pointer increment
            compiler.setVariables(size());
            int i = 1;
            Register stackBase = Register.LB; 
            if (isGlobal) {
                i = compiler.getGBOffset();
                stackBase = Register.GB;
            }
            for (AbstractDeclVar declVar: getList()) {
                // Setting up the operands
                declVar.setCodeGenOperand(new RegisterOffset(i, stackBase));
                i++;
                // Generating code for initialization
                declVar.codeGenDeclVar(compiler);
            }
            compiler.setGBOffset(i);
        }
        else {
            compiler.addComment("No variables to declare.");
        }
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
    	for (AbstractDeclVar declVar: getList()) {
    		declVar.decompile(s);
    	}
    }

    /**
     * Implements non-terminal "list_decl_var" of [SyntaxeContextuelle] in pass 3, rule (3.16)
     * @param compiler contains the "env_types" attribute
     * @param localEnv 
     *   its "parentEnvironment" corresponds to "env_exp_sup" attribute
     *   in precondition, its "current" dictionary corresponds to 
     *      the "env_exp" attribute
     *   in postcondition, its "current" dictionary corresponds to 
     *      the "env_exp_r" attribute
     * @param currentClass 
     *          corresponds to "class" attribute (null in the main bloc).
     */    
    EnvironmentExp verifyListDeclVariable(DecacCompiler compiler, EnvironmentExp superEnv,
            EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
//            EnvironmentExp currentEnv = new EnvironmentExp(localEnv);
            for (AbstractDeclVar declVar : this.getList()){
                declVar.verifyDeclVar(compiler, superEnv, localEnv, currentClass);
            }
            return localEnv;
    }


}
