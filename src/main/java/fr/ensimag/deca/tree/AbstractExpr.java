package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.PrintGen;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Expression, i.e. anything that has a value.
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractExpr extends AbstractInst {
    /**
     * @return true if the expression does not correspond to any concrete token
     * in the source code (and should be decompiled to the empty string).
     */
    boolean isImplicit() {
        return false;
    }

    /**
     * Get the type decoration associated to this expression (i.e. the type computed by contextual verification).
     */
    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        Validate.notNull(type);
        this.type = type;
    }
    private Type type;
    
    public AbstractExpr getExpression() {
        return this;
    }

    @Override
    protected void checkDecoration() {
        if (getType() == null) {
            throw new DecacInternalError("Expression " + decompile() + " has no Type decoration");
        }
    }
    

    /**
     * Verify the expression for contextual error.
     * 
     * implements non-terminals "expr" and "lvalue" 
     *    of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  (contains the "env_types" attribute)
     * @param localEnv
     *            Environment in which the expression should be checked
     *            (corresponds to the "env_exp" attribute)
     * @param currentClass
     *            Definition of the class containing the expression
     *            (corresponds to the "class" attribute)
     *             is null in the main bloc.
     * @return the Type of the expression
     *            (corresponds to the "type" attribute)
     */
    public abstract Type verifyExpr(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Verify the expression in right hand-side of (implicit) assignments 
     * 
     * implements non-terminal "rvalue" of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  contains the "env_types" attribute
     * @param localEnv corresponds to the "env_exp" attribute
     * @param currentClass corresponds to the "class" attribute
     * @param expectedType corresponds to the "type1" attribute            
     * @return this with an additional ConvFloat if needed...
     */

    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.28)
     */
    public AbstractExpr verifyRValue(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type expectedType) throws ContextualError {
        Type type = this.verifyExpr(compiler, localEnv, currentClass);
        this.setType(type);
        if (expectedType.assignCompatible(type)) {
            return this;
        } else {
            throw new ContextualError("Operation impossible : a " + expectedType + " type was expected but and a " 
                    + type + " type was encountered", this.getLocation());
        }
    }
    
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.20)
     */
    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        verifyExpr(compiler, localEnv, currentClass);
    }

    /**
     * Verify the expression as a condition, i.e. check that the type is
     * boolean.
     * Pass 3 of [SyntaxeContextuelle], rule (3.29)
     * @param localEnv
     *            Environment in which the condition should be checked.
     * @param currentClass
     *            Definition of the class containing the expression, or null in
     *            the main program.
     */
    void verifyCondition(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        Type myType = this.verifyExpr(compiler, localEnv, currentClass);
        if (!(myType.isBoolean())) {
            throw new ContextualError("Conflicting types : the type of a condition should be boolean but is " + myType, this.getLocation());
        }        
    }

    /**
     * Generate code to print the expression
     * @param compiler
     */
    protected void codeGenPrint(DecacCompiler compiler, boolean printHex) {
        // Computing the result into R2
        codeGenExp(compiler, Register.getR(2));
        PrintGen.printReg(compiler, Register.getR(2), type, printHex);
    }
    
    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        // By default, compute into R2
        compiler.setMaxRegisters(1);
        codeGenExp(compiler, Register.getR(2));
    }
    
    /**
     * Evaluates the AbstractExp recursively
     * and stores the result into a register
     * @param compiler
     * @param reg
     */
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // type should exist if checkDecoration is called beforehand
        if (type.isBoolean()) {
            int labelID = compiler.getLabelID();
            Label trueLabel = new Label("true_bool_exp_" + labelID);
            Label endLabel = new Label("end_bool_exp_" + labelID);
            // <Code(C, vrai, E_vrai)
            codeGenExpBool(compiler, reg, true, trueLabel);
            compiler.addInstruction(new LOAD(0, reg));
            // BRA E_fin
            compiler.addInstruction(new BRA(endLabel));
            // E_vrai :
            compiler.addLabel(trueLabel);
            compiler.addInstruction(new LOAD(1, reg));
            // E_fin :
            compiler.addLabel(endLabel);
        }
        else {
            throw new DecacInternalError("AbstractExpr.codeGenExp was called with an expression whose type is not boolean but is " + getType());
        }
    }
    
    /**
     * <Code(C, b, E)> function specified at page 220
     * Generates instructions for boolean expressions
     * @param compiler
     * @param reg Register where to do the computation
     * @param branchTruthValue
     * @param branchLabel
     */
    protected void codeGenExpBool(DecacCompiler compiler, GPRegister reg,
            boolean branchTruthValue, Label branchLabel) {
        if (!type.isBoolean()) {
            throw new DecacInternalError("AbstractExpr.codeGenExpBool was called with an expression whose type is not boolean.");
        }
        // Computing the expression into R2 by default
        compiler.setMaxParameters(1);
        codeGenExp(compiler, reg);
        compiler.addInstruction(new CMP(0, Register.getR(2)));
        if (branchTruthValue) {
            compiler.addInstruction(new BNE(branchLabel));
        }
        else {
            compiler.addInstruction(new BEQ(branchLabel));
        }
    }
    
    /**
     * @return DVal for code generation
     * returns null by default, to be changed
     * in subclasses that should return a DVal
     */
    protected DVal getDVal() {
        return null;
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Type t = getType();
        if (t != null) {
            s.print(prefix);
            s.print("type: ");
            s.print(t);
            s.println();
        }
    }
}
