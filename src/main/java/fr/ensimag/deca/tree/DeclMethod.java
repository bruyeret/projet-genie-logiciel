package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;


public class DeclMethod extends AbstractDeclMethod {
    final private AbstractIdentifier returnType;
    final private AbstractIdentifier methodName;
    final private ListDeclParam params;
    final private AbstractMethodBody methodBody;
        
    public DeclMethod(AbstractIdentifier returnType, AbstractIdentifier methodName,
            ListDeclParam params, AbstractMethodBody methodBody) {
        Validate.notNull(returnType);
        Validate.notNull(methodName);
        Validate.notNull(params);
        Validate.notNull(methodBody);
        this.returnType = returnType;
        this.methodName = methodName;
        this.params = params;
        this.methodBody = methodBody;
    }
    
    @Override
    public AbstractIdentifier getMethodName() {
        return this.methodName;
    }
    
    @Override
    public AbstractMethodBody getMethodBody() {
        return this.methodBody;
    }
    
    @Override
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.7)
     */
    protected AbstractIdentifier verifyMethodMembers(DecacCompiler compiler, ClassDefinition superClass, ClassDefinition currentClass)
            throws ContextualError {
        // verification of the parameters and the return type of the method
        Type type = this.returnType.verifyType(compiler);
        Signature sig = this.params.verifyListParam(compiler);
        // condition to factorize the code
        boolean conditionSi1 = superClass.isClass();
        boolean conditionSi2 = !(superClass.getMembers().get(methodName.getName()) == null); 
        boolean conditionSi = conditionSi1 && conditionSi2;
        boolean conditionAlors1;
        int index = 0;
        // check if the method is alrady declared in the mother class. If yes, conditionAlors1 will be true
        if (superClass.getMembers().get(methodName.getName()) != null){
            conditionAlors1 = superClass.getMembers().get(methodName.getName()).isMethod();
            if (conditionAlors1){
                ExpDefinition def = superClass.getMembers().get(methodName.getName());
                MethodDefinition defMethod = (MethodDefinition) def;
                index = defMethod.getIndex();
            } else {
                currentClass.incNumberOfMethods();
                index = currentClass.getNumberOfMethods() - 1;
            }
        } else {
            conditionAlors1 = false;
            currentClass.incNumberOfMethods();
            index = currentClass.getNumberOfMethods() - 1;
        }
        // if it is a redefinition of the method : check if the types are compatibles for it
        if (conditionSi) {
            if (conditionAlors1) {
                MethodDefinition defMethod = (MethodDefinition) superClass.getMembers().get(methodName.getName());
                boolean conditionAlors2 = defMethod.getSignature().equals(sig);
                boolean conditionAlors3 = type.isSubtype(defMethod.getType());
                if (!(conditionAlors2 && conditionAlors3)){
                    throw new ContextualError("Conflicting types : the encountered types does not match the types"
                            + " of the previous declaration of the method " 
                            + this.methodName.getName().getName(), this.getLocation());
                }
            }     
        }
        // setting properties to the method declared
        MethodDefinition methodDef = new MethodDefinition(type, this.getLocation(), sig, index);
        this.methodName.setDefinition(methodDef);
        this.methodName.setType(type);
        return methodName; 
    }
    
    @Override
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.11)
     */
    protected void verifyMethodBodyParam(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        // Verfication of the method body 
        Type type = this.returnType.verifyType(compiler);
        EnvironmentExp paramEnv = this.params.verifyListParamBody(compiler);
//            // DEBUG BEGIN
//            System.out.println("Environment des paramètres de la méthode : " + this.methodName.getName().getName());
//            paramEnv.helpMePrint();
//            // DEBUG END
        this.methodBody.verifyMethodBody(compiler, localEnv, paramEnv, currentClass, type);
    }
       
       
    @Override
    public void decompile(IndentPrintStream s) {
        returnType.decompile(s);
        s.print(" ");
        methodName.decompile(s);
        s.print("(");
        params.decompile(s);
        s.print(")");
        methodBody.decompile(s);
    }
    
    @Override
    protected void setCodeGenOperands() {
        int i = -3;
        for (AbstractDeclParam param: params.getList()) {
            // Setting up operands for parameters
            // first is -3(LB) then -4(LB) and so on
            param.setCodeGenOperand(new RegisterOffset(i, Register.LB));
            i--;
        }
    }
    
    @Override
    protected void codeGenMethod(DecacCompiler compiler) {
        compiler.setMethodDef(methodName.getMethodDefinition());
        methodBody.codeGenMethodBody(compiler);
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        returnType.prettyPrint(s, prefix, false);
        methodName.prettyPrint(s, prefix, false);
        params.prettyPrint(s, prefix, false);
        methodBody.prettyPrint(s, prefix, true);
    }
    
    @Override
    protected void iterChildren(TreeFunction f) {
        returnType.iter(f);
        methodName.iter(f);
        params.iter(f);
        methodBody.iter(f);
    }
}