package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.EnvironmentExp.DoubleDefException;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;

public class ListDeclParam extends TreeList<AbstractDeclParam> {
    
    /**
     * Pass 2 of [SyntaxeContextuelle] rule (2.8)
     */
    public Signature verifyListParam(DecacCompiler compiler) throws ContextualError {
        Signature sig = new Signature();
        for (AbstractDeclParam param : this.getList()) {
            Type myType = param.verifyParam(compiler);
            sig.add(myType);
        }
        return sig;
    }
    
    /**
     * Pass 3 of [SyntaxeContextuelle] rule (3.12)
     */
    public EnvironmentExp verifyListParamBody(DecacCompiler compiler) throws ContextualError {
        EnvironmentExp localEnv = new EnvironmentExp(null); 
        for (AbstractDeclParam declParam : this.getList()){
            Symbol symbol = declParam.verifyParamBody(compiler);
            ExpDefinition def = new ParamDefinition(declParam.getParamName().getType(), declParam.getLocation());
            declParam.getParamName().setDefinition(def);
            try {
                localEnv.declare(symbol, def);
            } catch(DoubleDefException ex) {
                 throw new ContextualError("Parameter " + symbol.getName() + " has already been declared", declParam.getLocation());
            }
        }
        return localEnv; 
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        boolean comma = false;
        for (AbstractDeclParam declParam: getList()) {
            if (comma) {
                s.print(", ");
            }
            comma = true;
            declParam.decompile(s);
        }
    }
}