package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.CodeError;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

/**
 * read...() statement.
 *
 * @author gl46
 * @date 01/01/2021
 */
public abstract class AbstractReadExpr extends AbstractExpr {

    public AbstractReadExpr() {
        super();
    }

    @Override
    protected void codeGenExp(DecacCompiler compiler, GPRegister reg) {
        // Asks for input and stores into R1
        codeGenRead(compiler);
        // If input error
        CodeError.INPUT_OUTPUT.codeGenBOV(compiler);
        // Copy R1 to reg
        compiler.addInstruction(new LOAD(Register.R1, reg));
    }
    
    /**
     * Generates code that ask for input and
     * stores the result into R1
     * @param compiler
     */
    protected abstract void codeGenRead(DecacCompiler compiler);
}
