package fr.ensimag.deca;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * User-specified options influencing the compilation.
 *
 * @author gl46
 * @date 01/01/2021
 */
public class CompilerOptions {
	public static final int COMPILE = 0;
	public static final int PARSE = 1;
	public static final int VERIFY = 2;
	
    public static final int QUIET = 0;
    public static final int INFO  = 1;
    public static final int DEBUG = 2;
    public static final int TRACE = 3;
    
    public boolean getNoCheck() {
    	return noCheck;
    }
    
    public int getSteps() {
    	return steps;
    }
    
    public int getDebug() {
        return debug;
    }

    public boolean getParallel() {
        return parallel;
    }

    public boolean getPrintBanner() {
        return printBanner;
    }
    
    public boolean getPrintHelp() {
        return printHelp;
    }
    
    public int getRmax() {
        return rmax;
    }
    
    public List<File> getSourceFiles() {
        return Collections.unmodifiableList(sourceFiles);
    }

    private int debug = 0;
    private int steps = COMPILE;
    private boolean parallel = false;
    private boolean printBanner = false;
    private boolean printHelp = false;
    private boolean noCheck = false;
    private int rmax = 15;
    private List<File> sourceFiles = new ArrayList<File>();

    
    public void parseArgs(String[] args) throws CLIException {
    	// Parsing special options
    	if (args.length == 1 && args[0].equals("-b")) {
    		printBanner = true;
    	}
    	else if (args.length == 1 && (args[0].equals("-h") || args[0].equals("--help"))) {
    		printHelp = true;
    	}
    	// Regular case
    	else {
    		boolean parsingOptions = true;
    		boolean rmaxSpecified = false; 
    		int i = 0;
    		// Parsing options
    		while (parsingOptions && i < args.length) {
    			switch (args[i]) {
    			case "-b":
    			case "-h":
    			case "--help":
					throw new CLIException(args[i] + " option can only be used alone.");
					
    			case "-P":
    				if (parallel) {
    					throw new CLIException("-P option was specified twice.");
    				}
    				parallel = true;
    				break;
    				
    			case "-n":
    				if (noCheck) {
    					throw new CLIException("-n option was specified twice.");
    				}
    				noCheck = true;
    				break;
    				
    			case "-r":
    				if (rmaxSpecified) {
    					throw new CLIException("-r option was specified twice.");
    				}
    				rmaxSpecified = true;
    				i++;
    				if (i >= args.length) {
    					throw new CLIException("-r option should be followed by a number.");
    				}
    				try {
    					rmax = Integer.parseInt(args[i]) - 1;
    					if (rmax < 3 || rmax > 15) {
        					throw new CLIException("-r option should be followed by a number "
        					        + "X that satisfies 4 <= X <= 16.");
    					}
    				}
    				catch (NumberFormatException e) {
    					throw new CLIException("-r option should be followed by a number, not \""
    					        + args[i] + "\".");
    				}
    				break;
    				
    			case "-d":
    				if (debug >= 3) {
    					throw new CLIException("Debug level cannot be greater than 3.");
    				}
    				debug++;
    				break;
    				
    			case "-p":
    				if (steps != COMPILE) {
    					throw new CLIException("-p option can only be called once and is incompatible with option -v.");
    				}
    				steps = PARSE;
    				break;
    				
    			case "-v":
    				if (steps != COMPILE) {
    					throw new CLIException("-v option can only be called once and is incompatible with option -p.");
    				}
    				steps = VERIFY;
    				break;
    				
    			default:
    				if (args[i].startsWith("-")) {
    					throw new CLIException("Invalid option : " + args[i]);
    				}
    				i--;
    				parsingOptions = false;
    			}
    			i++;
    		}
    		// Parsing source files
    		while (i < args.length) {
    		    if (!args[i].endsWith(".deca")) {
    		        throw new CLIException("Filename doesn't end with \".deca\".");
    		    }
    		    File file = new File(args[i]);
    		    if (!sourceFiles.contains(file)) {
                    sourceFiles.add(new File(args[i]));
    		    }
                i++;
    		}
    	}
        Logger logger = Logger.getRootLogger();
        // map command-line debug option to log4j's level.
        switch (getDebug()) {
        case QUIET: break; // keep default
        case INFO:
            logger.setLevel(Level.INFO); break;
        case DEBUG:
            logger.setLevel(Level.DEBUG); break;
        case TRACE:
            logger.setLevel(Level.TRACE); break;
        default:
            logger.setLevel(Level.ALL); break;
        }
        logger.info("Application-wide trace level set to " + logger.getLevel());

        boolean assertsEnabled = false;
        assert assertsEnabled = true; // Intentional side effect!!!
        if (assertsEnabled) {
            logger.info("Java assertions enabled");
        } else {
            logger.info("Java assertions disabled");
        }
    }

    protected void displayUsage() {
    	System.out.println("Usage: \"decac [options] <file.deca>\"");
    	System.out.println("Use \"decac --help\" for more details");
    }
    
    protected void displayBanner() {
    	System.out.println("Projet GL 2021: Equipe 46 - Groupe 9");
    	System.out.println("- BRUYERE Thibault");
    	System.out.println("- COUSIN Marie");
    	System.out.println("- DELPUECH Julien");
    	System.out.println("- HOOGVELD Karmijn");
    	System.out.println("- MARION Mathis");
    }
    
    protected void displayHelp() {
    	System.out.println("USAGE:");
    	System.out.println("decac [[-p | -v] [-n] [-r X] [-d]* [-P] <file.deca>...] | [-b]");
    	System.out.println();
    	System.out.println("OPTIONS:");
    	System.out.println("    -b (banner)");
    	System.out.println("displays a banner containing the team name");
    	System.out.println();
    	System.out.println("    -p (parse)");
    	System.out.println("stops decac after the tree construction step,");
    	System.out.println("and displays the decompilation of the tree");
    	System.out.println("(if there is a single source file to compile,");
    	System.out.println("the output should be a syntaxically correct");
    	System.out.println("deca program)");
    	System.out.println();
    	System.out.println("    -v (verification)");
    	System.out.println("stops decac after the verifications step");
    	System.out.println("(no output is given if no error is encountered)");
    	System.out.println();
    	System.out.println("    -n (no check)");
    	System.out.println("deletes the overflow tests");
    	System.out.println("* arithmetic overflow");
    	System.out.println("* memory overflow");
    	System.out.println("* null dereferencing");
    	System.out.println();
    	System.out.println("    -r X (registers)");
    	System.out.println("restricts the general purpose registers to");
    	System.out.println("R0 ... R{X-1}, with 4 <= X <= 16");
    	System.out.println();
    	System.out.println("    -d (debug)");
    	System.out.println("enables the debug traces.");
    	System.out.println("Repeat the option for more traces.");
    	System.out.println("0: quiet");
    	System.out.println("1: info");
    	System.out.println("2: debug");
    	System.out.println("3: trace");
    	System.out.println();
    	System.out.println("    -P (parallel)");
    	System.out.println("if there are several source files,");
    	System.out.println("compiles the files in parallel");
    	System.out.println("(to speed up the compilation)");
    	System.out.println();
    	System.out.println("    -h --help");
    	System.out.println("shows this page");
    }
}
