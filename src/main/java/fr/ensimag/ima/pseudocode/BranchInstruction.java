package fr.ensimag.ima.pseudocode;

/**
 *
 * @author Ensimag
 * @date 01/01/2021
 */
public class BranchInstruction extends UnaryInstruction {

    public BranchInstruction(Label op) {
        super(op);
    }
    
    public BranchInstruction(String op) {
        super(new Label(op));
    }

}
