package fr.ensimag.ima.pseudocode;

/**
 * Operand representing a register indirection with an offset and an index
 * e.g. d(XX, Rm)
 *
 */
public class RegisterOffsetIndex extends DAddr {
    private final int offset;
    private final Register register;
    private final GPRegister index;

    public RegisterOffsetIndex(int offset, Register register, GPRegister index) {
        super();
        this.offset = offset;
        this.register = register;
        this.index = index;
    }
    
    @Override
    public String toString() {
        return offset + "(" + register + ", " + index + ")";
    }
}
