#!/bin/bash

# Script interactif pour définir les sorties attendues après avoir lancé regex_regression.sh
# Il récupère les fichiers .new du dossier src/test/deca et créé en conséquence
# un fichier .xpc (pour expected) servant à l'oracle

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TEMP_DIR="$DIR/oracle-temp"
DECA_TEST_DIR="$DIR/../deca"

mkdir -p $TEMP_DIR

# On s'assure que tous les fichiers aient été créés
for FILE in $@
do
    FILE_ROOT=${FILE:0: -5}
    XPC_FILE="$FILE_ROOT.xpc"
    if [ -f "$XPC_FILE" ]
    then
        echo "Le fichier $XPC_FILE existe déjà, voulez-vous ajouter des sections à la suite ?"
        echo "(O/Y : ajouter des sections à la suite, n : modifier le fichier xpc)"
        read IN_MODIF
        while [ -n "$IN_MODIF" ] && [ "${IN_MODIF:0:1}" != "O" ] && [ "${IN_MODIF:0:1}" != "Y" ] && [ "${IN_MODIF:0:1}" != "n" ]
        do
            read IN_MODIF
        done
        if [ -n "$IN_MODIF" ] && [ "${IN_MODIF:0:1}" = "n" ]
        then
            atom --wait $XPC_FILE
        fi
    else
        echo "Le fichier $XPC_FILE n'existe pas"
        echo "Création du fichier"
        >$XPC_FILE;
    fi
    echo ""
done

# On demande à ajouter des sections
IN_MODIF="O"
while [ -z "$IN_MODIF" ] || [ "$IN_MODIF" != "n" ]
do
    echo "Voulez-vous voulez ajouter une section automatiquement (un executable à tester) à l'oracle ?"
    echo "(O/Y : ajouter une section, n : ne pas en ajouter)"
    read IN_MODIF
    while [ -n "$IN_MODIF" ] && [ "${IN_MODIF:0:1}" != "O" ] && [ "${IN_MODIF:0:1}" != "Y" ] && [ "${IN_MODIF:0:1}" != "n" ]
    do
        read IN_MODIF
    done
    if [ -z "$IN_MODIF" ] || [ "$IN_MODIF" != "n" ]
    then
        echo "Entrez le nom de l'executable (par exemple test_context s'il est dans le PATH, decac ou ima) :"
        read SECTION_NAME
        echo "Entrez l'extension du fichier à tester (par exemple : deca, ass) :"
        read EXTENSION
        # On ajoute la section à tous les fichiers
        for FILE in $@
        do
            FILE_ROOT=${FILE:0: -5}
            XPC_FILE="$FILE_ROOT.xpc"
            $SECTION_NAME "$FILE_ROOT.$EXTENSION" > $TEMP_DIR/std_out 2> $TEMP_DIR/std_err
            printf $? > $TEMP_DIR/exit_code
            python3 "$DIR/oracle-section.py" $XPC_FILE $TEMP_DIR $SECTION_NAME $EXTENSION
            echo "Fait : $XPC_FILE"
        done
        echo ""
    fi
done

# On modifie tous les fichiers créé (pas facultatif, sinon il risque d'y avoir des erreurs)
for FILE in $@
do
    FILE_ROOT=${FILE:0: -5}
    XPC_FILE="$FILE_ROOT.xpc"
    atom --wait $XPC_FILE
done

echo "Tous les fichiers ont été traités"

rm -rf $TEMP_DIR
