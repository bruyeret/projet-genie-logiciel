#!/usr/bin/env python3

import sys
import re
import subprocess as sbp
import string


if sys.version_info[0] < 3 or sys.version_info[1] < 7:
    print("Mettez à jour python vers une version plus récente (au moins 3.7)")
    exit(1)

if (len(sys.argv) != 2):
    print("Erreur, un unique argument attendu par oracle.py, reçu : ", len(sys.argv)-1)
    exit(1)


# Lis des espaces, une string puis ":", puis une string renvoyée qui s'arrête
# après "\n" (exclu) ou "|x" si la première string commence par "std_"
# Renvoie un tuple (string de début, string de fin)
# Si aucun ":" n'est trouvé, renvoie le tuple vide
def mot_suivant(fichier):
    # On lis tous les espaces
    whitespc = set(string.whitespace)
    carac = " "
    while carac in whitespc:
        carac = fichier.read(1)
        if carac == "":
            return ()
    # On lit la première string
    prem = []
    while carac != ":":
        prem.append(carac)
        carac = fichier.read(1)
        if (carac == ""):
            return ()
    # On est au niveau de ":" on décide de quel caractère on cherche en fonction de prem
    prem = "".join(prem)
    end = "\n"
    if prem[:4] == "std_":
        end = "|x"
    # On lit la deuxième string
    # La détection de la fin de fichier ne marche que lorsque le end n'est pas dans un début de end
    # exemple : end = "abaab" et le fichier se termine par "ababaab" mais la fin se sera pas reconnue
    # Il n'y a pas de problème si end ne se termine pas par une séquence par
    # lequel il commence (dans l'exemple c'est "ab")
    deux = []
    fin_ok = 0
    while fin_ok < len(end):
        carac = fichier.read(1)
        if (carac == ""):
            print("Un caractère de fin attendu n'a pas été trouvé (EOF trop tôt)")
            exit(1)
        deux.append(carac)
        if end[fin_ok] == carac:
            fin_ok += 1
        else:
            fin_ok = 1 if end[0] == carac else 0
    deux = "".join(deux[:-len(end)])
    # On enlève l'échappement des "|"
    if end == "|x":
        deux.replace("||", "|")
    return (prem, deux)


# Lis tout une section et renvoie false s'il n'y a pas de section
def lis_section(fichier, racine):
    # On lis le nom de la section (l'executable)
    duet = mot_suivant(fichier)
    if (duet == ()):
        # Pas de section à lire
        return False
    if (duet[0] != "section"):
        print("oracle.py : 'section' attendu, reçu '" + duet[0] + "'")
        exit(1)
    executable = duet[1]

    # On lis l'extension (par exemple deca ou ass)
    duet = mot_suivant(fichier)
    if (duet == () or duet[0] != "extension"):
        print("oracle.py : 'extension' attendu, reçu '" + (duet[0] if (duet != ()) else "") + "'")
        exit(1)
    extension = duet[1]

    # On lis les trois possibilités : code, std_out et std_err
    tests = {"code": None, "std_out": None, "std_err": None, "std_in": None}
    while (True):
        duet = mot_suivant(fichier)
        if (duet == () or duet[0] == "end"):
            break
        if (duet[0] not in tests):
            print("Les tests valides sont 'code', 'std_out', 'std_in' et 'std_err', mais pas '" + duet[0] + "'")
            exit(1)
        tests[duet[0]] = duet[1]

    # On a tout ce qu'il faut pour lancer le test
    result = sbp.run([executable, racine + "." + extension], capture_output=True, text=True, input=tests["std_in"])

    # On fait donc passer les tests
    if tests["code"] is not None and result.returncode != int(tests["code"]):
        print("Code de sortie de '" + " ".join(result.args) + "' attendu : " + tests["code"] + ", reçu : " + str(result.returncode))
        exit(1)
    if tests["std_out"] is not None and re.search(tests["std_out"], result.stdout) is None:
        print("Sortie standard de '" + " ".join(result.args) + "' inattendue")
        exit(1)
    if tests["std_err"] is not None and re.search(tests["std_err"], result.stderr) is None:
        print("Sortie d'erreur de '" + " ".join(result.args) + "' inattendu")
        exit(1)
    print("Succès de la commande de test :", " ".join(result.args))

    # Si la commande était une section decac, sur un fichier deca, on lance un test d'indempotance
    if (executable == "decac" and extension == "deca" and tests["code"] == "0"):
        print("Test d'indempotance : ", end="")
        if (sbp.run(["indempotance.sh", racine + ".deca"]).returncode == 0):
            print("succès")
        else:
            exit(1)

    # La section a été lue, on retourne vrai
    return True


# Programme principal
xpc_file = open(sys.argv[1], 'rt')
racine = sys.argv[1][0:-4]


# On lis chaque section une par une
while (lis_section(xpc_file, racine)):
    pass
xpc_file.close()
