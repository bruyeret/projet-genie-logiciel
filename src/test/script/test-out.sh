#!/bin/bash

# Test de toutes les sorties des fichiers .deca créés

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../.." >/dev/null 2>&1 && pwd )"
cd "$ROOT_DIR" || exit 1
PATH=./src/test/script/launchers:"$PATH"

DECA_TEST_DIR="$ROOT_DIR/src/test/deca"

echo Tests de syntaxe valides
for FILE in $(find "$DECA_TEST_DIR/syntax/valid" -name "*.deca")
do
    if test_synt "$FILE" 2>&1 \
        | head -n 1 | grep -q "${FILE##*/}:[0-9]"
    then
        echo "Echec inattendu de test_synt sur $FILE"
        exit 1
    fi
done
echo OK

echo Tests de syntaxe invalides
for FILE in $(find "$DECA_TEST_DIR/syntax/invalid" -name "*.deca")
do
    if ! (test_synt "$FILE" 2>&1 \
        | head -n 1 | grep -q "${FILE##*/}:[0-9]")
    then
        echo "Réussite inattendu de test_synt sur $FILE"
        exit 1
    fi
done
echo OK


echo Tests de contexte valides
for FILE in $(find "$DECA_TEST_DIR/context/valid" -name "*.deca")
do
    if test_context "$FILE" 2>&1 \
        | head -n 4 | grep -q "${FILE##*/}:[0-9]"
    then
        echo "Echec inattendu de test_context sur $FILE"
        exit 1
    fi
done
echo OK

echo Tests de contexte invalides
for FILE in $(find "$DECA_TEST_DIR/context/invalid" -name "*.deca")
do
    if ! (test_context "$FILE" 2>&1 \
        | head -n 4 | grep -q "${FILE##*/}:[0-9]")
    then
        echo "Réussite inattendu de test_context sur $FILE"
        exit 1
    fi
done
echo OK


echo Tests des programmes entièrement valides
for FILE in $(ls $DECA_TEST_DIR/codegen/valid/*.deca)
do
    if ! (decac "$FILE") > /dev/null 2>&1
    then
        echo "Échec inattendu de decac sur $FILE"
        exit 1
    fi
    if ! (ima "${FILE:0:-5}.ass") > /dev/null 2>&1
    then
        echo "Échec inattendu de ima sur $FILE"
        exit 1
    fi
done
echo OK

echo Tests des programmes dont la génération de code est invalide
for FILE in $(find $DECA_TEST_DIR/codegen/invalid -name "*.deca")
do
    if ! (decac "$FILE") > /dev/null 2>&1
    then
        echo "Échec inattendu de decac sur $FILE"
        exit 1
    fi
    if ima "${FILE:0:-5}.ass" > /dev/null 2>&1
    then
        echo "Réussite inattendu de ima sur $FILE"
        exit 1
    fi
done
echo OK
