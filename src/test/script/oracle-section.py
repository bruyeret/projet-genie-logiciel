#!/usr/bin/env python3

import sys
import re


if sys.version_info[0] < 3 or sys.version_info[1] < 7:
    print("Mettez à jour python vers une version plus récente (au moins 3.7)")
    exit(1)

if (len(sys.argv) != 5):
    print("Erreur : 4 arguments demandés par oracle_section.py, reçu ", len(sys.argv) - 1)


# Détecte lorsque la chaine contient une erreur ou du debug et renvoie
# [nom du fichier]:xx:xx: en cas d'erreur
# supprime les ligne DEBUG s'il y en a
def std_make_regex(chaine):
    result = re.search(r"[\w\.\+\-]*:[1-9]\d*:[1-9]\d*:", chaine)
    if result:
        return make_regex(result.group(0), exact_match=False)
    debug = False
    while (result := re.search(r"DEBUG.*\n", chaine)):
        debug = True
        chaine = chaine[:result.start(0)] + chaine[result.end(0):]
    if debug:
        make_regex(chaine, exact_match=False)
    return make_regex(chaine)


def make_regex(chaine, exact_match=True):
    chaine = re.escape(chaine)
    chaine.replace("|", "||")
    if exact_match:
        return "^" + chaine + "$|x"
    else:
        return chaine + "|x"


# Liste des arguments reçu de la ligne de commande
xpc_path = sys.argv[1]
temp_dir = sys.argv[2]
section = sys.argv[3]
extension = sys.argv[4]


# On va écrire dans ce fichier
xpc_file = open(xpc_path, 'a')


# On écrit en premier le nom de la section
xpc_file.write("section:")
xpc_file.write(section)
xpc_file.write("\n")


# On écrit l'extension du fichier attendu
xpc_file.write("extension:")
xpc_file.write(extension)
xpc_file.write("\n")


# On écrit le code de sortie attendu
exit_code_filename = temp_dir + "/exit_code"
exit_code_file = open(exit_code_filename, 'r')

xpc_file.write("code:")
xpc_file.write(exit_code_file.read())
xpc_file.write("\n")

exit_code_file.close()

# On écrit la sortie standard attendue si l'executable n'est pas un test_xxx
if (section[0:5] != "test_"):
    std_out_filename = temp_dir + "/std_out"
    std_out_file = open(std_out_filename, 'r')

    xpc_file.write("std_out:")
    xpc_file.write(std_make_regex(std_out_file.read()))
    xpc_file.write("\n")

    std_out_file.close()


# On écrit la sortie d'erreur attendue
std_err_filename = temp_dir + "/std_err"
std_err_file = open(std_err_filename, 'r')

xpc_file.write("std_err:")
xpc_file.write(std_make_regex(std_err_file.read()))
xpc_file.write("\n")

std_err_file.close()


# On écrit la fin de la section
xpc_file.write("end:\n\n")


# On ferme le fichier dans lequel on a écrit
xpc_file.close()
