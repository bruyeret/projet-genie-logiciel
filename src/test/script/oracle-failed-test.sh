#!/bin/bash

# Effectue les tests d'oracle sur tous les fichiers .xpc du dossier deca

TEST_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
PATH="$TEST_PATH/script/launchers:$PATH"

for FILE in $(find "$TEST_PATH/deca" -name "*.xpc")
do
    oracle.py $FILE > /dev/null 2>&1 || { echo $FILE; }
done
