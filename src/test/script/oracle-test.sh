#!/bin/bash

# Effectue les tests d'oracle sur tous les fichiers .xpc du dossier deca

TEST_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
PATH="$TEST_PATH/script:$TEST_PATH/script/launchers:$PATH"

for FILE in $(find "$TEST_PATH/deca" -name "*.xpc")
do
    if [ -f $FILE ] && [ ${FILE: -4} = ".xpc" ]
    then
        echo "Test de $FILE"
        if oracle.py $FILE
        then
            echo "Test terminé et accepté"
        else
            echo "Des erreurs ont eu lieu"
            exit 1
        fi
        echo ""
    else
        echo "$FILE n'est pas un fichier .xpc valide, fichier ignoré"
    fi
done
