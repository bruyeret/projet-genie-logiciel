#!/usr/bin/env bash

# Recoit en argument un fichier deca et effectue un test d'indempotance dessus

if [ $# -ne 1 ] || [ ! -f $1 ] || [ ${1: -5} != ".deca" ]
then
    echo "Un unique fichier .deca attendu en argument"
    exit 1
fi

RACINE="${1:0: -5}"
COMP="$1"
DECOMP="${RACINE}_decomp.deca"
DEDECOMP="${RACINE}_dedecomp.deca"

decac -p "$COMP" > "$DECOMP"
if [ $? -ne 0 ]
then
    rm "$DECOMP"
    echo "Echec de la première décompilation"
    exit 1
fi

decac -p "$DECOMP" > "$DEDECOMP"
if [ $? -ne 0 ]
then
    rm "$DECOMP"
    rm "$DEDECOMP"
    echo "Echec de la seconde décompilation"
    exit 1
fi

diff $DECOMP $DEDECOMP
OUT=$?
rm "$DECOMP"
rm "$DEDECOMP"

if [ $OUT -ne 0 ]
then
    echo "Des différences existent entre les deux décompilation"
fi
