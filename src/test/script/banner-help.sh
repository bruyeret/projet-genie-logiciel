#!/usr/bin/env bash

decac -b > /dev/null 2>&1 || { echo "Erreur de banner" ; exit 1 ; }

decac -h > /dev/null 2>&1 || { echo "Erreur de help" ; exit 1 ; }
