#!/bin/bash

# Script interactif pour définir les sorties attendues après avoir lancé regex_regression.sh
# Il récupère les fichiers .new du dossier src/test/deca et créé en conséquence
# un fichier .xpc (pour expected) servant à l'oracle

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TEMP_DIR="$DIR/oracle-temp"
DECA_TEST_DIR="$DIR/../deca"

mkdir -p $TEMP_DIR

for FILE in $@
do
    FILE_ROOT=${FILE:0: -5}
    XPC_FILE="$FILE_ROOT.xpc"
    echo ""
    if [ -f "$XPC_FILE" ]
    then
        echo "Le fichier $XPC_FILE existe déjà, voulez-vous le modifier ?"
        echo "Si vous le supprimez, il sera proposé de le créer au prochain appel à oracle.sh"
        echo "(O/Y : modifier, n : ne rien faire)"
        read IN_MODIF
        while [ -n "$IN_MODIF" ] && [ "${IN_MODIF:0:1}" != "O" ] && [ "${IN_MODIF:0:1}" != "Y" ] && [ "${IN_MODIF:0:1}" != "n" ]
        do
            read IN_MODIF
        done
        if [ -z "$IN_MODIF" ] || [ "${IN_MODIF:0:1}" != "n" ]
        then
            atom --wait $XPC_FILE
        fi
    else
        echo "Le fichier $XPC_FILE n'existe pas"
        echo "Création du fichier"

        >$XPC_FILE;
    fi
    IN_MODIF="O"
    while [ -z "$IN_MODIF" ] || [ "$IN_MODIF" != "n" ]
    do
        echo "Voulez-vous voulez ajouter une section automatiquement (un executable à tester) à l'oracle ?"
        echo "(O/Y : ajouter une section, n : ne pas en ajouter)"
        read IN_MODIF
        while [ -n "$IN_MODIF" ] && [ "${IN_MODIF:0:1}" != "O" ] && [ "${IN_MODIF:0:1}" != "Y" ] && [ "${IN_MODIF:0:1}" != "n" ]
        do
            read IN_MODIF
        done
        if [ -z "$IN_MODIF" ] || [ "$IN_MODIF" != "n" ]
        then
            echo "Entrez le nom de l'executable (par exemple test_context s'il est dans le PATH, decac ou ima) :"
            read SECTION_NAME
            echo "Entrez l'extension du fichier à tester (par exemple : deca, ass) :"
            read EXTENSION
            $SECTION_NAME "$FILE_ROOT.$EXTENSION" > $TEMP_DIR/std_out 2> $TEMP_DIR/std_err
            printf $? > $TEMP_DIR/exit_code
            python3 "$DIR/oracle-section.py" $XPC_FILE $TEMP_DIR $SECTION_NAME $EXTENSION
        fi
    done

    atom --wait $XPC_FILE
done

rm -rf $TEMP_DIR
