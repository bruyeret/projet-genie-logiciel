#!/usr/bin/env bash

# Test tous les fichiers xpc manquants

TEST_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
PATH="$TEST_PATH/script/launchers:$PATH"

if [ $# -ne 1 ]
then
    echo "Argument attendu : dossier où chercher"
    exit 1
fi

for FILE in $(find "$1" -name "*.deca")
do
    if [ ! -f "${FILE:0: -5}.xpc" ]
    then
        echo "$FILE"
    fi
done
