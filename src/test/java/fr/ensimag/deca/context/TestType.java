package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestType {
    @Test 
    public void testTypes() {
        // Check same Type
        SymbolTable symbols = new SymbolTable();
        // Boolean check
        BooleanType B1 = new BooleanType(symbols.create("b1"));
        BooleanType B2 = new BooleanType(symbols.create("b2"));
        // Check that B1 and B2 of same type
        assertTrue(B1.sameType(B2));
        // Float check
        FloatType F1 = new FloatType(symbols.create("f1"));
        FloatType F2 = new FloatType(symbols.create("f2"));
        // Check that F1 and F2 of same type
        assertTrue(F1.sameType(F2));
        // Int check
        IntType I1 = new IntType(symbols.create("i1"));
        IntType I2 = new IntType(symbols.create("i2"));
        // Check that I1 and I2 of same type
        assertTrue(I1.sameType(I2));
        // String check
        StringType S1 = new StringType(symbols.create("s1"));
        StringType S2 = new StringType(symbols.create("s2"));
        // Check that S1 and S2 of same type
        assertTrue(S1.sameType(S2));
        // Void check
        VoidType V1 = new VoidType(symbols.create("v1"));
        VoidType V2 = new VoidType(symbols.create("v2"));
        // Check that V1 and V2 of same type
        assertTrue(V1.sameType(V2)); 
        // Check different Type
        assertFalse(B1.sameType(F1));
        assertFalse(F1.sameType(S1));
        assertFalse(I1.sameType(V1));
        assertFalse(S1.sameType(I1));
        assertFalse(V1.sameType(B1));
        // Check for Class
        // Boolean check
        ClassType C1 = new ClassType(symbols.create("c1"));
        ClassType C2 = new ClassType(symbols.create("c2"));
        // Check that C1 and C2 of same type
        assertTrue(C1.sameType(C2));
        // Check C1 and B1 different Type
        assertFalse(C1.sameType(B1));
        // Check SuperType
        assertFalse(C1.isSubClassOf(C2));
    }
}