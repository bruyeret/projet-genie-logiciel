package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestEnvironmentExp {
    @Test 
    public void testEnvironmentExp() {
        // Check methode declare and get
        SymbolTable symbols = new SymbolTable();
        Symbol S = symbols.create("s");
        Type T = new IntType(S);
        // line 1 column 1 of file filename
        Location L = new Location(1,1, "filename");
        ExpDefinition D = new VariableDefinition(T, L);
        EnvironmentExp E = new EnvironmentExp(null); 
        try {
            E.declare(S, D);
        }
        catch (EnvironmentExp.DoubleDefException e) {}
        assertEquals(D,E.get(S));
    }
}